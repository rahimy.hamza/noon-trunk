﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NoonOnline.Application.Common.Interfaces;

namespace NoonOnline.Persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<NoonDbContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("NoonOnlineDatabase"))
                        .UseSnakeCaseNamingConvention());

            services.AddScoped<INoonDbContext>(provider => provider.GetService<NoonDbContext>());

            return services;
        }
    }
}
