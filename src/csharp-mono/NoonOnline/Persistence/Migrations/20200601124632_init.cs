﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NoonOnline.Domain.Enums.Courses;
using NoonOnline.Domain.Enums.Messaging;
using NoonOnline.Domain.Enums.Sales;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace NoonOnline.Persistence.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:Enum:action_types", "approve,deny,cancel,restart,resolve,begin,escalate,super_complete,super_escalate")
                .Annotation("Npgsql:Enum:activity_types", "add_note,send_email,add_stakeholders,remove_stakeholders")
                .Annotation("Npgsql:Enum:course_content_types", "chapter,base,pdf,video,quiz,assignment")
                .Annotation("Npgsql:Enum:course_cultures", "dari,pashto,english")
                .Annotation("Npgsql:Enum:course_grades", "grade1,grade2,grade3,grade4,grade5,grade6,grade7,grade8,grade9,grade10,grade11,grade12")
                .Annotation("Npgsql:Enum:course_version_production_process_actions", "start_development,complete_development,approve_review,reject_review,start_content_preparation,complete_content_preparation,pass_content_preparation,fail_content_preparation,cancel")
                .Annotation("Npgsql:Enum:course_version_production_states", "started,in_development,review_rejected,content_preparation,content_preparation_failed,released,cancelled")
                .Annotation("Npgsql:Enum:message_reaction_types", "like,dislike")
                .Annotation("Npgsql:Enum:message_types", "discussion_topic,discussion_comment,course_admin_note,assignment_submission_note")
                .Annotation("Npgsql:Enum:processes", "course_version_production,course_package_version_production")
                .Annotation("Npgsql:Enum:quiz_content_types", "question,answer")
                .Annotation("Npgsql:Enum:software_options", "discussions,library,analytics,sponsor_cp")
                .Annotation("Npgsql:Enum:software_types", "course,course_package")
                .Annotation("Npgsql:Enum:state_types", "start,normal,complete,denied,cancelled")
                .Annotation("Npgsql:Enum:targets", "requester,stakeholders,group_members,process_admins");

            migrationBuilder.CreateTable(
                name: "course_version_production_transitions",
                columns: table => new
                {
                    transition_id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    current_state = table.Column<CourseVersionProductionStates>(nullable: false),
                    next_state = table.Column<CourseVersionProductionStates>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_course_version_production_transitions", x => x.transition_id);
                });

            migrationBuilder.CreateTable(
                name: "offers",
                columns: table => new
                {
                    offer_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    start_date = table.Column<DateTime>(nullable: false),
                    end_date = table.Column<DateTime>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    discount_amount = table.Column<decimal>(nullable: true),
                    discount_percentage = table.Column<decimal>(nullable: true),
                    duration_months = table.Column<int>(nullable: true),
                    duration_end_date = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_offers", x => x.offer_id);
                });

            migrationBuilder.CreateTable(
                name: "options",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_options", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "type_infos",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    type_name = table.Column<string>(nullable: true),
                    type_value = table.Column<int>(nullable: false),
                    course_content_type = table.Column<CourseContentTypes>(nullable: false),
                    course_culture = table.Column<CourseCultures>(nullable: false),
                    course_grade = table.Column<CourseGrades>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_type_infos", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "user_group_types",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    members_min = table.Column<int>(nullable: false),
                    members_max = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_group_types", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "course_version_production_transition_actions",
                columns: table => new
                {
                    transition_id = table.Column<int>(nullable: false),
                    action = table.Column<CourseVersionProductionProcessActions>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_course_version_production_transition_actions", x => new { x.transition_id, x.action });
                    table.ForeignKey(
                        name: "FK_course_version_production_transition_actions_course_version~",
                        column: x => x.transition_id,
                        principalTable: "course_version_production_transitions",
                        principalColumn: "transition_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "user_groups",
                columns: table => new
                {
                    user_group_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    user_group_type_id = table.Column<int>(nullable: false),
                    customer_invoice_data = table.Column<string>(nullable: true),
                    insert_ts = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_groups", x => x.user_group_id);
                    table.ForeignKey(
                        name: "FK_user_groups_user_group_types_user_group_type_id",
                        column: x => x.user_group_type_id,
                        principalTable: "user_group_types",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "memberships_in_group",
                columns: table => new
                {
                    in_group_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    user_group_id = table.Column<string>(nullable: true),
                    auth_user_id_ref = table.Column<string>(nullable: true),
                    added_on = table.Column<DateTime>(nullable: false),
                    removed_on = table.Column<DateTime>(nullable: true),
                    group_admin = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_memberships_in_group", x => x.in_group_id);
                    table.ForeignKey(
                        name: "FK_memberships_in_group_user_groups_user_group_id",
                        column: x => x.user_group_id,
                        principalTable: "user_groups",
                        principalColumn: "user_group_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "sponsors",
                columns: table => new
                {
                    sponsor_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    sponsor_name = table.Column<string>(nullable: true),
                    user_id = table.Column<string>(nullable: true),
                    user_name = table.Column<string>(nullable: true),
                    user_email = table.Column<string>(nullable: true),
                    saas_user_group_id = table.Column<string>(nullable: true),
                    validity_start = table.Column<DateTime>(nullable: false),
                    validity_end = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sponsors", x => x.sponsor_id);
                    table.ForeignKey(
                        name: "FK_sponsors_user_groups_saas_user_group_id",
                        column: x => x.saas_user_group_id,
                        principalTable: "user_groups",
                        principalColumn: "user_group_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bucket_files",
                columns: table => new
                {
                    bucket_file_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    bucket_name = table.Column<string>(nullable: true),
                    original_file_name = table.Column<string>(nullable: true),
                    file_path = table.Column<string>(nullable: true),
                    uploaded = table.Column<bool>(nullable: false),
                    content_type_metadata = table.Column<string>(nullable: true),
                    content_encoding_metadata = table.Column<string>(nullable: true),
                    content_language_metadata = table.Column<string>(nullable: true),
                    cache_control_metadata = table.Column<string>(nullable: true),
                    size_in_bytes = table.Column<int>(nullable: false),
                    ui_title = table.Column<string>(nullable: true),
                    ui_description = table.Column<string>(nullable: true),
                    archived = table.Column<bool>(nullable: false),
                    attached_to_message_id = table.Column<string>(nullable: true),
                    resource_of_course_content_id = table.Column<string>(nullable: true),
                    resource_of_course_content_version_id = table.Column<string>(nullable: true),
                    added_to_assignment_submission_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bucket_files", x => x.bucket_file_id);
                });

            migrationBuilder.CreateTable(
                name: "messages",
                columns: table => new
                {
                    message_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    type = table.Column<MessageTypes>(nullable: false),
                    comment_in_topic_with_id = table.Column<string>(nullable: true),
                    title = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    message_text = table.Column<string>(nullable: true),
                    permanent = table.Column<bool>(nullable: false),
                    course_id = table.Column<string>(nullable: true),
                    course_version_id = table.Column<string>(nullable: true),
                    assignment_submission_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_messages", x => x.message_id);
                    table.ForeignKey(
                        name: "FK_messages_messages_comment_in_topic_with_id",
                        column: x => x.comment_in_topic_with_id,
                        principalTable: "messages",
                        principalColumn: "message_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "message_reactions",
                columns: table => new
                {
                    message_reaction_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    message_id = table.Column<string>(nullable: true),
                    type = table.Column<MessageReactionTypes>(nullable: false),
                    removed = table.Column<bool>(nullable: false),
                    removed_on = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_message_reactions", x => x.message_reaction_id);
                    table.ForeignKey(
                        name: "FK_message_reactions_messages_message_id",
                        column: x => x.message_id,
                        principalTable: "messages",
                        principalColumn: "message_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "course_content",
                columns: table => new
                {
                    course_content_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    content_type = table.Column<CourseContentTypes>(nullable: false),
                    course_id = table.Column<string>(nullable: true),
                    in_chapter_with_id = table.Column<string>(nullable: true),
                    sequence_position = table.Column<int>(nullable: false),
                    title = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    transcript_file_id = table.Column<string>(nullable: true),
                    pdf_file_id = table.Column<string>(nullable: true),
                    video_file_id = table.Column<string>(nullable: true),
                    optional = table.Column<bool>(nullable: false),
                    archived = table.Column<bool>(nullable: false),
                    total_marks = table.Column<int>(nullable: true),
                    passing_marks = table.Column<int>(nullable: true),
                    weight = table.Column<decimal>(nullable: false),
                    attempt_limit = table.Column<int>(nullable: true),
                    current_version_id = table.Column<string>(nullable: true),
                    current_version_course_content_version_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_course_content", x => x.course_content_id);
                    table.ForeignKey(
                        name: "FK_course_content_course_content_in_chapter_with_id",
                        column: x => x.in_chapter_with_id,
                        principalTable: "course_content",
                        principalColumn: "course_content_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_content_bucket_files_pdf_file_id",
                        column: x => x.pdf_file_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_content_bucket_files_transcript_file_id",
                        column: x => x.transcript_file_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_content_bucket_files_video_file_id",
                        column: x => x.video_file_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "course_content_versions",
                columns: table => new
                {
                    course_content_version_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    content_type = table.Column<CourseContentTypes>(nullable: false),
                    course_id = table.Column<string>(nullable: true),
                    course_version_id = table.Column<string>(nullable: true),
                    in_chapter_version_with_id = table.Column<string>(nullable: true),
                    course_content_id = table.Column<string>(nullable: true),
                    sequence_position = table.Column<int>(nullable: false),
                    title = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    transcript_file_id = table.Column<string>(nullable: true),
                    pdf_file_id = table.Column<string>(nullable: true),
                    video_file_id = table.Column<string>(nullable: true),
                    optional = table.Column<bool>(nullable: false),
                    archived = table.Column<bool>(nullable: false),
                    passing_mark = table.Column<int>(nullable: true),
                    weight = table.Column<decimal>(nullable: false),
                    previous_version_id = table.Column<string>(nullable: true),
                    next_version_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_course_content_versions", x => x.course_content_version_id);
                    table.ForeignKey(
                        name: "FK_course_content_versions_course_content_course_content_id",
                        column: x => x.course_content_id,
                        principalTable: "course_content",
                        principalColumn: "course_content_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_content_versions_course_content_versions_in_chapter_~",
                        column: x => x.in_chapter_version_with_id,
                        principalTable: "course_content_versions",
                        principalColumn: "course_content_version_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_content_versions_course_content_versions_next_versio~",
                        column: x => x.next_version_id,
                        principalTable: "course_content_versions",
                        principalColumn: "course_content_version_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_content_versions_bucket_files_pdf_file_id",
                        column: x => x.pdf_file_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_content_versions_course_content_versions_previous_ve~",
                        column: x => x.previous_version_id,
                        principalTable: "course_content_versions",
                        principalColumn: "course_content_version_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_content_versions_bucket_files_transcript_file_id",
                        column: x => x.transcript_file_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_content_versions_bucket_files_video_file_id",
                        column: x => x.video_file_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "assignment_submissions",
                columns: table => new
                {
                    assignment_submission_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    course_content_id = table.Column<string>(nullable: true),
                    course_content_version_id = table.Column<string>(nullable: true),
                    course_id = table.Column<string>(nullable: true),
                    graded = table.Column<bool>(nullable: false),
                    graded_by = table.Column<string>(nullable: true),
                    graded_on = table.Column<DateTime>(nullable: true),
                    assigned_marks = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_assignment_submissions", x => x.assignment_submission_id);
                    table.ForeignKey(
                        name: "FK_assignment_submissions_course_content_course_content_id",
                        column: x => x.course_content_id,
                        principalTable: "course_content",
                        principalColumn: "course_content_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_assignment_submissions_course_content_versions_course_conte~",
                        column: x => x.course_content_version_id,
                        principalTable: "course_content_versions",
                        principalColumn: "course_content_version_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "course_content_progressions",
                columns: table => new
                {
                    course_content_progress_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    course_content_id = table.Column<string>(nullable: true),
                    course_content_version_id = table.Column<string>(nullable: true),
                    progress_percentage = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_course_content_progressions", x => x.course_content_progress_id);
                    table.ForeignKey(
                        name: "FK_course_content_progressions_course_content_course_content_id",
                        column: x => x.course_content_id,
                        principalTable: "course_content",
                        principalColumn: "course_content_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_content_progressions_course_content_versions_course_~",
                        column: x => x.course_content_version_id,
                        principalTable: "course_content_versions",
                        principalColumn: "course_content_version_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "quiz_attempts",
                columns: table => new
                {
                    quiz_attempt_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    course_content_id = table.Column<string>(nullable: true),
                    course_content_version_id = table.Column<string>(nullable: true),
                    passed = table.Column<bool>(nullable: false),
                    achieved_marks = table.Column<int>(nullable: false),
                    start_time = table.Column<DateTime>(nullable: true),
                    end_time = table.Column<DateTime>(nullable: true),
                    attempt_duration = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_quiz_attempts", x => x.quiz_attempt_id);
                    table.ForeignKey(
                        name: "FK_quiz_attempts_course_content_course_content_id",
                        column: x => x.course_content_id,
                        principalTable: "course_content",
                        principalColumn: "course_content_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_quiz_attempts_course_content_versions_course_content_versio~",
                        column: x => x.course_content_version_id,
                        principalTable: "course_content_versions",
                        principalColumn: "course_content_version_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "course_versions",
                columns: table => new
                {
                    course_version_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    course_id = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    culture = table.Column<CourseCultures>(nullable: false),
                    grade = table.Column<CourseGrades>(nullable: false),
                    changes_file_id = table.Column<string>(nullable: true),
                    available_in_trial = table.Column<bool>(nullable: false),
                    archived = table.Column<bool>(nullable: false),
                    version_number = table.Column<int>(nullable: false),
                    version_name = table.Column<string>(nullable: true),
                    administered_by = table.Column<string>(nullable: true),
                    administered_on = table.Column<DateTime>(nullable: false),
                    published_by = table.Column<string>(nullable: true),
                    published_on = table.Column<DateTime>(nullable: false),
                    released_by = table.Column<string>(nullable: true),
                    released_on = table.Column<DateTime>(nullable: true),
                    previous_version_id = table.Column<string>(nullable: true),
                    next_version_id = table.Column<string>(nullable: true),
                    app_title = table.Column<string>(nullable: true),
                    app_description = table.Column<string>(nullable: true),
                    app_thumbnail_id = table.Column<string>(nullable: true),
                    app_trailer_id = table.Column<string>(nullable: true),
                    teacher_name = table.Column<string>(nullable: true),
                    teacher_image_id = table.Column<string>(nullable: true),
                    teacher_description = table.Column<string>(nullable: true),
                    app_course_manifest_file_id = table.Column<string>(nullable: true),
                    current_state = table.Column<CourseVersionProductionStates>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_course_versions", x => x.course_version_id);
                    table.ForeignKey(
                        name: "FK_course_versions_bucket_files_app_course_manifest_file_id",
                        column: x => x.app_course_manifest_file_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_versions_bucket_files_app_thumbnail_id",
                        column: x => x.app_thumbnail_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_versions_bucket_files_app_trailer_id",
                        column: x => x.app_trailer_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_versions_bucket_files_changes_file_id",
                        column: x => x.changes_file_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_versions_course_versions_next_version_id",
                        column: x => x.next_version_id,
                        principalTable: "course_versions",
                        principalColumn: "course_version_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_versions_course_versions_previous_version_id",
                        column: x => x.previous_version_id,
                        principalTable: "course_versions",
                        principalColumn: "course_version_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_versions_bucket_files_teacher_image_id",
                        column: x => x.teacher_image_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "course_version_production_request_actions",
                columns: table => new
                {
                    request_action_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    course_version_id = table.Column<string>(nullable: true),
                    action = table.Column<CourseVersionProductionProcessActions>(nullable: false),
                    transition_id = table.Column<int>(nullable: false),
                    active = table.Column<bool>(nullable: false),
                    complete = table.Column<bool>(nullable: false),
                    archived = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_course_version_production_request_actions", x => x.request_action_id);
                    table.ForeignKey(
                        name: "FK_course_version_production_request_actions_course_versions_c~",
                        column: x => x.course_version_id,
                        principalTable: "course_versions",
                        principalColumn: "course_version_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_version_production_request_actions_course_version_pr~",
                        column: x => x.transition_id,
                        principalTable: "course_version_production_transitions",
                        principalColumn: "transition_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "courses",
                columns: table => new
                {
                    course_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    culture = table.Column<CourseCultures>(nullable: false),
                    grade = table.Column<CourseGrades>(nullable: false),
                    available_in_trial = table.Column<bool>(nullable: false),
                    archived = table.Column<bool>(nullable: false),
                    app_title = table.Column<string>(nullable: true),
                    app_description = table.Column<string>(nullable: true),
                    app_thumbnail_id = table.Column<string>(nullable: true),
                    app_trailer_id = table.Column<string>(nullable: true),
                    teacher_name = table.Column<string>(nullable: true),
                    teacher_image_id = table.Column<string>(nullable: true),
                    teacher_description = table.Column<string>(nullable: true),
                    current_version_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_courses", x => x.course_id);
                    table.ForeignKey(
                        name: "FK_courses_bucket_files_app_thumbnail_id",
                        column: x => x.app_thumbnail_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_courses_bucket_files_app_trailer_id",
                        column: x => x.app_trailer_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_courses_course_versions_current_version_id",
                        column: x => x.current_version_id,
                        principalTable: "course_versions",
                        principalColumn: "course_version_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_courses_bucket_files_teacher_image_id",
                        column: x => x.teacher_image_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "course_progressions",
                columns: table => new
                {
                    course_progress_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    course_id = table.Column<string>(nullable: true),
                    course_version_id = table.Column<string>(nullable: true),
                    progress_percentage = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_course_progressions", x => x.course_progress_id);
                    table.ForeignKey(
                        name: "FK_course_progressions_courses_course_id",
                        column: x => x.course_id,
                        principalTable: "courses",
                        principalColumn: "course_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_course_progressions_course_versions_course_version_id",
                        column: x => x.course_version_id,
                        principalTable: "course_versions",
                        principalColumn: "course_version_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "enrollments",
                columns: table => new
                {
                    enrollment_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    assignee_user_id = table.Column<string>(nullable: true),
                    assignee_user_name = table.Column<string>(nullable: true),
                    assignee_user_email = table.Column<string>(nullable: true),
                    course_id = table.Column<string>(nullable: true),
                    course_version_id = table.Column<string>(nullable: true),
                    sponsor_id = table.Column<string>(nullable: true),
                    subscription_group_member_id = table.Column<string>(nullable: true),
                    fixed_duration_months = table.Column<int>(nullable: true),
                    fixed_duration_days = table.Column<int>(nullable: true),
                    start_date = table.Column<DateTime>(nullable: true),
                    end_date = table.Column<DateTime>(nullable: true),
                    active = table.Column<bool>(nullable: false),
                    activated_by = table.Column<string>(nullable: true),
                    activated_on = table.Column<DateTime>(nullable: true),
                    revoked = table.Column<bool>(nullable: false),
                    revoked_by = table.Column<string>(nullable: true),
                    revoked_on = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enrollments", x => x.enrollment_id);
                    table.ForeignKey(
                        name: "FK_enrollments_courses_course_id",
                        column: x => x.course_id,
                        principalTable: "courses",
                        principalColumn: "course_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_enrollments_course_versions_course_version_id",
                        column: x => x.course_version_id,
                        principalTable: "course_versions",
                        principalColumn: "course_version_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_enrollments_sponsors_sponsor_id",
                        column: x => x.sponsor_id,
                        principalTable: "sponsors",
                        principalColumn: "sponsor_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_enrollments_memberships_in_group_subscription_group_member_~",
                        column: x => x.subscription_group_member_id,
                        principalTable: "memberships_in_group",
                        principalColumn: "in_group_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "package_versions",
                columns: table => new
                {
                    package_version_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    package_id = table.Column<string>(nullable: true),
                    title = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    culture = table.Column<CourseCultures>(nullable: false),
                    grade = table.Column<CourseGrades>(nullable: true),
                    version_number = table.Column<int>(nullable: false),
                    version_name = table.Column<string>(nullable: true),
                    version_administered_by = table.Column<string>(nullable: true),
                    version_administered_on = table.Column<DateTime>(nullable: true),
                    version_published_by = table.Column<string>(nullable: true),
                    version_published_on = table.Column<DateTime>(nullable: true),
                    version_released_by = table.Column<string>(nullable: true),
                    version_released_on = table.Column<DateTime>(nullable: true),
                    app_title = table.Column<string>(nullable: true),
                    app_description = table.Column<string>(nullable: true),
                    app_thumbnail_id = table.Column<string>(nullable: true),
                    previous_version_id = table.Column<string>(nullable: true),
                    next_version_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_package_versions", x => x.package_version_id);
                    table.ForeignKey(
                        name: "FK_package_versions_bucket_files_app_thumbnail_id",
                        column: x => x.app_thumbnail_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_package_versions_package_versions_next_version_id",
                        column: x => x.next_version_id,
                        principalTable: "package_versions",
                        principalColumn: "package_version_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_package_versions_package_versions_previous_version_id",
                        column: x => x.previous_version_id,
                        principalTable: "package_versions",
                        principalColumn: "package_version_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "packages",
                columns: table => new
                {
                    package_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    title = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    culture = table.Column<CourseCultures>(nullable: false),
                    grade = table.Column<CourseGrades>(nullable: true),
                    version_number = table.Column<int>(nullable: false),
                    version_name = table.Column<string>(nullable: true),
                    version_administered_by = table.Column<string>(nullable: true),
                    version_administered_on = table.Column<DateTime>(nullable: true),
                    version_published_by = table.Column<string>(nullable: true),
                    version_published_on = table.Column<DateTime>(nullable: true),
                    version_released_by = table.Column<string>(nullable: true),
                    version_released_on = table.Column<DateTime>(nullable: true),
                    app_title = table.Column<string>(nullable: true),
                    app_description = table.Column<string>(nullable: true),
                    app_thumbnail_id = table.Column<string>(nullable: true),
                    current_version_id = table.Column<string>(nullable: true),
                    current_version_package_version_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_packages", x => x.package_id);
                    table.ForeignKey(
                        name: "FK_packages_bucket_files_app_thumbnail_id",
                        column: x => x.app_thumbnail_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_packages_package_versions_current_version_package_version_id",
                        column: x => x.current_version_package_version_id,
                        principalTable: "package_versions",
                        principalColumn: "package_version_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "package_courses_versions",
                columns: table => new
                {
                    package_version_id = table.Column<string>(nullable: false),
                    course_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    package_id = table.Column<string>(nullable: true),
                    removed = table.Column<bool>(nullable: false),
                    removed_at = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_package_courses_versions", x => new { x.package_version_id, x.course_id });
                    table.ForeignKey(
                        name: "FK_package_courses_versions_courses_course_id",
                        column: x => x.course_id,
                        principalTable: "courses",
                        principalColumn: "course_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_package_courses_versions_packages_package_id",
                        column: x => x.package_id,
                        principalTable: "packages",
                        principalColumn: "package_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_package_courses_versions_package_versions_package_version_id",
                        column: x => x.package_version_id,
                        principalTable: "package_versions",
                        principalColumn: "package_version_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "softwares",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(nullable: true),
                    details = table.Column<string>(nullable: true),
                    access_link = table.Column<string>(nullable: true),
                    type = table.Column<SoftwareTypes>(nullable: false),
                    course_id = table.Column<string>(nullable: true),
                    course_package_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_softwares", x => x.id);
                    table.ForeignKey(
                        name: "FK_softwares_courses_course_id",
                        column: x => x.course_id,
                        principalTable: "courses",
                        principalColumn: "course_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_softwares_packages_course_package_id",
                        column: x => x.course_package_id,
                        principalTable: "packages",
                        principalColumn: "package_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "package_courses",
                columns: table => new
                {
                    course_id = table.Column<string>(nullable: false),
                    package_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    current_version_id = table.Column<string>(nullable: true),
                    removed = table.Column<bool>(nullable: false),
                    removed_at = table.Column<DateTime>(nullable: true),
                    current_version_package_version_id = table.Column<string>(nullable: true),
                    current_version_course_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_package_courses", x => new { x.course_id, x.package_id });
                    table.ForeignKey(
                        name: "FK_package_courses_courses_course_id",
                        column: x => x.course_id,
                        principalTable: "courses",
                        principalColumn: "course_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_package_courses_packages_package_id",
                        column: x => x.package_id,
                        principalTable: "packages",
                        principalColumn: "package_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_package_courses_package_courses_versions_current_version_pa~",
                        columns: x => new { x.current_version_package_version_id, x.current_version_course_id },
                        principalTable: "package_courses_versions",
                        principalColumns: new[] { "package_version_id", "course_id" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "plans",
                columns: table => new
                {
                    plan_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    software_id = table.Column<int>(nullable: false),
                    user_group_type_id = table.Column<int>(nullable: false),
                    current_price = table.Column<decimal>(nullable: false),
                    inser_ts = table.Column<DateTime>(nullable: false),
                    active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_plans", x => x.plan_id);
                    table.ForeignKey(
                        name: "FK_plans_softwares_software_id",
                        column: x => x.software_id,
                        principalTable: "softwares",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_plans_user_group_types_user_group_type_id",
                        column: x => x.user_group_type_id,
                        principalTable: "user_group_types",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "included",
                columns: table => new
                {
                    include_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    offer_id = table.Column<string>(nullable: true),
                    plan_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_included", x => x.include_id);
                    table.ForeignKey(
                        name: "FK_included_offers_offer_id",
                        column: x => x.offer_id,
                        principalTable: "offers",
                        principalColumn: "offer_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_included_plans_plan_id",
                        column: x => x.plan_id,
                        principalTable: "plans",
                        principalColumn: "plan_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "option_includeds",
                columns: table => new
                {
                    option_included_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    plan_id = table.Column<string>(nullable: true),
                    option_id = table.Column<int>(nullable: false),
                    added_on = table.Column<DateTime>(nullable: false),
                    removed_on = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_option_includeds", x => x.option_included_id);
                    table.ForeignKey(
                        name: "FK_option_includeds_options_option_id",
                        column: x => x.option_id,
                        principalTable: "options",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_option_includeds_plans_plan_id",
                        column: x => x.plan_id,
                        principalTable: "plans",
                        principalColumn: "plan_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "prerequisites",
                columns: table => new
                {
                    prerequisite_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    offer_id = table.Column<string>(nullable: true),
                    plan_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_prerequisites", x => x.prerequisite_id);
                    table.ForeignKey(
                        name: "FK_prerequisites_offers_offer_id",
                        column: x => x.offer_id,
                        principalTable: "offers",
                        principalColumn: "offer_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_prerequisites_plans_plan_id",
                        column: x => x.plan_id,
                        principalTable: "plans",
                        principalColumn: "plan_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "subscriptions",
                columns: table => new
                {
                    subscription_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    user_group_id = table.Column<string>(nullable: true),
                    trial_period_start_date = table.Column<DateTime>(nullable: true),
                    trial_period_end_date = table.Column<DateTime>(nullable: true),
                    subscribe_after_trial = table.Column<bool>(nullable: false),
                    current_plan_id = table.Column<string>(nullable: true),
                    offer_id = table.Column<string>(nullable: true),
                    offer_start_date = table.Column<DateTime>(nullable: true),
                    offer_end_date = table.Column<DateTime>(nullable: true),
                    subscribed_on = table.Column<DateTime>(nullable: false),
                    valid_to = table.Column<DateTime>(nullable: false),
                    unsubscribed_on = table.Column<DateTime>(nullable: true),
                    insert_ts = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_subscriptions", x => x.subscription_id);
                    table.ForeignKey(
                        name: "FK_subscriptions_plans_current_plan_id",
                        column: x => x.current_plan_id,
                        principalTable: "plans",
                        principalColumn: "plan_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_subscriptions_offers_offer_id",
                        column: x => x.offer_id,
                        principalTable: "offers",
                        principalColumn: "offer_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_subscriptions_user_groups_user_group_id",
                        column: x => x.user_group_id,
                        principalTable: "user_groups",
                        principalColumn: "user_group_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "plan_histories",
                columns: table => new
                {
                    plan_history_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    subscription_id = table.Column<string>(nullable: true),
                    plan_id = table.Column<string>(nullable: true),
                    start_date = table.Column<DateTime>(nullable: false),
                    end_date = table.Column<DateTime>(nullable: true),
                    insert_ts = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_plan_histories", x => x.plan_history_id);
                    table.ForeignKey(
                        name: "FK_plan_histories_plans_plan_id",
                        column: x => x.plan_id,
                        principalTable: "plans",
                        principalColumn: "plan_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_plan_histories_subscriptions_subscription_id",
                        column: x => x.subscription_id,
                        principalTable: "subscriptions",
                        principalColumn: "subscription_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "invoices",
                columns: table => new
                {
                    invoice_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    customer_invoice_data = table.Column<string>(nullable: true),
                    subscription_id = table.Column<string>(nullable: true),
                    plan_history_id = table.Column<string>(nullable: true),
                    invoice_period_start_date = table.Column<DateTime>(nullable: false),
                    invoice_period_end_date = table.Column<DateTime>(nullable: false),
                    invoice_description = table.Column<string>(nullable: true),
                    invoice_amount = table.Column<decimal>(nullable: false),
                    invoice_created_ts = table.Column<DateTime>(nullable: false),
                    invoice_due_ts = table.Column<DateTime>(nullable: false),
                    invoice_paid_ts = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_invoices", x => x.invoice_id);
                    table.ForeignKey(
                        name: "FK_invoices_plan_histories_plan_history_id",
                        column: x => x.plan_history_id,
                        principalTable: "plan_histories",
                        principalColumn: "plan_history_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_invoices_subscriptions_subscription_id",
                        column: x => x.subscription_id,
                        principalTable: "subscriptions",
                        principalColumn: "subscription_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "quiz_content",
                columns: table => new
                {
                    quiz_content_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    course_content_id = table.Column<string>(nullable: true),
                    type = table.Column<QuizContentTypes>(nullable: false),
                    in_question_with_id = table.Column<string>(nullable: true),
                    title = table.Column<string>(nullable: true),
                    value = table.Column<string>(nullable: true),
                    explanation = table.Column<string>(nullable: true),
                    sequence_position = table.Column<int>(nullable: false),
                    attachment_file_id = table.Column<string>(nullable: true),
                    correct_answer = table.Column<bool>(nullable: true),
                    archived = table.Column<bool>(nullable: false),
                    current_version_id = table.Column<string>(nullable: true),
                    current_version_quiz_content_version_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_quiz_content", x => x.quiz_content_id);
                    table.ForeignKey(
                        name: "FK_quiz_content_bucket_files_attachment_file_id",
                        column: x => x.attachment_file_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_quiz_content_course_content_course_content_id",
                        column: x => x.course_content_id,
                        principalTable: "course_content",
                        principalColumn: "course_content_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_quiz_content_quiz_content_in_question_with_id",
                        column: x => x.in_question_with_id,
                        principalTable: "quiz_content",
                        principalColumn: "quiz_content_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "quiz_content_versions",
                columns: table => new
                {
                    quiz_content_version_id = table.Column<string>(nullable: false),
                    created_by = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    last_modified_by = table.Column<string>(nullable: true),
                    last_modified_on = table.Column<DateTime>(nullable: true),
                    course_content_version_id = table.Column<string>(nullable: true),
                    course_content_id = table.Column<string>(nullable: true),
                    quiz_content_id = table.Column<string>(nullable: true),
                    type = table.Column<QuizContentTypes>(nullable: false),
                    in_question_version_with_id = table.Column<string>(nullable: true),
                    title = table.Column<string>(nullable: true),
                    value = table.Column<string>(nullable: true),
                    explanation = table.Column<string>(nullable: true),
                    sequence = table.Column<int>(nullable: false),
                    attachment_file_id = table.Column<string>(nullable: true),
                    correct_answer = table.Column<bool>(nullable: true),
                    archived = table.Column<bool>(nullable: false),
                    previous_version_id = table.Column<string>(nullable: true),
                    next_version_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_quiz_content_versions", x => x.quiz_content_version_id);
                    table.ForeignKey(
                        name: "FK_quiz_content_versions_bucket_files_attachment_file_id",
                        column: x => x.attachment_file_id,
                        principalTable: "bucket_files",
                        principalColumn: "bucket_file_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_quiz_content_versions_course_content_course_content_id",
                        column: x => x.course_content_id,
                        principalTable: "course_content",
                        principalColumn: "course_content_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_quiz_content_versions_course_content_versions_course_conten~",
                        column: x => x.course_content_version_id,
                        principalTable: "course_content_versions",
                        principalColumn: "course_content_version_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_quiz_content_versions_quiz_content_versions_in_question_ver~",
                        column: x => x.in_question_version_with_id,
                        principalTable: "quiz_content_versions",
                        principalColumn: "quiz_content_version_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_quiz_content_versions_quiz_content_versions_next_version_id",
                        column: x => x.next_version_id,
                        principalTable: "quiz_content_versions",
                        principalColumn: "quiz_content_version_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_quiz_content_versions_quiz_content_versions_previous_versio~",
                        column: x => x.previous_version_id,
                        principalTable: "quiz_content_versions",
                        principalColumn: "quiz_content_version_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_quiz_content_versions_quiz_content_quiz_content_id",
                        column: x => x.quiz_content_id,
                        principalTable: "quiz_content",
                        principalColumn: "quiz_content_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_assignment_submissions_course_content_id",
                table: "assignment_submissions",
                column: "course_content_id");

            migrationBuilder.CreateIndex(
                name: "IX_assignment_submissions_course_content_version_id",
                table: "assignment_submissions",
                column: "course_content_version_id");

            migrationBuilder.CreateIndex(
                name: "IX_bucket_files_added_to_assignment_submission_id",
                table: "bucket_files",
                column: "added_to_assignment_submission_id");

            migrationBuilder.CreateIndex(
                name: "IX_bucket_files_attached_to_message_id",
                table: "bucket_files",
                column: "attached_to_message_id");

            migrationBuilder.CreateIndex(
                name: "IX_bucket_files_resource_of_course_content_id",
                table: "bucket_files",
                column: "resource_of_course_content_id");

            migrationBuilder.CreateIndex(
                name: "IX_bucket_files_resource_of_course_content_version_id",
                table: "bucket_files",
                column: "resource_of_course_content_version_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_course_id",
                table: "course_content",
                column: "course_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_current_version_course_content_version_id",
                table: "course_content",
                column: "current_version_course_content_version_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_in_chapter_with_id",
                table: "course_content",
                column: "in_chapter_with_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_pdf_file_id",
                table: "course_content",
                column: "pdf_file_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_transcript_file_id",
                table: "course_content",
                column: "transcript_file_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_video_file_id",
                table: "course_content",
                column: "video_file_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_progressions_course_content_id",
                table: "course_content_progressions",
                column: "course_content_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_progressions_course_content_version_id",
                table: "course_content_progressions",
                column: "course_content_version_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_versions_course_content_id",
                table: "course_content_versions",
                column: "course_content_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_versions_course_id",
                table: "course_content_versions",
                column: "course_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_versions_course_version_id",
                table: "course_content_versions",
                column: "course_version_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_versions_in_chapter_version_with_id",
                table: "course_content_versions",
                column: "in_chapter_version_with_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_versions_next_version_id",
                table: "course_content_versions",
                column: "next_version_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_course_content_versions_pdf_file_id",
                table: "course_content_versions",
                column: "pdf_file_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_versions_previous_version_id",
                table: "course_content_versions",
                column: "previous_version_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_course_content_versions_transcript_file_id",
                table: "course_content_versions",
                column: "transcript_file_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_content_versions_video_file_id",
                table: "course_content_versions",
                column: "video_file_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_progressions_course_id",
                table: "course_progressions",
                column: "course_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_progressions_course_version_id",
                table: "course_progressions",
                column: "course_version_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_version_production_request_actions_course_version_id",
                table: "course_version_production_request_actions",
                column: "course_version_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_version_production_request_actions_transition_id",
                table: "course_version_production_request_actions",
                column: "transition_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_versions_app_course_manifest_file_id",
                table: "course_versions",
                column: "app_course_manifest_file_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_versions_app_thumbnail_id",
                table: "course_versions",
                column: "app_thumbnail_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_versions_app_trailer_id",
                table: "course_versions",
                column: "app_trailer_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_course_versions_changes_file_id",
                table: "course_versions",
                column: "changes_file_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_versions_course_id",
                table: "course_versions",
                column: "course_id");

            migrationBuilder.CreateIndex(
                name: "IX_course_versions_next_version_id",
                table: "course_versions",
                column: "next_version_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_course_versions_previous_version_id",
                table: "course_versions",
                column: "previous_version_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_course_versions_teacher_image_id",
                table: "course_versions",
                column: "teacher_image_id");

            migrationBuilder.CreateIndex(
                name: "IX_courses_app_thumbnail_id",
                table: "courses",
                column: "app_thumbnail_id");

            migrationBuilder.CreateIndex(
                name: "IX_courses_app_trailer_id",
                table: "courses",
                column: "app_trailer_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_courses_current_version_id",
                table: "courses",
                column: "current_version_id");

            migrationBuilder.CreateIndex(
                name: "IX_courses_teacher_image_id",
                table: "courses",
                column: "teacher_image_id");

            migrationBuilder.CreateIndex(
                name: "IX_enrollments_course_id",
                table: "enrollments",
                column: "course_id");

            migrationBuilder.CreateIndex(
                name: "IX_enrollments_course_version_id",
                table: "enrollments",
                column: "course_version_id");

            migrationBuilder.CreateIndex(
                name: "IX_enrollments_sponsor_id",
                table: "enrollments",
                column: "sponsor_id");

            migrationBuilder.CreateIndex(
                name: "IX_enrollments_subscription_group_member_id",
                table: "enrollments",
                column: "subscription_group_member_id");

            migrationBuilder.CreateIndex(
                name: "IX_included_offer_id",
                table: "included",
                column: "offer_id");

            migrationBuilder.CreateIndex(
                name: "IX_included_plan_id",
                table: "included",
                column: "plan_id");

            migrationBuilder.CreateIndex(
                name: "IX_invoices_plan_history_id",
                table: "invoices",
                column: "plan_history_id");

            migrationBuilder.CreateIndex(
                name: "IX_invoices_subscription_id",
                table: "invoices",
                column: "subscription_id");

            migrationBuilder.CreateIndex(
                name: "IX_memberships_in_group_user_group_id",
                table: "memberships_in_group",
                column: "user_group_id");

            migrationBuilder.CreateIndex(
                name: "IX_message_reactions_message_id",
                table: "message_reactions",
                column: "message_id");

            migrationBuilder.CreateIndex(
                name: "IX_messages_assignment_submission_id",
                table: "messages",
                column: "assignment_submission_id");

            migrationBuilder.CreateIndex(
                name: "IX_messages_comment_in_topic_with_id",
                table: "messages",
                column: "comment_in_topic_with_id");

            migrationBuilder.CreateIndex(
                name: "IX_messages_course_id",
                table: "messages",
                column: "course_id");

            migrationBuilder.CreateIndex(
                name: "IX_messages_course_version_id",
                table: "messages",
                column: "course_version_id");

            migrationBuilder.CreateIndex(
                name: "IX_option_includeds_option_id",
                table: "option_includeds",
                column: "option_id");

            migrationBuilder.CreateIndex(
                name: "IX_option_includeds_plan_id",
                table: "option_includeds",
                column: "plan_id");

            migrationBuilder.CreateIndex(
                name: "IX_package_courses_package_id",
                table: "package_courses",
                column: "package_id");

            migrationBuilder.CreateIndex(
                name: "IX_package_courses_current_version_package_version_id_current_~",
                table: "package_courses",
                columns: new[] { "current_version_package_version_id", "current_version_course_id" });

            migrationBuilder.CreateIndex(
                name: "IX_package_courses_versions_course_id",
                table: "package_courses_versions",
                column: "course_id");

            migrationBuilder.CreateIndex(
                name: "IX_package_courses_versions_package_id",
                table: "package_courses_versions",
                column: "package_id");

            migrationBuilder.CreateIndex(
                name: "IX_package_versions_app_thumbnail_id",
                table: "package_versions",
                column: "app_thumbnail_id");

            migrationBuilder.CreateIndex(
                name: "IX_package_versions_next_version_id",
                table: "package_versions",
                column: "next_version_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_package_versions_package_id",
                table: "package_versions",
                column: "package_id");

            migrationBuilder.CreateIndex(
                name: "IX_package_versions_previous_version_id",
                table: "package_versions",
                column: "previous_version_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_packages_app_thumbnail_id",
                table: "packages",
                column: "app_thumbnail_id");

            migrationBuilder.CreateIndex(
                name: "IX_packages_current_version_package_version_id",
                table: "packages",
                column: "current_version_package_version_id");

            migrationBuilder.CreateIndex(
                name: "IX_plan_histories_plan_id",
                table: "plan_histories",
                column: "plan_id");

            migrationBuilder.CreateIndex(
                name: "IX_plan_histories_subscription_id",
                table: "plan_histories",
                column: "subscription_id");

            migrationBuilder.CreateIndex(
                name: "IX_plans_software_id",
                table: "plans",
                column: "software_id");

            migrationBuilder.CreateIndex(
                name: "IX_plans_user_group_type_id",
                table: "plans",
                column: "user_group_type_id");

            migrationBuilder.CreateIndex(
                name: "IX_prerequisites_offer_id",
                table: "prerequisites",
                column: "offer_id");

            migrationBuilder.CreateIndex(
                name: "IX_prerequisites_plan_id",
                table: "prerequisites",
                column: "plan_id");

            migrationBuilder.CreateIndex(
                name: "IX_quiz_attempts_course_content_id",
                table: "quiz_attempts",
                column: "course_content_id");

            migrationBuilder.CreateIndex(
                name: "IX_quiz_attempts_course_content_version_id",
                table: "quiz_attempts",
                column: "course_content_version_id");

            migrationBuilder.CreateIndex(
                name: "IX_quiz_content_attachment_file_id",
                table: "quiz_content",
                column: "attachment_file_id");

            migrationBuilder.CreateIndex(
                name: "IX_quiz_content_course_content_id",
                table: "quiz_content",
                column: "course_content_id");

            migrationBuilder.CreateIndex(
                name: "IX_quiz_content_current_version_quiz_content_version_id",
                table: "quiz_content",
                column: "current_version_quiz_content_version_id");

            migrationBuilder.CreateIndex(
                name: "IX_quiz_content_in_question_with_id",
                table: "quiz_content",
                column: "in_question_with_id");

            migrationBuilder.CreateIndex(
                name: "IX_quiz_content_versions_attachment_file_id",
                table: "quiz_content_versions",
                column: "attachment_file_id");

            migrationBuilder.CreateIndex(
                name: "IX_quiz_content_versions_course_content_id",
                table: "quiz_content_versions",
                column: "course_content_id");

            migrationBuilder.CreateIndex(
                name: "IX_quiz_content_versions_course_content_version_id",
                table: "quiz_content_versions",
                column: "course_content_version_id");

            migrationBuilder.CreateIndex(
                name: "IX_quiz_content_versions_in_question_version_with_id",
                table: "quiz_content_versions",
                column: "in_question_version_with_id");

            migrationBuilder.CreateIndex(
                name: "IX_quiz_content_versions_next_version_id",
                table: "quiz_content_versions",
                column: "next_version_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_quiz_content_versions_previous_version_id",
                table: "quiz_content_versions",
                column: "previous_version_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_quiz_content_versions_quiz_content_id",
                table: "quiz_content_versions",
                column: "quiz_content_id");

            migrationBuilder.CreateIndex(
                name: "IX_softwares_course_id",
                table: "softwares",
                column: "course_id");

            migrationBuilder.CreateIndex(
                name: "IX_softwares_course_package_id",
                table: "softwares",
                column: "course_package_id");

            migrationBuilder.CreateIndex(
                name: "IX_sponsors_saas_user_group_id",
                table: "sponsors",
                column: "saas_user_group_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_subscriptions_current_plan_id",
                table: "subscriptions",
                column: "current_plan_id");

            migrationBuilder.CreateIndex(
                name: "IX_subscriptions_offer_id",
                table: "subscriptions",
                column: "offer_id");

            migrationBuilder.CreateIndex(
                name: "IX_subscriptions_user_group_id",
                table: "subscriptions",
                column: "user_group_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_groups_user_group_type_id",
                table: "user_groups",
                column: "user_group_type_id");

            migrationBuilder.AddForeignKey(
                name: "FK_bucket_files_course_content_resource_of_course_content_id",
                table: "bucket_files",
                column: "resource_of_course_content_id",
                principalTable: "course_content",
                principalColumn: "course_content_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_bucket_files_course_content_versions_resource_of_course_con~",
                table: "bucket_files",
                column: "resource_of_course_content_version_id",
                principalTable: "course_content_versions",
                principalColumn: "course_content_version_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_bucket_files_assignment_submissions_added_to_assignment_sub~",
                table: "bucket_files",
                column: "added_to_assignment_submission_id",
                principalTable: "assignment_submissions",
                principalColumn: "assignment_submission_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_bucket_files_messages_attached_to_message_id",
                table: "bucket_files",
                column: "attached_to_message_id",
                principalTable: "messages",
                principalColumn: "message_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_messages_assignment_submissions_assignment_submission_id",
                table: "messages",
                column: "assignment_submission_id",
                principalTable: "assignment_submissions",
                principalColumn: "assignment_submission_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_messages_courses_course_id",
                table: "messages",
                column: "course_id",
                principalTable: "courses",
                principalColumn: "course_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_messages_course_versions_course_version_id",
                table: "messages",
                column: "course_version_id",
                principalTable: "course_versions",
                principalColumn: "course_version_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_course_content_course_content_versions_current_version_cour~",
                table: "course_content",
                column: "current_version_course_content_version_id",
                principalTable: "course_content_versions",
                principalColumn: "course_content_version_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_course_content_courses_course_id",
                table: "course_content",
                column: "course_id",
                principalTable: "courses",
                principalColumn: "course_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_course_content_versions_courses_course_id",
                table: "course_content_versions",
                column: "course_id",
                principalTable: "courses",
                principalColumn: "course_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_course_content_versions_course_versions_course_version_id",
                table: "course_content_versions",
                column: "course_version_id",
                principalTable: "course_versions",
                principalColumn: "course_version_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_course_versions_courses_course_id",
                table: "course_versions",
                column: "course_id",
                principalTable: "courses",
                principalColumn: "course_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_package_versions_packages_package_id",
                table: "package_versions",
                column: "package_id",
                principalTable: "packages",
                principalColumn: "package_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_quiz_content_quiz_content_versions_current_version_quiz_con~",
                table: "quiz_content",
                column: "current_version_quiz_content_version_id",
                principalTable: "quiz_content_versions",
                principalColumn: "quiz_content_version_id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_assignment_submissions_course_content_course_content_id",
                table: "assignment_submissions");

            migrationBuilder.DropForeignKey(
                name: "FK_bucket_files_course_content_resource_of_course_content_id",
                table: "bucket_files");

            migrationBuilder.DropForeignKey(
                name: "FK_course_content_versions_course_content_course_content_id",
                table: "course_content_versions");

            migrationBuilder.DropForeignKey(
                name: "FK_quiz_content_course_content_course_content_id",
                table: "quiz_content");

            migrationBuilder.DropForeignKey(
                name: "FK_quiz_content_versions_course_content_course_content_id",
                table: "quiz_content_versions");

            migrationBuilder.DropForeignKey(
                name: "FK_assignment_submissions_course_content_versions_course_conte~",
                table: "assignment_submissions");

            migrationBuilder.DropForeignKey(
                name: "FK_bucket_files_course_content_versions_resource_of_course_con~",
                table: "bucket_files");

            migrationBuilder.DropForeignKey(
                name: "FK_quiz_content_versions_course_content_versions_course_conten~",
                table: "quiz_content_versions");

            migrationBuilder.DropForeignKey(
                name: "FK_bucket_files_assignment_submissions_added_to_assignment_sub~",
                table: "bucket_files");

            migrationBuilder.DropForeignKey(
                name: "FK_messages_assignment_submissions_assignment_submission_id",
                table: "messages");

            migrationBuilder.DropForeignKey(
                name: "FK_bucket_files_messages_attached_to_message_id",
                table: "bucket_files");

            migrationBuilder.DropForeignKey(
                name: "FK_course_versions_courses_course_id",
                table: "course_versions");

            migrationBuilder.DropForeignKey(
                name: "FK_package_versions_bucket_files_app_thumbnail_id",
                table: "package_versions");

            migrationBuilder.DropForeignKey(
                name: "FK_packages_bucket_files_app_thumbnail_id",
                table: "packages");

            migrationBuilder.DropForeignKey(
                name: "FK_quiz_content_bucket_files_attachment_file_id",
                table: "quiz_content");

            migrationBuilder.DropForeignKey(
                name: "FK_quiz_content_versions_bucket_files_attachment_file_id",
                table: "quiz_content_versions");

            migrationBuilder.DropForeignKey(
                name: "FK_package_versions_packages_package_id",
                table: "package_versions");

            migrationBuilder.DropForeignKey(
                name: "FK_quiz_content_quiz_content_versions_current_version_quiz_con~",
                table: "quiz_content");

            migrationBuilder.DropTable(
                name: "course_content_progressions");

            migrationBuilder.DropTable(
                name: "course_progressions");

            migrationBuilder.DropTable(
                name: "course_version_production_request_actions");

            migrationBuilder.DropTable(
                name: "course_version_production_transition_actions");

            migrationBuilder.DropTable(
                name: "enrollments");

            migrationBuilder.DropTable(
                name: "included");

            migrationBuilder.DropTable(
                name: "invoices");

            migrationBuilder.DropTable(
                name: "message_reactions");

            migrationBuilder.DropTable(
                name: "option_includeds");

            migrationBuilder.DropTable(
                name: "package_courses");

            migrationBuilder.DropTable(
                name: "prerequisites");

            migrationBuilder.DropTable(
                name: "quiz_attempts");

            migrationBuilder.DropTable(
                name: "type_infos");

            migrationBuilder.DropTable(
                name: "course_version_production_transitions");

            migrationBuilder.DropTable(
                name: "sponsors");

            migrationBuilder.DropTable(
                name: "memberships_in_group");

            migrationBuilder.DropTable(
                name: "plan_histories");

            migrationBuilder.DropTable(
                name: "options");

            migrationBuilder.DropTable(
                name: "package_courses_versions");

            migrationBuilder.DropTable(
                name: "subscriptions");

            migrationBuilder.DropTable(
                name: "plans");

            migrationBuilder.DropTable(
                name: "offers");

            migrationBuilder.DropTable(
                name: "user_groups");

            migrationBuilder.DropTable(
                name: "softwares");

            migrationBuilder.DropTable(
                name: "user_group_types");

            migrationBuilder.DropTable(
                name: "course_content");

            migrationBuilder.DropTable(
                name: "course_content_versions");

            migrationBuilder.DropTable(
                name: "assignment_submissions");

            migrationBuilder.DropTable(
                name: "messages");

            migrationBuilder.DropTable(
                name: "courses");

            migrationBuilder.DropTable(
                name: "course_versions");

            migrationBuilder.DropTable(
                name: "bucket_files");

            migrationBuilder.DropTable(
                name: "packages");

            migrationBuilder.DropTable(
                name: "package_versions");

            migrationBuilder.DropTable(
                name: "quiz_content_versions");

            migrationBuilder.DropTable(
                name: "quiz_content");
        }
    }
}
