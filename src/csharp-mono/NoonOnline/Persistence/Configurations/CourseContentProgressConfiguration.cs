﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Students;

namespace NoonOnline.Persistence.Configurations
{
    public class CourseContentProgressConfiguration : IEntityTypeConfiguration<CourseContentProgress>
    {
        public void Configure(EntityTypeBuilder<CourseContentProgress> builder)
        {
            builder.HasKey(e => e.CourseContentProgressId);
            builder.Property(e => e.CourseContentProgressId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.CourseContent)
                .WithMany(e => e.Progressions)
                .HasForeignKey(e => e.CourseContentId);

            builder.HasOne(e => e.CourseContentVersion)
                .WithMany(e => e.Progressions)
                .HasForeignKey(e => e.CourseContentVersionId);
        }
    }
}
