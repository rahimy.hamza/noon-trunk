﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class PackageConfiguration : IEntityTypeConfiguration<Package>
    {
        public void Configure(EntityTypeBuilder<Package> builder)
        {
            builder.HasKey(e => e.PackageId);
            builder.Property(e => e.PackageId).ValueGeneratedOnAdd();
        }
    }
}
