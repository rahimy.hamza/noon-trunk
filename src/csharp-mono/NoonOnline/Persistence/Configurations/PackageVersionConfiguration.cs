﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class PackageVersionConfiguration : IEntityTypeConfiguration<PackageVersion>
    {
        public void Configure(EntityTypeBuilder<PackageVersion> builder)
        {
            builder.HasKey(e => e.PackageVersionId);
            builder.Property(e => e.PackageVersionId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.PreviousVersion)
                .WithOne()
                .HasForeignKey<PackageVersion>(e => e.PreviousVersionId);

            builder.HasOne(e => e.NextVersion)
                .WithOne()
                .HasForeignKey<PackageVersion>(e => e.NextVersionId);

            builder.HasOne(e => e.Package)
                .WithMany(e => e.Versions)
                .HasForeignKey(e => e.PackageId);
        }
    }
}
