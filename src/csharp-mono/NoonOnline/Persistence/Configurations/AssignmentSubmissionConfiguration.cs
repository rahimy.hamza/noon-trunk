﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Students;

namespace NoonOnline.Persistence.Configurations
{
    public class AssignmentSubmissionConfiguration : IEntityTypeConfiguration<AssignmentSubmission>
    {
        public void Configure(EntityTypeBuilder<AssignmentSubmission> builder)
        {
            builder.HasKey(e => e.AssignmentSubmissionId);
            builder.Property(e => e.AssignmentSubmissionId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.CourseContent)
                .WithMany(e => e.AssignmentSubmissions)
                .HasForeignKey(e => e.CourseContentId);

            builder.HasOne(e => e.CourseContentVersion)
                .WithMany(e => e.AssignmentSubmissions)
                .HasForeignKey(e => e.CourseContentVersionId);
        }
    }
}
