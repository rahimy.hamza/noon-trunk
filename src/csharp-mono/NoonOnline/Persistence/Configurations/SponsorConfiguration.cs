﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class SponsorConfiguration : IEntityTypeConfiguration<Sponsor>
    {
        public void Configure(EntityTypeBuilder<Sponsor> builder)
        {
            builder.HasKey(e => e.SponsorId);
            builder.Property(e => e.SponsorId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.SaasUserGroup)
                .WithOne()
                .HasForeignKey<Sponsor>(e => e.SaasUserGroupId);
        }
    }
}
