﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Sales;

namespace NoonOnline.Persistence.Configurations
{
    public class PrerequisiteConfiguration : IEntityTypeConfiguration<Prerequisite>
    {
        public void Configure(EntityTypeBuilder<Prerequisite> builder)
        {
            builder.HasKey(e => e.PrerequisiteId);
            builder.Property(e => e.PrerequisiteId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.Offer)
                .WithMany(e => e.OfferPrerequisites)
                .HasForeignKey(e => e.OfferId);

            builder.HasOne(e => e.Plan)
                .WithMany(e => e.OfferPrerequisites)
                .HasForeignKey(e => e.PlanId);
        }
    }
}
