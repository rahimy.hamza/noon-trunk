﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Sales;

namespace NoonOnline.Persistence.Configurations
{
    public class UserGroupConfiguration : IEntityTypeConfiguration<UserGroup>
    {
        public void Configure(EntityTypeBuilder<UserGroup> builder)
        {
            builder.HasKey(e => e.UserGroupId);
            builder.Property(e => e.UserGroupId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.UserGroupType)
                .WithMany(e => e.Groups)
                .HasForeignKey(e => e.UserGroupTypeId);
        }
    }
}
