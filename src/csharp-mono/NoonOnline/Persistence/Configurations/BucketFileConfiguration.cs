﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Files;

namespace NoonOnline.Persistence.Configurations
{
    public class BucketFileConfiguration : IEntityTypeConfiguration<BucketFile>
    {
        public void Configure(EntityTypeBuilder<BucketFile> builder)
        {
            builder.HasKey(e => e.BucketFileId);
            builder.Property(e => e.BucketFileId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.ResourceOfCourseContent)
                .WithMany(e => e.SupportingResources)
                .HasForeignKey(e => e.ResourceOfCourseContentId);

            builder.HasOne(e => e.ResourceOfCourseContentVersion)
                .WithMany(e => e.SupportingResources)
                .HasForeignKey(e => e.ResourceOfCourseContentVersionId);

            builder.HasOne(e => e.AttachedToMessage)
                .WithMany(e => e.Attachments)
                .HasForeignKey(e => e.AttachedToMessageId);

            builder.HasOne(e => e.AddedToAssignmentSubmission)
                .WithMany(e => e.SubmittedFiles)
                .HasForeignKey(e => e.AddedToAssignmentSubmissionId);
        }
    }
}
