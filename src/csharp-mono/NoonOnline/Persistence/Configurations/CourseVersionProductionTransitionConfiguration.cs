﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class CourseVersionProductionTransitionConfiguration : IEntityTypeConfiguration<CourseVersionProductionTransition>
    {
        public void Configure(EntityTypeBuilder<CourseVersionProductionTransition> builder)
        {
            builder.HasKey(e => e.TransitionId);
        }
    }
}
