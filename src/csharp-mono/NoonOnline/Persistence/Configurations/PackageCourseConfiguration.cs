﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class PackageCourseConfiguration : IEntityTypeConfiguration<PackageCourse>
    {
        public void Configure(EntityTypeBuilder<PackageCourse> builder)
        {
            builder.HasKey(e => new { e.CourseId, e.PackageId });

            builder.HasOne(e => e.Course)
                .WithMany(e => e.PackageCourses)
                .HasForeignKey(e => e.CourseId);

            builder.HasOne(e => e.Package)
                .WithMany(e => e.PackageCourses)
                .HasForeignKey(e => e.PackageId);
        }
    }
}
