﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class CourseConfiguration : IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder.HasKey(e => e.CourseId);
            builder.Property(e => e.CourseId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.CurrentVersion)
                .WithMany()
                .HasForeignKey(e => e.CurrentVersionId);

            builder.HasOne(e => e.AppTrailer)
                .WithOne()
                .HasForeignKey<Course>(e => e.AppTrailerId);
        }
    }
}
