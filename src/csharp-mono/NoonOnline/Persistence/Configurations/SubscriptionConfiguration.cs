﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Sales;

namespace NoonOnline.Persistence.Configurations
{
    public class SubscriptionConfiguration : IEntityTypeConfiguration<Subscription>
    {
        public void Configure(EntityTypeBuilder<Subscription> builder)
        {
            builder.HasKey(e => e.SubscriptionId);
            builder.Property(e => e.SubscriptionId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.Offer)
                .WithMany(e => e.Subscriptions)
                .HasForeignKey(e => e.OfferId);

            builder.HasOne(e => e.CurrentPlan)
                .WithMany(e => e.Subscriptions)
                .HasForeignKey(e => e.CurrentPlanId);

            builder.HasOne(e => e.UserGroup)
                .WithMany(e => e.Subscriptions)
                .HasForeignKey(e => e.UserGroupId);
        }
    }
}
