﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Sales;

namespace NoonOnline.Persistence.Configurations
{
    public class OptionIncludedConfiguration : IEntityTypeConfiguration<OptionIncluded>
    {
        public void Configure(EntityTypeBuilder<OptionIncluded> builder)
        {
            builder.HasKey(e => e.OptionIncludedId);
            builder.Property(e => e.OptionIncludedId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.Option)
                .WithMany(e => e.IncludedOptions)
                .HasForeignKey(e => e.OptionId);

            builder.HasOne(e => e.Plan)
                .WithMany(e => e.IncludedOptions)
                .HasForeignKey(e => e.PlanId);
        }
    }
}
