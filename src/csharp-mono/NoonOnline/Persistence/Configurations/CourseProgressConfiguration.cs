﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Students;

namespace NoonOnline.Persistence.Configurations
{
    public class CourseProgressConfiguration : IEntityTypeConfiguration<CourseProgress>
    {
        public void Configure(EntityTypeBuilder<CourseProgress> builder)
        {
            builder.HasKey(e => e.CourseProgressId);
            builder.Property(e => e.CourseProgressId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.Course)
                .WithMany(e => e.Progressions)
                .HasForeignKey(e => e.CourseId);

            builder.HasOne(e => e.CourseVersion)
                .WithMany(e => e.Progressions)
                .HasForeignKey(e => e.CourseVersionId);
        }
    }
}
