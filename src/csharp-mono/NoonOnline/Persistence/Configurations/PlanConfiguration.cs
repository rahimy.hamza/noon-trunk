﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Sales;

namespace NoonOnline.Persistence.Configurations
{
    public class PlanConfiguration : IEntityTypeConfiguration<Plan>
    {
        public void Configure(EntityTypeBuilder<Plan> builder)
        {
            builder.HasKey(e => e.PlanId);
            builder.Property(e => e.PlanId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.Software)
                .WithMany(e => e.Plans)
                .HasForeignKey(e => e.SoftwareId);

            builder.HasOne(e => e.UserGroupType)
                .WithMany(e => e.Plans)
                .HasForeignKey(e => e.UserGroupTypeId);
        }
    }
}
