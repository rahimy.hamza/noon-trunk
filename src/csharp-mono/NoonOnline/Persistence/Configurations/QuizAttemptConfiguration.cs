﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Students;

namespace NoonOnline.Persistence.Configurations
{
    public class QuizAttemptConfiguration : IEntityTypeConfiguration<QuizAttempt>
    {
        public void Configure(EntityTypeBuilder<QuizAttempt> builder)
        {
            builder.HasKey(e => e.QuizAttemptId);
            builder.Property(e => e.QuizAttemptId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.CourseContent)
                .WithMany(e => e.QuizAttempts)
                .HasForeignKey(e => e.CourseContentId);

            builder.HasOne(e => e.CourseContentVersion)
                .WithMany(e => e.QuizAttempts)
                .HasForeignKey(e => e.CourseContentVersionId);
        }
    }
}
