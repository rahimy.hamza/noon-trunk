﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Sales;

namespace NoonOnline.Persistence.Configurations
{
    public class PlanHistoryConfiguration : IEntityTypeConfiguration<PlanHistory>
    {
        public void Configure(EntityTypeBuilder<PlanHistory> builder)
        {
            builder.HasKey(e => e.PlanHistoryId);
            builder.Property(e => e.PlanHistoryId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.Plan)
                .WithMany(e => e.SubscriptionPlanHistoryEvents)
                .HasForeignKey(e => e.PlanId);

            builder.HasOne(e => e.Subscription)
                .WithMany(e => e.PlanHistoryEvents)
                .HasForeignKey(e => e.SubscriptionId);
        }
    }
}
