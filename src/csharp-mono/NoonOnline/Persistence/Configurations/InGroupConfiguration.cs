﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Sales;

namespace NoonOnline.Persistence.Configurations
{
    public class InGroupConfiguration : IEntityTypeConfiguration<InGroup>
    {
        public void Configure(EntityTypeBuilder<InGroup> builder)
        {
            builder.HasKey(e => e.InGroupId);
            builder.Property(e => e.InGroupId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.UserGroup)
                .WithMany(e => e.GroupMembers)
                .HasForeignKey(e => e.UserGroupId);
        }
    }
}
