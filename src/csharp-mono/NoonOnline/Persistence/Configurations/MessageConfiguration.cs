﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Messaging;

namespace NoonOnline.Persistence.Configurations
{
    public class MessageConfiguration : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            builder.HasKey(e => e.MessageId);
            builder.Property(e => e.MessageId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.Course)
                .WithMany(e => e.Notes)
                .HasForeignKey(e => e.CourseId);

            builder.HasOne(e => e.CourseVersion)
                .WithMany(e => e.Notes)
                .HasForeignKey(e => e.CourseVersionId);

            builder.HasOne(e => e.Topic)
                .WithMany(e => e.TopicComments)
                .HasForeignKey(e => e.CommentInTopicWithId);

            builder.HasOne(e => e.AssignmentSubmission)
                .WithMany(e => e.Messages)
                .HasForeignKey(e => e.AssignmentSubmissionId);
        }
    }
}
