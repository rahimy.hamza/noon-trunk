﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class CourseVersionProductionRequestActionConfiguration : IEntityTypeConfiguration<CourseVersionProductionRequestAction>
    {
        public void Configure(EntityTypeBuilder<CourseVersionProductionRequestAction> builder)
        {
            builder.HasKey(e => e.RequestActionId);
            builder.Property(e => e.RequestActionId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.CourseVersion)
                .WithMany(e => e.RequestActions)
                .HasForeignKey(e => e.CourseVersionId);

            builder.HasOne(e => e.Transition)
                .WithMany(e => e.RequestActions)
                .HasForeignKey(e => e.TransitionId);
        }
    }
}
