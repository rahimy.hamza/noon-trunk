﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Sales;

namespace NoonOnline.Persistence.Configurations
{
    public class IncludeConfiguration : IEntityTypeConfiguration<Include>
    {
        public void Configure(EntityTypeBuilder<Include> builder)
        {
            builder.HasKey(e => e.IncludeId);
            builder.Property(e => e.IncludeId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.Offer)
                .WithMany(e => e.OfferInclusions)
                .HasForeignKey(e => e.OfferId);

            builder.HasOne(e => e.Plan)
                .WithMany(e => e.OfferInclusions)
                .HasForeignKey(e => e.PlanId);
        }
    }
}
