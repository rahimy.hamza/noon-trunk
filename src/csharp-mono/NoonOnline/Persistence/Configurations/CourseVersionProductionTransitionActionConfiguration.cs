﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class CourseVersionProductionTransitionActionConfiguration : IEntityTypeConfiguration<CourseVersionProductionTransitionAction>
    {
        public void Configure(EntityTypeBuilder<CourseVersionProductionTransitionAction> builder)
        {
            builder.HasKey(e => new { e.TransitionId, e.Action });

            builder.HasOne(e => e.Transition)
                .WithMany(e => e.TransitionActions)
                .HasForeignKey(e => e.TransitionId);
        }
    }
}
