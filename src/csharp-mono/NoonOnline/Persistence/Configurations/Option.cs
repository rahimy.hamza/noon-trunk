﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace NoonOnline.Persistence.Configurations
{
    public class Option : IEntityTypeConfiguration<Domain.Entities.Sales.Option>
    {
        public void Configure(EntityTypeBuilder<Domain.Entities.Sales.Option> builder)
        {
            builder.HasKey(e => e.Id);
        }
    }
}
