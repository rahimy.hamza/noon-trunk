﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class EnrollmentConfiguration : IEntityTypeConfiguration<Enrollment>
    {
        public void Configure(EntityTypeBuilder<Enrollment> builder)
        {
            builder.HasKey(e => e.EnrollmentId);
            builder.Property(e => e.EnrollmentId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.Course)
                .WithMany(e => e.Enrollments)
                .HasForeignKey(e => e.CourseId);

            builder.HasOne(e => e.CourseVersion)
                .WithMany(e => e.Enrollments)
                .HasForeignKey(e => e.CourseVersionId);

            builder.HasOne(e => e.Sponsor)
                .WithMany(e => e.Enrollments)
                .HasForeignKey(e => e.SponsorId);

            builder.HasOne(e => e.SubscriptionGroupMember)
                .WithMany(e => e.MemberCourseEnrollments)
                .HasForeignKey(e => e.SubscriptionGroupMemberId);
        }
    }
}
