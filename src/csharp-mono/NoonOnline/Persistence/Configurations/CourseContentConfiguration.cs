﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class CourseContentConfiguration : IEntityTypeConfiguration<CourseContent>
    {
        public void Configure(EntityTypeBuilder<CourseContent> builder)
        {
            builder.HasKey(e => e.CourseContentId);
            builder.Property(e => e.CourseContentId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.TranscriptFile)
                .WithMany()
                .HasForeignKey(e => e.TranscriptFileId);

            builder.HasOne(e => e.Chapter)
                .WithMany(e => e.ChapterContents)
                .HasForeignKey(e => e.InChapterWithId);

            builder.HasOne(e => e.Course)
                .WithMany(e => e.Content)
                .HasForeignKey(e => e.CourseId);
        }
    }
}
