﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class QuizContentVersionConfiguration : IEntityTypeConfiguration<QuizContentVersion>
    {
        public void Configure(EntityTypeBuilder<QuizContentVersion> builder)
        {
            builder.HasKey(e => e.QuizContentVersionId);
            builder.Property(e => e.QuizContentVersionId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.PreviousVersion)
                .WithOne()
                .HasForeignKey<QuizContentVersion>(e => e.PreviousVersionId);

            builder.HasOne(e => e.NextVersion)
                .WithOne()
                .HasForeignKey<QuizContentVersion>(e => e.NextVersionId);

            builder.HasOne(e => e.CourseContentVersion)
                .WithMany(e => e.QuizContent)
                .HasForeignKey(e => e.CourseContentVersionId);

            builder.HasOne(e => e.QuizContent)
                .WithMany(e => e.Versions)
                .HasForeignKey(e => e.QuizContentId);

            builder.HasOne(e => e.Question)
                .WithMany(e => e.QuestionAnswers)
                .HasForeignKey(e => e.InQuestionVersionWithId);
        }
    }
}
