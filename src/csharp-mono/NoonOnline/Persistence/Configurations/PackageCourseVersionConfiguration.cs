﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class PackageCourseVersionConfiguration : IEntityTypeConfiguration<PackageCourseVersion>
    {
        public void Configure(EntityTypeBuilder<PackageCourseVersion> builder)
        {
            builder.HasKey(e => new { e.PackageVersionId, e.CourseId });

            builder.HasOne(e => e.Course)
                .WithMany(e => e.PackageCoursesVersion)
                .HasForeignKey(e => e.CourseId);

            builder.HasOne(e => e.PackageVersion)
                .WithMany(e => e.PackageCoursesVersion)
                .HasForeignKey(e => e.PackageVersionId);
        }
    }
}
