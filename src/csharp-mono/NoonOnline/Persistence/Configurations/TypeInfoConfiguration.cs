﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.System;

namespace NoonOnline.Persistence.Configurations
{
    public class TypeInfoConfiguration : IEntityTypeConfiguration<TypeInfo>
    {
        public void Configure(EntityTypeBuilder<TypeInfo> builder)
        {
            builder.HasKey(e => e.Id);
        }
    }
}
