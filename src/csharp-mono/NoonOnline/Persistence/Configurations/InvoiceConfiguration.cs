﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Sales;

namespace NoonOnline.Persistence.Configurations
{
    public class InvoiceConfiguration : IEntityTypeConfiguration<Invoice>
    {
        public void Configure(EntityTypeBuilder<Invoice> builder)
        {
            builder.HasKey(e => e.InvoiceId);
            builder.Property(e => e.InvoiceId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.PlanHistory)
                .WithMany(e => e.Invoices)
                .HasForeignKey(e => e.PlanHistoryId);

            builder.HasOne(e => e.Subscription)
                .WithMany(e => e.Invoices)
                .HasForeignKey(e => e.SubscriptionId);
        }
    }
}
