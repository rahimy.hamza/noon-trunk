﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class CourseContentVersionConfiguration : IEntityTypeConfiguration<CourseContentVersion>
    {
        public void Configure(EntityTypeBuilder<CourseContentVersion> builder)
        {
            builder.HasKey(e => e.CourseContentVersionId);
            builder.Property(e => e.CourseContentVersionId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.PreviousVersion)
                .WithOne()
                .HasForeignKey<CourseContentVersion>(e => e.PreviousVersionId);

            builder.HasOne(e => e.NextVersion)
                .WithOne()
                .HasForeignKey<CourseContentVersion>(e => e.NextVersionId);

            builder.HasOne(e => e.CourseContent)
                .WithMany(e => e.Versions)
                .HasForeignKey(e => e.CourseContentId);

            builder.HasOne(e => e.ChapterVersion)
                .WithMany(e => e.ChapterVersionContents)
                .HasForeignKey(e => e.InChapterVersionWithId);

            builder.HasOne(e => e.CourseVersion)
                .WithMany(e => e.Content)
                .HasForeignKey(e => e.CourseVersionId);
        }
    }
}
