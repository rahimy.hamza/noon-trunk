﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class CourseVersionConfiguration : IEntityTypeConfiguration<CourseVersion>
    {
        public void Configure(EntityTypeBuilder<CourseVersion> builder)
        {
            builder.HasKey(e => e.CourseVersionId);
            builder.Property(e => e.CourseVersionId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.PreviousVersion)
                .WithOne()
                .HasForeignKey<CourseVersion>(e => e.PreviousVersionId);

            builder.HasOne(e => e.NextVersion)
                .WithOne()
                .HasForeignKey<CourseVersion>(e => e.NextVersionId);

            builder.HasOne(e => e.Course)
                .WithMany(e => e.Versions)
                .HasForeignKey(e => e.CourseId);

            builder.HasOne(e => e.AppTrailer)
                .WithOne()
                .HasForeignKey<CourseVersion>(e => e.AppTrailerId);
        }
    }
}
