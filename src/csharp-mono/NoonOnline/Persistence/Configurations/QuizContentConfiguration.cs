﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Persistence.Configurations
{
    public class QuizContentConfiguration : IEntityTypeConfiguration<QuizContent>
    {
        public void Configure(EntityTypeBuilder<QuizContent> builder)
        {
            builder.HasKey(e => e.QuizContentId);
            builder.Property(e => e.QuizContentId).ValueGeneratedOnAdd();

            builder.HasOne(e => e.CourseContent)
                .WithMany(e => e.QuizContent)
                .HasForeignKey(e => e.CourseContentId);

            builder.HasOne(e => e.Question)
                .WithMany(e => e.QuestionAnswers)
                .HasForeignKey(e => e.InQuestionWithId);
        }
    }
}
