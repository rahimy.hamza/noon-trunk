﻿using Microsoft.EntityFrameworkCore;

namespace NoonOnline.Persistence
{
    public class NoonDbContextFactory : DesignTimeDbContextFactoryBase<NoonDbContext>
    {
        protected override NoonDbContext CreateNewInstance(DbContextOptions<NoonDbContext> options)
        {
            return new NoonDbContext(options);
        }
    }
}
