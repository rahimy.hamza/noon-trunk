﻿using Microsoft.EntityFrameworkCore;
using NoonOnline.Application.Common.Interfaces;
using NoonOnline.Common;
using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Courses;
using NoonOnline.Domain.Entities.Files;
using NoonOnline.Domain.Entities.Messaging;
using NoonOnline.Domain.Entities.Sales;
using NoonOnline.Domain.Entities.Students;
using NoonOnline.Domain.Entities.System;
using NoonOnline.Domain.Enums.Courses;
using NoonOnline.Domain.Enums.Messaging;
using NoonOnline.Domain.Enums.Sales;
using NoonOnline.Domain.Enums.Workflow;
using Npgsql;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Persistence
{
    public class NoonDbContext : DbContext, INoonDbContext
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;

        public NoonDbContext(DbContextOptions<NoonDbContext> options)
            : base(options)
        {
            // Map enums
            NpgsqlConnection.GlobalTypeMapper.MapEnum<CourseCultures>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<CourseContentTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<CourseGrades>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<CourseVersionProductionProcessActions>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<CourseVersionProductionStates>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<QuizContentTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<MessageTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<MessageReactionTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<SoftwareOptions>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<SoftwareTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<ActionTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<ActivityTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<Processes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<StateTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<Targets>();
        }

        public NoonDbContext(DbContextOptions<NoonDbContext> options,
            ICurrentUserService currentUserService,
            IDateTime dateTime)
            : base(options)
        {
            _currentUserService = currentUserService;
            _dateTime = dateTime;

            // Map enums
            NpgsqlConnection.GlobalTypeMapper.MapEnum<CourseCultures>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<CourseContentTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<CourseGrades>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<CourseVersionProductionProcessActions>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<CourseVersionProductionStates>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<QuizContentTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<MessageTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<MessageReactionTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<SoftwareOptions>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<SoftwareTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<ActionTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<ActivityTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<Processes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<StateTypes>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<Targets>();
        }

        public DbSet<TypeInfo> TypeInfos { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<CourseContent> CourseContent { get; set; }
        public DbSet<CourseContentVersion> CourseContentVersions { get; set; }
        public DbSet<CourseVersion> CourseVersions { get; set; }
        public DbSet<CourseVersionProductionRequestAction> CourseVersionProductionRequestActions { get; set; }
        public DbSet<CourseVersionProductionTransition> CourseVersionProductionTransitions { get; set; }
        public DbSet<CourseVersionProductionTransitionAction> CourseVersionProductionTransitionActions { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<PackageCourse> PackageCourses { get; set; }
        public DbSet<PackageCourseVersion> PackageCoursesVersions { get; set; }
        public DbSet<PackageVersion> PackageVersions { get; set; }
        public DbSet<QuizContent> QuizContent { get; set; }
        public DbSet<QuizContentVersion> QuizContentVersions { get; set; }
        public DbSet<Sponsor> Sponsors { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<MessageReaction> MessageReactions { get; set; }
        public DbSet<Include> Included { get; set; }
        public DbSet<InGroup> MembershipsInGroup { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<Option> Options { get; set; }
        public DbSet<OptionIncluded> OptionIncludeds { get; set; }
        public DbSet<Plan> Plans { get; set; }
        public DbSet<PlanHistory> PlanHistories { get; set; }
        public DbSet<Prerequisite> Prerequisites { get; set; }
        public DbSet<Software> Softwares { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<UserGroupType> UserGroupTypes { get; set; }
        public DbSet<AssignmentSubmission> AssignmentSubmissions { get; set; }
        public DbSet<CourseContentProgress> CourseContentProgressions { get; set; }
        public DbSet<CourseProgress> CourseProgressions { get; set; }
        public DbSet<QuizAttempt> QuizAttempts { get; set; }
        public DbSet<BucketFile> BucketFiles { get; set; }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedBy = _currentUserService.UserId;
                        entry.Entity.CreatedOn = _dateTime.Now;
                        break;
                    case EntityState.Modified:
                        entry.Entity.LastModifiedBy = _currentUserService.UserId;
                        entry.Entity.LastModifiedOn = _dateTime.Now;
                        break;
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(NoonDbContext).Assembly);

            // Map enums
            modelBuilder.HasPostgresEnum<CourseContentTypes>();
            modelBuilder.HasPostgresEnum<CourseCultures>();
            modelBuilder.HasPostgresEnum<CourseGrades>();
            modelBuilder.HasPostgresEnum<CourseVersionProductionProcessActions>();
            modelBuilder.HasPostgresEnum<CourseVersionProductionStates>();
            modelBuilder.HasPostgresEnum<QuizContentTypes>();
            modelBuilder.HasPostgresEnum<MessageTypes>();
            modelBuilder.HasPostgresEnum<MessageReactionTypes>();
            modelBuilder.HasPostgresEnum<SoftwareOptions>();
            modelBuilder.HasPostgresEnum<SoftwareTypes>();
            modelBuilder.HasPostgresEnum<ActionTypes>();
            modelBuilder.HasPostgresEnum<ActivityTypes>();
            modelBuilder.HasPostgresEnum<Processes>();
            modelBuilder.HasPostgresEnum<StateTypes>();
            modelBuilder.HasPostgresEnum<Targets>();
        }
    }
}
