﻿using System;

namespace NoonOnline.Common
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
