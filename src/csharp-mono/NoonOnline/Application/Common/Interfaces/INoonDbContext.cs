﻿using Microsoft.EntityFrameworkCore;
using NoonOnline.Domain.Entities.Courses;
using NoonOnline.Domain.Entities.Files;
using NoonOnline.Domain.Entities.Messaging;
using NoonOnline.Domain.Entities.Sales;
using NoonOnline.Domain.Entities.Students;
using NoonOnline.Domain.Entities.System;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.Common.Interfaces
{
    public interface INoonDbContext
    {
        DbSet<TypeInfo> TypeInfos { get; set; }
        DbSet<Course> Courses { get; set; }
        DbSet<CourseContent> CourseContent { get; set; }
        DbSet<CourseContentVersion> CourseContentVersions { get; set; }
        DbSet<CourseVersion> CourseVersions { get; set; }
        DbSet<CourseVersionProductionRequestAction> CourseVersionProductionRequestActions { get; set; }
        DbSet<CourseVersionProductionTransition> CourseVersionProductionTransitions { get; set; }
        DbSet<CourseVersionProductionTransitionAction> CourseVersionProductionTransitionActions { get; set; }
        DbSet<Enrollment> Enrollments { get; set; }
        DbSet<Package> Packages { get; set; }
        DbSet<PackageCourse> PackageCourses { get; set; }
        DbSet<PackageCourseVersion> PackageCoursesVersions { get; set; }
        DbSet<PackageVersion> PackageVersions { get; set; }
        DbSet<QuizContent> QuizContent { get; set; }
        DbSet<QuizContentVersion> QuizContentVersions { get; set; }
        DbSet<Sponsor> Sponsors { get; set; }
        DbSet<Message> Messages { get; set; }
        DbSet<MessageReaction> MessageReactions { get; set; }
        DbSet<Include> Included { get; set; }
        DbSet<InGroup> MembershipsInGroup { get; set; }
        DbSet<Invoice> Invoices { get; set; }
        DbSet<Offer> Offers { get; set; }
        DbSet<Option> Options { get; set; }
        DbSet<OptionIncluded> OptionIncludeds { get; set; }
        DbSet<Plan> Plans { get; set; }
        DbSet<PlanHistory> PlanHistories { get; set; }
        DbSet<Prerequisite> Prerequisites { get; set; }
        DbSet<Software> Softwares { get; set; }
        DbSet<Subscription> Subscriptions { get; set; }
        DbSet<UserGroup> UserGroups { get; set; }
        DbSet<UserGroupType> UserGroupTypes { get; set; }
        DbSet<AssignmentSubmission> AssignmentSubmissions { get; set; }
        DbSet<CourseContentProgress> CourseContentProgressions { get; set; }
        DbSet<CourseProgress> CourseProgressions { get; set; }
        DbSet<QuizAttempt> QuizAttempts { get; set; }
        DbSet<BucketFile> BucketFiles { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
