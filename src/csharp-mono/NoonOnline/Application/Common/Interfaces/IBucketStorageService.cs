﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.Common.Interfaces
{
    public interface IBucketStorageService
    {
        Task<string> CreateDownloadSignedUrl(string bucketName, string objectName, CancellationToken cancellationToken);
        Task<string> CreateUploadSignedUrl(string bucketName, string objectUri, string contentTypeHeader, CancellationToken cancellationToken);
        Task<string> CreateUploadSignedUrl(string bucketName, string objectUri, Dictionary<string, IEnumerable<string>> contentHeaders, CancellationToken cancellationToken);
        Task DeleteObject(string bucketName, string objectUri, CancellationToken cancellationToken);
    }
}
