﻿using NoonOnline.Application.Notifications.Models;
using System.Threading.Tasks;

namespace NoonOnline.Application.Common.Interfaces
{
    public interface INotificationService
    {
        Task SendAsync(MessageDto message);
    }
}
