﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NoonOnline.Application.Common.Exceptions;
using NoonOnline.Application.Common.Interfaces;
using NoonOnline.Domain.Entities.System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.System.Queries.GetTypeInfo
{
    public class GetTypeInfoQuery : IRequest<TypeInfoListVm>
    {
    }

    public class Handler : IRequestHandler<GetTypeInfoQuery, TypeInfoListVm>
    {
        private readonly INoonDbContext _context;
        private readonly IMapper _mapper;

        public Handler(INoonDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<TypeInfoListVm> Handle(GetTypeInfoQuery request, CancellationToken cancellationToken)
        {
            var info = await _context.TypeInfos
                .ProjectTo<TypeInfoLookupDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            if (!info.Any())
                throw new NotFoundException(nameof(TypeInfo), "any");

            var vm = new TypeInfoListVm
            {
                TypeInfo = info
            };
            return vm;
        }
    }
}
