﻿using System.Collections.Generic;

namespace NoonOnline.Application.System.Queries.GetTypeInfo
{
    public class TypeInfoListVm
    {
        public IList<TypeInfoLookupDto> TypeInfo { get; set; }
    }
}
