﻿using AutoMapper;
using NoonOnline.Application.Common.Mappings;
using NoonOnline.Domain.Entities.System;

namespace NoonOnline.Application.System.Queries.GetTypeInfo
{
    public class TypeInfoLookupDto : IMapFrom<TypeInfo>
    {
        public string Name { get; set; }
        public int Value { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<TypeInfo, TypeInfoLookupDto>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.TypeName))
                .ForMember(d => d.Value, opt => opt.MapFrom(s => s.TypeValue));
        }
    }
}
