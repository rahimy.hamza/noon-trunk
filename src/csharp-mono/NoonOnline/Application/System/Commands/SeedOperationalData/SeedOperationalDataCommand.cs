﻿using MediatR;
using NoonOnline.Application.Common.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.System.Commands.SeedOperationalData
{
    public class SeedOperationalDataCommand : IRequest
    {
    }

    public class SeedOperationalDataCommandHandler : IRequestHandler<SeedOperationalDataCommand>
    {
        private readonly INoonDbContext _context;

        public SeedOperationalDataCommandHandler(INoonDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(SeedOperationalDataCommand request, CancellationToken cancellationToken)
        {
            var seeder = new OperationalDataSeeder(_context);

            await seeder.SeedAllAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
