﻿using NoonOnline.Application.Common.Interfaces;
using NoonOnline.Domain.Constants;
using NoonOnline.Domain.Entities.Courses;
using NoonOnline.Domain.Entities.System;
using NoonOnline.Domain.Enums.Courses;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.System.Commands.SeedOperationalData
{
    public class OperationalDataSeeder
    {
        private readonly INoonDbContext _context;

        public OperationalDataSeeder(INoonDbContext context)
        {
            _context = context;
        }

        public async Task SeedAllAsync(CancellationToken cancellationToken)
        {
            await SeedCourseVersionProductionTransitions(cancellationToken);
            await SeedSystemTypeInfo(cancellationToken);
        }

        private async Task SeedCourseVersionProductionTransitions(CancellationToken cancellationToken)
        {
            if (_context.CourseVersionProductionTransitions.Any())
            {
                return;
            }
            var StartToInDevTransition = CourseProductionTransitions.StartToInDevTransition;
            var InDevToReviewRejectedTransition = CourseProductionTransitions.InDevToReviewRejectedTransition;
            var InDevToContentPrepTransition = CourseProductionTransitions.InDevToContentPrepTransition;
            var ContentPrepToContentPrepFailTransition = CourseProductionTransitions.ContentPrepToContentPrepFailTransition;
            var ContentPrepToReleasedTransition = CourseProductionTransitions.ContentPrepToReleasedTransition;
            var ReviewRejectedToInDevelopmentTransition = CourseProductionTransitions.ReviewRejectedToInDevelopmentTransition;
            var ContentPrepFailToInDevTransition = CourseProductionTransitions.ContentPrepFailToInDevTransition;
            var StartedToCancelledTransition = CourseProductionTransitions.StartedToCancelledTransition;
            var InDevToCancelledTransition = CourseProductionTransitions.InDevToCancelledTransition;
            var ReviewRejectedToCancelledTransition = CourseProductionTransitions.ReviewRejectedToCancelledTransition;
            var ContentPrepFailToCancelledTransition = CourseProductionTransitions.ContentPrepFailToCancelledTransition;

            _context.CourseVersionProductionTransitions.Add(StartToInDevTransition);
            _context.CourseVersionProductionTransitions.Add(InDevToReviewRejectedTransition);
            _context.CourseVersionProductionTransitions.Add(InDevToContentPrepTransition);
            _context.CourseVersionProductionTransitions.Add(ContentPrepToContentPrepFailTransition);
            _context.CourseVersionProductionTransitions.Add(ContentPrepToReleasedTransition);
            _context.CourseVersionProductionTransitions.Add(ReviewRejectedToInDevelopmentTransition);
            _context.CourseVersionProductionTransitions.Add(ContentPrepFailToInDevTransition);
            _context.CourseVersionProductionTransitions.Add(StartedToCancelledTransition);
            _context.CourseVersionProductionTransitions.Add(InDevToCancelledTransition);
            _context.CourseVersionProductionTransitions.Add(ReviewRejectedToCancelledTransition);
            _context.CourseVersionProductionTransitions.Add(ContentPrepFailToCancelledTransition);

            await _context.SaveChangesAsync(cancellationToken);

            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = StartedToCancelledTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.Cancel
            });
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = InDevToCancelledTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.Cancel
            });
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = ReviewRejectedToCancelledTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.Cancel
            });
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = ContentPrepFailToCancelledTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.Cancel
            });

            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = StartToInDevTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.StartDevelopment
            });
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = ReviewRejectedToInDevelopmentTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.StartDevelopment
            });
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = ContentPrepFailToInDevTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.StartDevelopment
            });

            // InDevToReviewRejectedTransition Actions
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = InDevToReviewRejectedTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.CompleteDevelopment
            });
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = InDevToReviewRejectedTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.RejectReview
            });

            // InDevToContentPrepTransition Actions
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = InDevToContentPrepTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.CompleteDevelopment
            });
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = InDevToContentPrepTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.ApproveReview
            });

            // ContentPrepToContentPrepFailTransition Actions
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = ContentPrepToContentPrepFailTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.StartContentPreparation
            });
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = ContentPrepToContentPrepFailTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.CompleteContentPreparation
            });
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = ContentPrepToContentPrepFailTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.FailContentPreparation
            });

            // ContentPrepToReleasedTransition Actions
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = ContentPrepToReleasedTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.StartContentPreparation
            });
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = ContentPrepToReleasedTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.CompleteContentPreparation
            });
            _context.CourseVersionProductionTransitionActions.Add(new CourseVersionProductionTransitionAction
            {
                TransitionId = ContentPrepToReleasedTransition.TransitionId,
                Action = CourseVersionProductionProcessActions.PassContentPreparation
            });

            await _context.SaveChangesAsync(cancellationToken);
        }

        private async Task SeedSystemTypeInfo(CancellationToken cancellationToken)
        {
            if (_context.TypeInfos.Any())
            {
                return;
            }

            _context.TypeInfos.Add(new TypeInfo
            {
                Id = 1,
                TypeName = nameof(CourseContentTypes.Base),
                TypeValue = (int)CourseContentTypes.Base,
                CourseContentType = CourseContentTypes.Base
            });
            _context.TypeInfos.Add(new TypeInfo
            {
                Id = 2,
                TypeName = nameof(CourseContentTypes.Chapter),
                TypeValue = (int)CourseContentTypes.Chapter,
                CourseContentType = CourseContentTypes.Chapter
            });
            _context.TypeInfos.Add(new TypeInfo
            {
                Id = 3,
                TypeName = nameof(CourseContentTypes.PDF),
                TypeValue = (int)CourseContentTypes.PDF,
                CourseContentType = CourseContentTypes.PDF
            });
            _context.TypeInfos.Add(new TypeInfo
            {
                Id = 4,
                TypeName = nameof(CourseContentTypes.Video),
                TypeValue = (int)CourseContentTypes.Video,
                CourseContentType = CourseContentTypes.Video
            });
            _context.TypeInfos.Add(new TypeInfo
            {
                Id = 5,
                TypeName = nameof(CourseContentTypes.Quiz),
                TypeValue = (int)CourseContentTypes.Quiz,
                CourseContentType = CourseContentTypes.Quiz
            });
            _context.TypeInfos.Add(new TypeInfo
            {
                Id = 6,
                TypeName = nameof(CourseContentTypes.Assignment),
                TypeValue = (int)CourseContentTypes.Assignment,
                CourseContentType = CourseContentTypes.Assignment
            });

            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
