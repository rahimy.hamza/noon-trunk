﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NoonOnline.Application.Common.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.Courses.Queries.GetCourseNotes
{
    public class GetCourseNotesQueryHandler : IRequestHandler<GetCourseNotesQuery, NoteListVm>
    {
        private readonly INoonDbContext _context;
        private readonly IMapper _mapper;

        public GetCourseNotesQueryHandler(INoonDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<NoteListVm> Handle(GetCourseNotesQuery request, CancellationToken cancellationToken)
        {
            int pageIndex;
            if (request.Page <= 0)
            {
                pageIndex = 0;
            }
            else
            {
                pageIndex = request.Page * 30;
            }

            var notesCount = await _context.Messages
                .Where(e => e.CourseId == request.CourseId)
                .CountAsync(cancellationToken);

            var notes = await _context.Messages
                .Where(e => e.CourseId == request.CourseId)
                .OrderByDescending(e => e.CreatedOn)
                .Skip(pageIndex)
                .Take(30)
                .ProjectTo<CourseNoteLookupDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            var vm = new NoteListVm
            {
                Notes = notes,
                Page = request.Page,
                PageSize = notes.Count,
                TotalCount = notesCount,
                TotalPages = (int)Math.Ceiling((decimal)notesCount / 30)
            };

            return vm;
        }
    }
}
