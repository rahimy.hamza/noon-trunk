﻿using MediatR;

namespace NoonOnline.Application.Courses.Queries.GetCourseNotes
{
    public class GetCourseNotesQuery : IRequest<NoteListVm>
    {
        public string CourseId { get; set; }
        public int Page { get; set; } = 1;
    }
}
