﻿using System.Collections;
using System.Collections.Generic;

namespace NoonOnline.Application.Courses.Queries.GetCourseNotes
{
    public class NoteListVm
    {
        public IList<CourseNoteLookupDto> Notes { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }
    }
}
