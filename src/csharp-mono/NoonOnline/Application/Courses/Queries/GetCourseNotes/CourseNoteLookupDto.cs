﻿using AutoMapper;
using NoonOnline.Application.Common.Mappings;
using NoonOnline.Domain.Entities.Messaging;
using System;

namespace NoonOnline.Application.Courses.Queries.GetCourseNotes
{
    public class CourseNoteLookupDto : IMapFrom<Message>
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Message { get; set; }
        public string By { get; set; }
        public DateTime CreatedOn { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Message, CourseNoteLookupDto>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.MessageId))
                .ForMember(d => d.Title, opt => opt.MapFrom(s => s.Title))
                .ForMember(d => d.Description, opt => opt.MapFrom(s => s.Description))
                .ForMember(d => d.Message, opt => opt.MapFrom(s => s.MessageText))
                .ForMember(d => d.By, opt => opt.MapFrom(s => s.CreatedBy))
                .ForMember(d => d.CreatedOn, opt => opt.MapFrom(s => s.CreatedOn));
        }
    }
}
