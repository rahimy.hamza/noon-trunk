﻿using AutoMapper;
using NoonOnline.Application.Common.Mappings;
using NoonOnline.Domain.Entities.Courses;
using NoonOnline.Domain.Enums.Courses;

namespace NoonOnline.Application.Courses.Queries.GetCoursesList
{
    public class CourseLookupDto : IMapFrom<Course>
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public CourseCultures Culture { get; set; }
        public CourseGrades Grade { get; set; }
        public string CurrentVersionState { get; set; }
        public string CurrentVersion { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Course, CourseLookupDto>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.CourseId))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Name))
                .ForMember(d => d.Culture, opt => opt.MapFrom(s => s.Culture))
                .ForMember(d => d.Grade, opt => opt.MapFrom(s => s.Grade))
                .ForMember(d => d.CurrentVersionState, opt => opt.MapFrom(s => s.CurrentVersion.CurrentState.ToString()))
                .ForMember(d => d.CurrentVersion, opt => opt.MapFrom(s => s.CurrentVersion.VersionNumber.ToString() + "." + s.CurrentVersion.VersionName));
        }
    }
}
