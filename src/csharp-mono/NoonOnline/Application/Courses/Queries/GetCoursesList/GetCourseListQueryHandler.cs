﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NoonOnline.Application.Common.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.Courses.Queries.GetCoursesList
{
    public class GetCourseListQueryHandler : IRequestHandler<GetCoursesListQuery, CourseListVm>
    {
        private readonly INoonDbContext _context;
        private readonly IMapper _mapper;

        public GetCourseListQueryHandler(INoonDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<CourseListVm> Handle(GetCoursesListQuery request, CancellationToken cancellationToken)
        {
            int pageIndex;
            if (request.Page <= 0)
            {
                pageIndex = 0;
            }
            else
            {
                pageIndex = request.Page * 30;
            }

            var coursesCount = await _context.Courses.CountAsync(cancellationToken);
            var courses = await _context.Courses
                .Skip(pageIndex)
                .Take(30)
                .ProjectTo<CourseLookupDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            var vm = new CourseListVm
            {
                CoursesList = courses,
                Page = request.Page,
                PageSize = courses.Count,
                TotalCount = coursesCount,
                TotalPages = (int)Math.Ceiling((decimal)coursesCount / 30)
            };

            return vm;
        }
    }
}
