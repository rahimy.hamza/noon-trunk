﻿using MediatR;

namespace NoonOnline.Application.Courses.Queries.GetCoursesList
{
    public class GetCoursesListQuery : IRequest<CourseListVm>
    {
        public int Page { get; set; } = 1;
    }
}
