﻿using MediatR;

namespace NoonOnline.Application.Courses.Queries.GetCourseDetail
{
    public class GetCourseDetailQuery : IRequest<CourseDetailVm>
    {
        public string Id { get; set; }
    }
}
