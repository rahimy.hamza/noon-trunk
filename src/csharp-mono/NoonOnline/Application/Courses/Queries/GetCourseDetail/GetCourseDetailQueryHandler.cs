﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NoonOnline.Application.Common.Exceptions;
using NoonOnline.Application.Common.Interfaces;
using NoonOnline.Domain.Constants;
using NoonOnline.Domain.Entities.Courses;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.Courses.Queries.GetCourseDetail
{
    public class GetCourseDetailQueryHandler : IRequestHandler<GetCourseDetailQuery, CourseDetailVm>
    {
        private readonly INoonDbContext _context;
        private readonly IBucketStorageService _storageService;
        private readonly IMapper _mapper;

        public GetCourseDetailQueryHandler(INoonDbContext context, IBucketStorageService storageService, IMapper mapper)
        {
            _context = context;
            _storageService = storageService;
            _mapper = mapper;
        }

        public async Task<CourseDetailVm> Handle(GetCourseDetailQuery request, CancellationToken cancellationToken)
        {
            var vm = await _context.Courses
                .Where(c => c.CourseId == request.Id)
                .ProjectTo<CourseDetailVm>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(cancellationToken);

            if (vm == null)
            {
                throw new NotFoundException(nameof(Course), request.Id);
            }

            if (vm.GetThumbnailBucket() != null)
                vm.ThumbnailUrl = await _storageService.CreateDownloadSignedUrl(vm.GetThumbnailBucket(), vm.GetThumbnailFilePath(), cancellationToken);

            if (vm.GetTrailerFileBucket() != null)
                vm.TrailerUrl = await _storageService.CreateDownloadSignedUrl(vm.GetTrailerFileBucket(), vm.GetTrailerFilepath(), cancellationToken);

            if (vm.GetTeacherImageBucket() != null)
                vm.TeacherImageUrl = await _storageService.CreateDownloadSignedUrl(vm.GetTeacherImageBucket(), vm.GetTeacherImageFilepath(), cancellationToken);

            return vm;
        }
    }
}
