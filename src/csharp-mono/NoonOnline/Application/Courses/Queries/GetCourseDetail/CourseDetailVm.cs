﻿using AutoMapper;
using NoonOnline.Application.Common.Mappings;
using NoonOnline.Domain.Entities.Courses;
using NoonOnline.Domain.Enums.Courses;
using System.Linq;

namespace NoonOnline.Application.Courses.Queries.GetCourseDetail
{
    public class CourseDetailVm : IMapFrom<Course>
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Culture { get; set; }
        public string Grade { get; set; }
        public bool AvailableInTrial { get; set; }
        public bool Archived { get; set; }

        public string InAppTitle { get; set; }
        public string InAppDescription { get; set; }
        public string ThumbnailUrl { get; set; }
        public string TrailerUrl { get; set; }
        public string TrailerContentType { get; set; }
        public string TeacherName { get; set; }
        public string TeacherImageUrl { get; set; }
        public string TeacherDescription { get; set; }

        public string CurrentVersionId { get; set; }
        public string CurrentVersion { get; set; }
        public string CurrentVersionState { get; set; }

        public bool ArchiveEnabled { get; set; }
        public bool UnarchiveEnabled { get; set; }

        public bool ContentManagementEnabled { get; set; }
        public bool AdministerNewVersionEnabled { get; set; }

        // Version development action parameters
        public bool StartDevelopmentEnabled { get; set; }
        public string StartDevelopmentActionId { get; set; }
        public bool CompleteDevelopmentEnabled { get; set; }
        public string CompleteDevelopmentActionId { get; set; }
        public bool ApproveDevelopmentEnabled { get; set; }
        public string ApproveDevelopmentActionId { get; set; }
        public bool RejectDevelopmentEnabled { get; set; }
        public string RejectDevelopmentActionId { get; set; }
        public bool StartContentPreparationEnabled { get; set; }
        public string StartContentPreparationActionId { get; set; }
        public bool CompleteContentPreparationEnabled { get; set; }
        public string CompleteContentPreparationActionId { get; set; }
        public bool PassContentPreparationEnabled { get; set; }
        public string PassContentPreparationActionId { get; set; }
        public bool FailContentPreparationEnabled { get; set; }
        public string FailContentPreparationActionId { get; set; }
        public bool VersionProductionCancelEnabled { get; set; }
        public string VersionProductionCancelActionId { get; set; }

        private string ThumbnailBucket;
        private string ThumbnailFilepath;
        private string TrailerFileBucket;
        private string TrailerFilepath;
        private string TeacherImageBucket;
        private string TeacherImageFilepath;

        public string GetThumbnailBucket()
        {
            return ThumbnailBucket;
        }
        public string GetThumbnailFilePath()
        {
            return ThumbnailFilepath;
        }
        public string GetTrailerFileBucket()
        {
            return TrailerFileBucket;
        }
        public string GetTrailerFilepath()
        {
            return TrailerFilepath;
        }
        public string GetTeacherImageBucket()
        {
            return TeacherImageBucket;
        }
        public string GetTeacherImageFilepath()
        {
            return TeacherImageFilepath;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Course, CourseDetailVm>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.CourseId))
                .ForMember(d => d.Culture, opt => opt.MapFrom(s => s.Culture.ToString()))
                .ForMember(d => d.Grade, opt => opt.MapFrom(s => s.Grade.ToString()))
                .ForMember(d => d.InAppTitle, opt => opt.MapFrom(s => s.AppTitle))
                .ForMember(d => d.InAppDescription, opt => opt.MapFrom(s => s.AppDescription))
                .ForMember(d => d.ThumbnailBucket, opt => opt.MapFrom(s => s.AppThumbnail.BucketName))
                .ForMember(d => d.ThumbnailFilepath, opt => opt.MapFrom(s => s.AppThumbnail.FilePath))
                .ForMember(d => d.TrailerFileBucket, opt => opt.MapFrom(s => s.AppTrailer.BucketName))
                .ForMember(d => d.TrailerFilepath, opt => opt.MapFrom(s => s.AppTrailer.FilePath))
                .ForMember(d => d.TrailerContentType, opt => opt.MapFrom(s => s.AppTrailer.ContentTypeMetadata))
                .ForMember(d => d.TeacherImageBucket, opt => opt.MapFrom(s => s.TeacherImage.BucketName))
                .ForMember(d => d.TeacherImageFilepath, opt => opt.MapFrom(s => s.TeacherImage.FilePath))
                .ForMember(d => d.CurrentVersion, opt => opt.MapFrom(s => s.CurrentVersion.VersionNumber.ToString() + "." + s.CurrentVersion.VersionName))
                .ForMember(d => d.CurrentVersionState, opt => opt.MapFrom(s => s.CurrentVersion.CurrentState.ToString()))
                .ForMember(d => d.ContentManagementEnabled, opt => opt.MapFrom(s => s.CurrentVersion.CurrentState == CourseVersionProductionStates.InDevelopment && s.CurrentVersion.RequestActions.Any(a => a.Action == CourseVersionProductionProcessActions.CompleteDevelopment && a.Active && !a.Archived)))
                .ForMember(d => d.AdministerNewVersionEnabled, opt => opt.MapFrom(s => s.CurrentVersion.CurrentState == CourseVersionProductionStates.Cancelled || s.CurrentVersion.CurrentState == CourseVersionProductionStates.Released))
                .ForMember(d => d.StartDevelopmentEnabled, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.Any(a => a.Action == CourseVersionProductionProcessActions.StartDevelopment && a.Active && !a.Archived)))
                .ForMember(d => d.StartDevelopmentActionId, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.First(a => a.Action == CourseVersionProductionProcessActions.StartDevelopment && a.Active && !a.Archived).RequestActionId))
                .ForMember(d => d.CompleteDevelopmentEnabled, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.Any(a => a.Action == CourseVersionProductionProcessActions.CompleteDevelopment && a.Active && !a.Archived)))
                .ForMember(d => d.CompleteDevelopmentActionId, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.First(a => a.Action == CourseVersionProductionProcessActions.CompleteDevelopment && a.Active && !a.Archived).RequestActionId))
                .ForMember(d => d.RejectDevelopmentEnabled, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.Any(a => a.Action == CourseVersionProductionProcessActions.RejectReview && a.Active && !a.Archived)))
                .ForMember(d => d.RejectDevelopmentActionId, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.First(a => a.Action == CourseVersionProductionProcessActions.RejectReview && a.Active && !a.Archived).RequestActionId))
                .ForMember(d => d.ApproveDevelopmentEnabled, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.Any(a => a.Action == CourseVersionProductionProcessActions.ApproveReview && a.Active && !a.Archived)))
                .ForMember(d => d.ApproveDevelopmentActionId, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.First(a => a.Action == CourseVersionProductionProcessActions.ApproveReview && a.Active && !a.Archived).RequestActionId))
                .ForMember(d => d.StartContentPreparationEnabled, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.Any(a => a.Action == CourseVersionProductionProcessActions.StartContentPreparation && a.Active && !a.Archived)))
                .ForMember(d => d.StartContentPreparationActionId, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.First(a => a.Action == CourseVersionProductionProcessActions.StartContentPreparation && a.Active && !a.Archived).RequestActionId))
                .ForMember(d => d.CompleteContentPreparationEnabled, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.Any(a => a.Action == CourseVersionProductionProcessActions.CompleteContentPreparation && a.Active && !a.Archived)))
                .ForMember(d => d.CompleteContentPreparationActionId, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.First(a => a.Action == CourseVersionProductionProcessActions.CompleteContentPreparation && a.Active && !a.Archived).RequestActionId))
                .ForMember(d => d.PassContentPreparationEnabled, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.Any(a => a.Action == CourseVersionProductionProcessActions.PassContentPreparation && a.Active && !a.Archived)))
                .ForMember(d => d.PassContentPreparationActionId, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.First(a => a.Action == CourseVersionProductionProcessActions.PassContentPreparation && a.Active && !a.Archived).RequestActionId))
                .ForMember(d => d.FailContentPreparationEnabled, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.Any(a => a.Action == CourseVersionProductionProcessActions.FailContentPreparation && a.Active && !a.Archived)))
                .ForMember(d => d.FailContentPreparationActionId, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.First(a => a.Action == CourseVersionProductionProcessActions.FailContentPreparation && a.Active && !a.Archived).RequestActionId))
                .ForMember(d => d.VersionProductionCancelEnabled, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.Any(a => a.Action == CourseVersionProductionProcessActions.Cancel && a.Active && !a.Archived)))
                .ForMember(d => d.VersionProductionCancelActionId, opt => opt.MapFrom(s => s.CurrentVersion.RequestActions.First(a => a.Action == CourseVersionProductionProcessActions.Cancel && a.Active && !a.Archived).RequestActionId));
        }
    }
}
