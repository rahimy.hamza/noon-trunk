﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NoonOnline.Application.Common.Exceptions;
using NoonOnline.Application.Common.Interfaces;
using NoonOnline.Domain.Entities.Courses;
using NoonOnline.Domain.Entities.Files;
using NoonOnline.Domain.Enums.Courses;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.Courses.Commands.SetCourseThumbnail
{
    public class SetCourseThumbnailCommand : IRequest
    {
        public string CourseId { get; set; }
        public string FileId { get; set; }
    }

    public class Handler : IRequestHandler<SetCourseThumbnailCommand>
    {
        private readonly INoonDbContext _context;
        private readonly IMediator _mediator;

        public Handler(INoonDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }
        public async Task<Unit> Handle(SetCourseThumbnailCommand request, CancellationToken cancellationToken)
        {
            var course = await _context.Courses
                .Where(e => e.CourseId == request.CourseId)
                .Include(e => e.CurrentVersion).ThenInclude(c => c.RequestActions)
                .Include(e => e.AppThumbnail)
                .FirstOrDefaultAsync(cancellationToken);

            var file = await _context.BucketFiles
                .Where(e => e.BucketFileId == request.FileId)
                .FirstOrDefaultAsync(cancellationToken);

            if (course == null)
            {
                throw new NotFoundException(nameof(Course), request.CourseId);
            }
            else if (course.CurrentVersion.CurrentState != CourseVersionProductionStates.InDevelopment && !course.CurrentVersion.RequestActions.Any(a => a.Action == CourseVersionProductionProcessActions.CompleteDevelopment && a.Active))
            {
                throw new BadRequestException($"[NOT PERMITTED] Course {course.CourseId} must be in-development in order to set thumbnail");
            }

            if (file == null)
                throw new NotFoundException(nameof(BucketFile), request.FileId);

            file.Uploaded = true;

            if (course.AppThumbnailId != null)
                course.AppThumbnail.Archived = true;

            course.AppThumbnailId = file.BucketFileId;

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
