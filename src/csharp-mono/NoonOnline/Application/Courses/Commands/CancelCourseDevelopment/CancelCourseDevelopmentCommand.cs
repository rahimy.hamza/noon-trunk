﻿using MediatR;
using NoonOnline.Application.Common.Exceptions;
using NoonOnline.Application.Common.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.Courses.Commands.CancelCourseDevelopment
{
    public class CancelCourseDevelopmentCommand : IRequest
    {
        public string CourseId { get; set; }
        public string ActionId { get; set; }
    }

    public class Handler : IRequestHandler<CancelCourseDevelopmentCommand>
    {
        private readonly INoonDbContext _context;
        private readonly IMediator _mediator;

        public Handler(INoonDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }
        public async Task<Unit> Handle(CancelCourseDevelopmentCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
