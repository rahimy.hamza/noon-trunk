﻿using MediatR;
using NoonOnline.Application.Common.Interfaces;
using NoonOnline.Domain.Constants;
using NoonOnline.Domain.Entities.Files;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.Courses.Commands.CreateCourseTrailerUploadUrl
{
    public class CreateCourseTrailerUploadUrlCommand : IRequest<CourseTrailerCreatedVm>
    {
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public int SizeInBytes { get; set; }
    }

    public class Handler : IRequestHandler<CreateCourseTrailerUploadUrlCommand, CourseTrailerCreatedVm>
    {
        private readonly INoonDbContext _context;
        private readonly IBucketStorageService _storageService;
        private readonly IMediator _mediator;

        public Handler(INoonDbContext context, IBucketStorageService storageService, IMediator mediator)
        {
            _context = context;
            _storageService = storageService;
            _mediator = mediator;
        }

        public async Task<CourseTrailerCreatedVm> Handle(CreateCourseTrailerUploadUrlCommand request, CancellationToken cancellationToken)
        {
            var fileId = Guid.NewGuid().ToString();
            var bucketFileUri = "course-trailers/" + fileId;

            var signedUrl = await _storageService
                .CreateUploadSignedUrl(StorageBuckets.RawMediaStorage, bucketFileUri, request.ContentType, cancellationToken);

            var entity = new BucketFile
            {
                BucketFileId = fileId,
                BucketName = StorageBuckets.RawMediaStorage,
                FilePath = bucketFileUri,
                OriginalFileName = request.FileName,
                ContentTypeMetadata = request.ContentType,
                SizeInBytes = request.SizeInBytes
            };

            _context.BucketFiles.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            var vm = new CourseTrailerCreatedVm
            {
                FileId = fileId,
                SignedUrl = signedUrl
            };

            return vm;
        }
    }
}
