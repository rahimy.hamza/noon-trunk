﻿namespace NoonOnline.Application.Courses.Commands.CreateCourseTrailerUploadUrl
{
    public class CourseTrailerCreatedVm
    {
        public string FileId { get; set; }
        public string SignedUrl { get; set; }
    }
}
