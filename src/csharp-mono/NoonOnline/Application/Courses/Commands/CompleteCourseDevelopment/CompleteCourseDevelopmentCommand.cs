﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NoonOnline.Application.Common.Exceptions;
using NoonOnline.Application.Common.Interfaces;
using NoonOnline.Domain.Constants;
using NoonOnline.Domain.Entities.Courses;
using NoonOnline.Domain.Entities.Messaging;
using NoonOnline.Domain.Enums.Courses;
using NoonOnline.Domain.Enums.Messaging;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.Courses.Commands.CompleteCourseDevelopment
{
    public class CompleteCourseDevelopmentCommand : IRequest
    {
        public string CourseId { get; set; }
        public string ActionId { get; set; }
    }

    public class Handler : IRequestHandler<CompleteCourseDevelopmentCommand>
    {
        private readonly INoonDbContext _context;
        private readonly IMediator _mediator;

        public Handler(INoonDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(CompleteCourseDevelopmentCommand request, CancellationToken cancellationToken)
        {
            var course = await _context.Courses
                .Where(e => e.CourseId == request.CourseId && e.CurrentVersion.RequestActions.Any(a => a.RequestActionId == request.ActionId))
                .Include(e => e.CurrentVersion).ThenInclude(e => e.RequestActions)
                .FirstOrDefaultAsync(cancellationToken);
            var requestAction = await _context.CourseVersionProductionRequestActions
                .Include(e => e.Transition).ThenInclude(e => e.TransitionActions)
                .SingleOrDefaultAsync(e => e.RequestActionId == request.ActionId);

            if (course == null)
                throw new NotFoundException(nameof(Course), request.CourseId);
            if (requestAction.Action != CourseVersionProductionProcessActions.CompleteDevelopment)
                throw new MethodNotAllowedException(requestAction.Action.ToString(), requestAction.RequestActionId, $"Action must be of type \"{CourseVersionProductionProcessActions.CompleteDevelopment.ToString()}\"");
            if (requestAction.Complete)
                throw new MethodNotAllowedException(requestAction.Action.ToString(), requestAction.RequestActionId, "[ACTION-COMPLETE] Only incomplete actions may be executed");
            if (!requestAction.Active)
                throw new MethodNotAllowedException(requestAction.Action.ToString(), requestAction.RequestActionId, "[ACTION-INACTIVE] Only active actions may be executed");

            // Complete and disable the executing action
            requestAction.Active = false;
            requestAction.Archived = true;
            requestAction.Complete = true;

            // Enable next actions
            var validTransitionIds = CourseProductionTransitions.StaticCourseVersionProductionTransitions
                .Where(e => e.CurrentState == requestAction.Transition.CurrentState).Select(e => e.TransitionId);
            var nextActions = course.CurrentVersion.RequestActions
                .Where(e => e.RequestActionId != requestAction.RequestActionId && validTransitionIds.Contains(e.TransitionId) && !e.Archived);

            foreach (var action in nextActions)
            {
                action.Active = true;
                action.Complete = false;
            }

            _context.Messages.Add(new Message
            {
                CourseId = course.CourseId,
                CourseVersionId = course.CurrentVersionId,
                Title = CourseVersionProductionProcessActions.CompleteDevelopment.ToString(),
                Description = "Course version production workflow action note",
                MessageText = $"Action \"{CourseVersionProductionProcessActions.CompleteDevelopment.ToString()}\" ({requestAction.RequestActionId}) was completed",
                Permanent = true,
                Type = MessageTypes.CourseAdminNote
            });

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
