﻿namespace NoonOnline.Application.Courses.Commands.CreateTeacherImageUploadUrl
{
    public class TeacherImageCreatedVm
    {
        public string FileId { get; set; }
        public string SignedUrl { get; set; }
    }
}
