﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NoonOnline.Application.Common.Interfaces;
using NoonOnline.Domain.Entities.Courses;
using NoonOnline.Domain.Enums.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.Courses.Commands.CreateCourse
{
    public class CreateCourseCommand : IRequest<string>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public CourseCultures Culture { get; set; }
        public CourseGrades Grade { get; set; }
        public bool AvailableInTrial { get; set; }
        public string InAppTitle { get; set; }
        public string InAppDescription { get; set; }
        public string ThumbnailFileId { get; set; }
        public string TrailerFileId { get; set; }
        public string TeacherName { get; set; }
        public string TeacherImageFileId { get; set; }
        public string TeacherDescription { get; set; }

        public class Handler : IRequestHandler<CreateCourseCommand, string>
        {
            private readonly INoonDbContext _context;
            private readonly IMediator _mediator;

            public Handler(INoonDbContext context, IMediator mediator)
            {
                _context = context;
                _mediator = mediator;
            }

            public async Task<string> Handle(CreateCourseCommand request, CancellationToken cancellationToken)
            {
                var newCourse = new Course
                {
                    Name = request.Name,
                    Description = request.Description,
                    Culture = request.Culture,
                    Grade = request.Grade,
                    AvailableInTrial = request.AvailableInTrial,
                    AppTitle = request.InAppTitle,
                    AppDescription = request.InAppDescription,
                    AppThumbnailId = request.ThumbnailFileId,
                    AppTrailerId = request.TrailerFileId,
                    TeacherName = request.TeacherName,
                    TeacherDescription = request.TeacherDescription,
                    TeacherImageId = request.TeacherImageFileId
                };

                newCourse.CurrentVersion = new CourseVersion
                {
                    Name = request.Name,
                    Description = request.Description,
                    Culture = request.Culture,
                    Grade = request.Grade,
                    AvailableInTrial = request.AvailableInTrial,
                    VersionNumber = 1,
                    VersionName = "course-initial",
                    AppTitle = request.InAppTitle,
                    AppDescription = request.InAppDescription,
                    AppThumbnailId = request.ThumbnailFileId,
                    AppTrailerId = request.TrailerFileId,
                    TeacherName = request.TeacherName,
                    TeacherDescription = request.TeacherDescription,
                    TeacherImageId = request.TeacherImageFileId,
                    CurrentState = CourseVersionProductionStates.Started
                };

                var newAddedCourse = _context.Courses.Add(newCourse);

                await _context.SaveChangesAsync(cancellationToken);

                newAddedCourse.Entity.CurrentVersion.CourseId = newAddedCourse.Entity.CourseId;

                await _context.SaveChangesAsync(cancellationToken);

                var startedTransitionActions = await _context.CourseVersionProductionTransitionActions
                    .Where(e => e.Transition.CurrentState == CourseVersionProductionStates.Started)
                    .ToListAsync();

                var startedRequestActions = new List<CourseVersionProductionRequestAction>();

                foreach (var transitionAction in startedTransitionActions)
                {
                    startedRequestActions.Add(new CourseVersionProductionRequestAction
                    {
                        Action = transitionAction.Action,
                        TransitionId = transitionAction.TransitionId,
                        Active = true,
                        Complete = false,
                        CourseVersionId = newAddedCourse.Entity.CurrentVersionId
                    });
                }

                _context.CourseVersionProductionRequestActions.AddRange(startedRequestActions);

                await _context.SaveChangesAsync(cancellationToken);

                return newCourse.CourseId;
            }
        }
    }
}
