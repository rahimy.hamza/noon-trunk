﻿using FluentValidation;

namespace NoonOnline.Application.Courses.Commands.CreateCourse
{
    public class CreateCourseCommandValidator : AbstractValidator<CreateCourseCommand>
    {
        public CreateCourseCommandValidator()
        {
            RuleFor(x => x.Name).MinimumLength(3).MaximumLength(250).NotEmpty().NotNull();
        }
    }
}
