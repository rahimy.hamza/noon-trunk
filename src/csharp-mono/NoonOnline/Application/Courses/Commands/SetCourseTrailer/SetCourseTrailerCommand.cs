﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NoonOnline.Application.Common.Exceptions;
using NoonOnline.Application.Common.Interfaces;
using NoonOnline.Domain.Entities.Courses;
using NoonOnline.Domain.Entities.Files;
using NoonOnline.Domain.Enums.Courses;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.Courses.Commands.SetCourseTrailer
{
    public class SetCourseTrailerCommand : IRequest
    {
        public string CourseId { get; set; }
        public string FileId { get; set; }
    }

    public class Handler : IRequestHandler<SetCourseTrailerCommand>
    {
        private readonly INoonDbContext _context;
        private readonly IMediator _mediator;

        public Handler(INoonDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }
        public async Task<Unit> Handle(SetCourseTrailerCommand request, CancellationToken cancellationToken)
        {
            var course = await _context.Courses
                .Where(e => e.CourseId == request.CourseId)
                .Include(e => e.CurrentVersion).ThenInclude(c => c.RequestActions)
                .Include(e => e.AppTrailer)
                .FirstOrDefaultAsync(cancellationToken);

            var file = await _context.BucketFiles
                .Where(e => e.BucketFileId == request.FileId)
                .FirstOrDefaultAsync(cancellationToken);

            if (course == null)
            {
                throw new NotFoundException(nameof(Course), request.CourseId);
            }
            else if (course.CurrentVersion.CurrentState != CourseVersionProductionStates.InDevelopment && !course.CurrentVersion.RequestActions.Any(a => a.Action == CourseVersionProductionProcessActions.CompleteDevelopment && a.Active))
            {
                throw new BadRequestException($"[NOT PERMITTED] Course {course.CourseId} must be in-development in order to set trailer");
            }

            if (file == null)
                throw new NotFoundException(nameof(BucketFile), request.FileId);

            file.Uploaded = true;

            if (course.AppTrailerId != null)
                course.AppTrailer.Archived = true;

            course.AppTrailerId = file.BucketFileId;

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
