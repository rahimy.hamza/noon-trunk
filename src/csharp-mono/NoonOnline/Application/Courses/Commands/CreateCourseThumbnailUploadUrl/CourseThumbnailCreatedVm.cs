﻿namespace NoonOnline.Application.Courses.Commands.CreateCourseThumbnailUploadUrl
{
    public class CourseThumbnailCreatedVm
    {
        public string FileId { get; set; }
        public string SignedUrl { get; set; }
    }
}
