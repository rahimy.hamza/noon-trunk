﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using MoreLinq;
using NoonOnline.Application.Common.Exceptions;
using NoonOnline.Application.Common.Interfaces;
using NoonOnline.Domain.Entities.Courses;
using NoonOnline.Domain.Entities.Messaging;
using NoonOnline.Domain.Enums.Courses;
using NoonOnline.Domain.Enums.Messaging;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Application.Courses.Commands.StartCourseDevelopment
{
    public class StartCourseDevelopmentCommand : IRequest
    {
        public string CourseId { get; set; }
        public string ActionId { get; set; }
    }

    public class Handler : IRequestHandler<StartCourseDevelopmentCommand>
    {
        private readonly INoonDbContext _context;
        private readonly IMediator _mediator;

        public Handler(INoonDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(StartCourseDevelopmentCommand request, CancellationToken cancellationToken)
        {
            var course = await _context.Courses
                .Where(e => e.CourseId == request.CourseId && e.CurrentVersion.RequestActions.Any(a => a.RequestActionId == request.ActionId))
                .Include(e => e.CurrentVersion).ThenInclude(e => e.RequestActions)
                .FirstOrDefaultAsync(cancellationToken);
            var requestAction = await _context.CourseVersionProductionRequestActions
                .Include(e => e.Transition).ThenInclude(e => e.TransitionActions)
                .SingleOrDefaultAsync(e => e.RequestActionId == request.ActionId);

            if (course == null)
                throw new NotFoundException(nameof(Course), request.CourseId);
            if (requestAction.Action != CourseVersionProductionProcessActions.StartDevelopment)
                throw new MethodNotAllowedException(requestAction.Action.ToString(), requestAction.RequestActionId, $"Action must be of type \"{CourseVersionProductionProcessActions.StartDevelopment.ToString()}\"");
            if (requestAction.Complete)
                throw new MethodNotAllowedException(requestAction.Action.ToString(), requestAction.RequestActionId, "[ACTION-COMPLETE] Only incomplete actions may be executed");
            if (!requestAction.Active)
                throw new MethodNotAllowedException(requestAction.Action.ToString(), requestAction.RequestActionId, "[ACTION-INACTIVE] Only active actions may be executed");

            // Update current RequestActions
            foreach (var action in course.CurrentVersion.RequestActions)
            {
                action.Active = false;
                action.Archived = true;
            }
            requestAction.Complete = true;

            // Add new actions for the new state
            var nextStateTransitionActions = await _context.CourseVersionProductionTransitionActions
                .Where(e => e.Transition.CurrentState == requestAction.Transition.NextState)
                .ToListAsync(cancellationToken);
            nextStateTransitionActions = nextStateTransitionActions.DistinctBy(e => e.Action).ToList();
            var newStateActions = new List<CourseVersionProductionRequestAction>();

            foreach (var transitionAction in nextStateTransitionActions)
            {
                newStateActions.Add(new CourseVersionProductionRequestAction
                {
                    TransitionId = transitionAction.TransitionId,
                    Action = transitionAction.Action,
                    CourseVersionId = course.CurrentVersionId,
                    Complete = false,
                    Archived = false,
                    Active = transitionAction.Action == CourseVersionProductionProcessActions.CompleteDevelopment || transitionAction.Action == CourseVersionProductionProcessActions.Cancel
                });
            }

            _context.CourseVersionProductionRequestActions.AddRange(newStateActions);

            // Update state
            course.CurrentVersion.CurrentState = CourseVersionProductionStates.InDevelopment;

            _context.Messages.Add(new Message
            {
                CourseId = course.CourseId,
                CourseVersionId = course.CurrentVersionId,
                Title = requestAction.Action.ToString(),
                Description = "Course version production workflow action note",
                MessageText = $"Action \"{requestAction.Action.ToString()}\" ({requestAction.RequestActionId}) was completed. Course version \"{course.CurrentVersionId}\" is now in \"{course.CurrentVersion.CurrentState.ToString()}\" state.",
                Permanent = true,
                Type = MessageTypes.CourseAdminNote
            });

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
