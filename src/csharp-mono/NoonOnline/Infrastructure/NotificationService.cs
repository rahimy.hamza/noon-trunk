﻿using NoonOnline.Application.Common.Interfaces;
using NoonOnline.Application.Notifications.Models;
using System.Threading.Tasks;

namespace NoonOnline.Infrastructure
{
    public class NotificationService : INotificationService
    {
        public Task SendAsync(MessageDto message)
        {
            return Task.CompletedTask;
        }
    }
}
