﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NoonOnline.Application.Common.Interfaces;
using NoonOnline.Common;
using NoonOnline.Infrastructure.BucketStorage;

namespace NoonOnline.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<INotificationService, NotificationService>();
            services.AddTransient<IDateTime, MachineDateTime>();
            services.AddTransient<IBucketStorageService, BucketStorageService>();

            return services;
        }
    }
}
