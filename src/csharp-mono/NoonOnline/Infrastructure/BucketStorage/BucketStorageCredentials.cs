﻿namespace NoonOnline.Infrastructure.BucketStorage
{
    public class BucketStorageCredentials
    {
        public string GoogleApplicationCredentials { get; set; }
        public string BucketNamePrefix { get; set; }
        public string GoogleProjectId { get; set; }
    }
}
