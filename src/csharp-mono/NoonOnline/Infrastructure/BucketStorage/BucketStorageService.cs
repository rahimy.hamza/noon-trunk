﻿using Google.Cloud.Storage.V1;
using Microsoft.Extensions.Options;
using NoonOnline.Application.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace NoonOnline.Infrastructure.BucketStorage
{
    public class BucketStorageService : IBucketStorageService
    {
        public BucketStorageCredentials Options { get; }
        private readonly StorageClient storageClient;

        public BucketStorageService(IOptions<BucketStorageCredentials> optionsAccessor)
        {
            Options = optionsAccessor.Value;
            storageClient = StorageClient.Create();
        }

        public async Task<string> CreateDownloadSignedUrl(string bucketName, string objectName, CancellationToken cancellationToken)
        {
            if (!String.IsNullOrEmpty(Options.BucketNamePrefix))
                bucketName = Options.BucketNamePrefix + "-" + bucketName;

            UrlSigner urlSigner = UrlSigner
                .FromServiceAccountPath(Options.GoogleApplicationCredentials);

            return await urlSigner.SignAsync(bucketName, objectName, TimeSpan.FromHours(2), cancellationToken: cancellationToken);
        }

        public async Task<string> CreateUploadSignedUrl(string bucketName, string objectUri, string contentTypeHeader, CancellationToken cancellationToken)
        {
            bucketName = EnsureBucket(bucketName);

            UrlSigner urlSigner = UrlSigner.FromServiceAccountPath(Options.GoogleApplicationCredentials);

            UrlSigner.RequestTemplate requestTemplate = UrlSigner.RequestTemplate
                .FromBucket(bucketName)
                .WithObjectName(objectUri)
                .WithHttpMethod(HttpMethod.Put)
                .WithContentHeaders(new Dictionary<string, IEnumerable<string>>
                {
                    {"Content-Type", new[] {contentTypeHeader} }
                });

            UrlSigner.Options options = UrlSigner.Options.FromDuration(TimeSpan.FromHours(2));

            return await urlSigner.SignAsync(requestTemplate, options, cancellationToken);
        }

        public async Task<string> CreateUploadSignedUrl(string bucketName, string objectUri,
            Dictionary<string, IEnumerable<string>> contentHeaders, CancellationToken cancellationToken)
        {
            bucketName = EnsureBucket(bucketName);

            UrlSigner urlSigner = UrlSigner.FromServiceAccountPath(Options.GoogleApplicationCredentials);

            UrlSigner.RequestTemplate requestTemplate = UrlSigner.RequestTemplate
                .FromBucket(bucketName)
                .WithObjectName(objectUri)
                .WithHttpMethod(HttpMethod.Put)
                .WithContentHeaders(contentHeaders);

            UrlSigner.Options options = UrlSigner.Options.FromDuration(TimeSpan.FromHours(2));

            return await urlSigner.SignAsync(requestTemplate, options, cancellationToken);
        }

        public async Task DeleteObject(string bucketName, string objectUri, CancellationToken cancellationToken)
        {
            if (!String.IsNullOrEmpty(Options.BucketNamePrefix))
                bucketName = Options.BucketNamePrefix + "-" + bucketName;

            await storageClient
                .DeleteObjectAsync(bucketName, objectUri, cancellationToken: cancellationToken);
        }

        private string EnsureBucket(string bucketName)
        {
            if (!String.IsNullOrEmpty(Options.BucketNamePrefix))
                bucketName = Options.BucketNamePrefix + "-" + bucketName;

            if (storageClient.GetBucket(bucketName) == null)
                storageClient.CreateBucket(Options.GoogleProjectId, bucketName);

            return bucketName;
        }
    }
}
