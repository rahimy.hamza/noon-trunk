﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NoonOnline.Application.Courses.Commands.ApproveCourseDevelopment;
using NoonOnline.Application.Courses.Commands.CancelCourseDevelopment;
using NoonOnline.Application.Courses.Commands.CompleteCourseDevelopment;
using NoonOnline.Application.Courses.Commands.CreateCourse;
using NoonOnline.Application.Courses.Commands.CreateCourseThumbnailUploadUrl;
using NoonOnline.Application.Courses.Commands.CreateCourseTrailerUploadUrl;
using NoonOnline.Application.Courses.Commands.CreateTeacherImageUploadUrl;
using NoonOnline.Application.Courses.Commands.RejectCourseDevelopment;
using NoonOnline.Application.Courses.Commands.SetCourseThumbnail;
using NoonOnline.Application.Courses.Commands.SetCourseTrailer;
using NoonOnline.Application.Courses.Commands.SetTeacherImage;
using NoonOnline.Application.Courses.Commands.StartCourseContentPreparation;
using NoonOnline.Application.Courses.Commands.StartCourseDevelopment;
using NoonOnline.Application.Courses.Queries.GetCourseDetail;
using NoonOnline.Application.Courses.Queries.GetCourseNotes;
using NoonOnline.Application.Courses.Queries.GetCoursesList;
using System.Threading.Tasks;

namespace NoonOnline.REST.Courses.Controllers
{
    [Authorize]
    public class CoursesController : BaseController
    {

        [HttpGet]
        public async Task<ActionResult<CourseListVm>> GetAll([FromQuery] GetCoursesListQuery query)
        {
            var vm = await Mediator.Send(query);

            return Ok(vm);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CourseDetailVm>> Get(string id)
        {
            var vm = await Mediator.Send(new GetCourseDetailQuery { Id = id });

            return base.Ok(vm);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<NoteListVm>> GetCourseNotes(string id, [FromQuery] int page)
        {
            var vm = await Mediator.Send(new GetCourseNotesQuery { CourseId = id, Page = page });

            return Ok(vm);
        }

        [HttpPost]
        public async Task<ActionResult<string>> Create([FromBody] CreateCourseCommand command)
        {
            var courseId = await Mediator.Send(command);

            return Ok(courseId);
        }

        [HttpPost]
        public async Task<ActionResult<CourseThumbnailCreatedVm>> CreateThumbnailUploadUrl([FromBody] CreateCourseThumbnailUploadUrlCommand command)
        {
            var vm = await Mediator.Send(command);

            return Ok(vm);
        }

        [HttpPost]
        public async Task<ActionResult<CourseTrailerCreatedVm>> CreateCourseTrailerUploadUrl([FromBody] CreateCourseTrailerUploadUrlCommand command)
        {
            var vm = await Mediator.Send(command);

            return Ok(vm);
        }

        [HttpPost]
        public async Task<ActionResult<TeacherImageCreatedVm>> CreateTeacherImageUploadUrl([FromBody] CreateTeacherImageUploadUrlCommand command)
        {
            var vm = await Mediator.Send(command);

            return Ok(vm);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> SetCourseThumbnail(string id, [FromQuery] string fileId)
        {
            await Mediator.Send(new SetCourseThumbnailCommand { CourseId = id, FileId = fileId });

            return NoContent();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> SetCourseTrailer(string id, [FromQuery] string fileId)
        {
            await Mediator.Send(new SetCourseTrailerCommand { CourseId = id, FileId = fileId });

            return NoContent();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> SetTeacherImage(string id, [FromQuery] string fileId)
        {
            await Mediator.Send(new SetTeacherImageCommand { CourseId = id, FileId = fileId });

            return NoContent();
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status405MethodNotAllowed)]
        public async Task<IActionResult> StartCourseDevelopment(string id, [FromQuery] string actionId)
        {
            await Mediator.Send(new StartCourseDevelopmentCommand { CourseId = id, ActionId = actionId });

            return NoContent();
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status405MethodNotAllowed)]
        public async Task<IActionResult> CompleteCourseDevelopment(string id, [FromQuery] string actionId)
        {
            await Mediator.Send(new CompleteCourseDevelopmentCommand { CourseId = id, ActionId = actionId });

            return NoContent();
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status405MethodNotAllowed)]
        public async Task<IActionResult> ApproveCourseDevelopment(string id, [FromQuery] string actionId)
        {
            await Mediator.Send(new ApproveCourseDevelopmentCommand { CourseId = id, ActionId = actionId });

            return NoContent();
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status405MethodNotAllowed)]
        public async Task<IActionResult> RejectCourseDevelopment(string id, [FromQuery] string actionId)
        {
            await Mediator.Send(new RejectCourseDevelopmentCommand { CourseId = id, ActionId = actionId });

            return NoContent();
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status405MethodNotAllowed)]
        public async Task<IActionResult> CancelCourseDevelopment(string id, [FromQuery] string actionId)
        {
            await Mediator.Send(new CancelCourseDevelopmentCommand { CourseId = id, ActionId = actionId });

            return NoContent();
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status405MethodNotAllowed)]
        public async Task<IActionResult> StartCourseContentPreparation(string id, [FromQuery] string actionId)
        {
            await Mediator.Send(new StartCourseContentPreparationCommand { CourseId = id, ActionId = actionId });

            return NoContent();
        }
    }
}
