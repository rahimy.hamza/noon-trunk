﻿using Microsoft.AspNetCore.Mvc;
using NoonOnline.Application.System.Queries.GetTypeInfo;
using System.Threading.Tasks;

namespace NoonOnline.REST.Courses.Controllers
{
    public class SystemController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult<TypeInfoListVm>> GetTypeInfo()
        {
            var vm = await Mediator.Send(new GetTypeInfoQuery());
            return vm;
        }
    }
}
