﻿using NoonOnline.Domain.Enums.Courses;

namespace NoonOnline.Domain.Entities.System
{
    public class TypeInfo
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
        public int TypeValue { get; set; }
        public CourseContentTypes CourseContentType { get; set; }
        public CourseCultures CourseCulture { get; set; }
        public CourseGrades CourseGrade { get; set; }
    }
}
