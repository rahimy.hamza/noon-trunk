﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Courses;
using NoonOnline.Domain.Entities.Files;
using NoonOnline.Domain.Entities.Students;
using NoonOnline.Domain.Enums.Messaging;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Messaging
{
    public class Message : AuditableEntity
    {
        public Message()
        {
            TopicComments = new HashSet<Message>();
            Reactions = new HashSet<MessageReaction>();
            Attachments = new HashSet<BucketFile>();
        }

        public string MessageId { get; set; }
        public MessageTypes Type { get; set; }
        public string CommentInTopicWithId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string MessageText { get; set; }
        public bool Permanent { get; set; }
        public string CourseId { get; set; }
        public string CourseVersionId { get; set; }
        public string AssignmentSubmissionId { get; set; }

        public Message Topic { get; set; }
        public Course Course { get; set; }
        public CourseVersion CourseVersion { get; set; }
        public AssignmentSubmission AssignmentSubmission { get; set; }
        public ICollection<Message> TopicComments { get; private set; }
        public ICollection<MessageReaction> Reactions { get; private set; }
        public ICollection<BucketFile> Attachments { get; private set; }
    }
}
