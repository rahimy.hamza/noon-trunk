﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Enums.Messaging;
using System;

namespace NoonOnline.Domain.Entities.Messaging
{
    public class MessageReaction : AuditableEntity
    {
        public string MessageReactionId { get; set; }
        public string MessageId { get; set; }
        public MessageReactionTypes Type { get; set; }

        public bool Removed { get; set; }
        public DateTime? RemovedOn { get; set; }

        public Message Message { get; set; }
    }
}
