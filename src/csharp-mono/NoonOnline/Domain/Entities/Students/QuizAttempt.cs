﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Courses;
using System;

namespace NoonOnline.Domain.Entities.Students
{
    public class QuizAttempt : AuditableEntity
    {
        public string QuizAttemptId { get; set; }
        public string CourseContentId { get; set; }
        public string CourseContentVersionId { get; set; }
        public bool Passed { get; set; }
        public int AchievedMarks { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public decimal AttemptDuration { get; set; }

        public CourseContent CourseContent { get; set; }
        public CourseContentVersion CourseContentVersion { get; set; }
    }
}
