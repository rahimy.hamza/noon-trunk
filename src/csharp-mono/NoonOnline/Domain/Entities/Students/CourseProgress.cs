﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Domain.Entities.Students
{
    public class CourseProgress : AuditableEntity
    {
        public string CourseProgressId { get; set; }
        public string CourseId { get; set; }
        public string CourseVersionId { get; set; }
        public decimal ProgressPercentage { get; set; }

        public Course Course { get; set; }
        public CourseVersion CourseVersion { get; set; }
    }
}
