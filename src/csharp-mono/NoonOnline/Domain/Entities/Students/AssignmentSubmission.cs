﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Courses;
using NoonOnline.Domain.Entities.Files;
using NoonOnline.Domain.Entities.Messaging;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Students
{
    public class AssignmentSubmission : AuditableEntity
    {
        public AssignmentSubmission()
        {
            SubmittedFiles = new HashSet<BucketFile>();
            Messages = new HashSet<Message>();
        }

        public string AssignmentSubmissionId { get; set; }
        public string CourseContentId { get; set; }
        public string CourseContentVersionId { get; set; }
        public string CourseId { get; set; }
        public bool Graded { get; set; }
        public string GradedBy { get; set; }
        public DateTime? GradedOn { get; set; }
        public int AssignedMarks { get; set; }

        public CourseContent CourseContent { get; set; }
        public CourseContentVersion CourseContentVersion { get; set; }
        public ICollection<BucketFile> SubmittedFiles { get; private set; }
        public ICollection<Message> Messages { get; private set; }
    }
}
