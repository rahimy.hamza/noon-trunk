﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Courses;

namespace NoonOnline.Domain.Entities.Students
{
    public class CourseContentProgress : AuditableEntity
    {
        public string CourseContentProgressId { get; set; }
        public string CourseContentId { get; set; }
        public string CourseContentVersionId { get; set; }
        public decimal ProgressPercentage { get; set; }

        public CourseContent CourseContent { get; set; }
        public CourseContentVersion CourseContentVersion { get; set; }
    }
}
