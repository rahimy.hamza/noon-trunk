﻿using NoonOnline.Domain.Common;
using System;

namespace NoonOnline.Domain.Entities.Courses
{
    public class PackageCourse : AuditableEntity
    {
        public string CourseId { get; set; }
        public string PackageId { get; set; }
        public string CurrentVersionId { get; set; }

        public bool Removed { get; set; }
        public DateTime? RemovedAt { get; set; }

        public Course Course { get; set; }
        public Package Package { get; set; }
        public PackageCourseVersion CurrentVersion { get; set; }
    }
}
