﻿using NoonOnline.Domain.Common;
using System;

namespace NoonOnline.Domain.Entities.Courses
{
    public class PackageCourseVersion : AuditableEntity
    {
        public string PackageVersionId { get; set; }
        public string CourseId { get; set; }
        public string PackageId { get; set; }

        public bool Removed { get; set; }
        public DateTime? RemovedAt { get; set; }

        public Course Course { get; set; }
        public PackageVersion PackageVersion { get; set; }
        public Package Package { get; set; }
    }
}
