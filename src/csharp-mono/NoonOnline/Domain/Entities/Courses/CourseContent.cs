﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Files;
using NoonOnline.Domain.Entities.Students;
using NoonOnline.Domain.Enums.Courses;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Courses
{
    public class CourseContent : AuditableEntity
    {
        public CourseContent()
        {
            ChapterContents = new HashSet<CourseContent>();
            SupportingResources = new HashSet<BucketFile>();
            Versions = new HashSet<CourseContentVersion>();
            QuizContent = new HashSet<QuizContent>();
            QuizAttempts = new HashSet<QuizAttempt>();
            Progressions = new HashSet<CourseContentProgress>();
            AssignmentSubmissions = new HashSet<AssignmentSubmission>();
        }

        public string CourseContentId { get; set; }
        public CourseContentTypes ContentType { get; set; }
        public string CourseId { get; set; }
        public string InChapterWithId { get; set; }
        public int SequencePosition { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string TranscriptFileId { get; set; }
        public string PdfFileId { get; set; }
        public string VideoFileId { get; set; }
        public bool Optional { get; set; }
        public bool Archived { get; set; }

        // The total marks that users will need to achieve in order to pass quizzes or assignments
        // Only used if object is a Quiz or Assignment.
        public int? TotalMarks { get; set; }
        public int? PassingMarks { get; set; }
        public decimal Weight { get; set; }
        public int? AttemptLimit { get; set; }

        public string CurrentVersionId { get; set; }

        public CourseContent Chapter { get; set; }
        public ICollection<CourseContent> ChapterContents { get; private set; }
        public Course Course { get; set; }
        public BucketFile TranscriptFile { get; set; }
        public BucketFile PdfFile { get; set; }
        public BucketFile VideoFile { get; set; }
        public ICollection<BucketFile> SupportingResources { get; private set; }
        public ICollection<CourseContentVersion> Versions { get; private set; }
        public ICollection<QuizContent> QuizContent { get; private set; }
        public ICollection<QuizAttempt> QuizAttempts { get; private set; }
        public ICollection<AssignmentSubmission> AssignmentSubmissions { get; private set; }
        public ICollection<CourseContentProgress> Progressions { get; private set; }
        public CourseContentVersion CurrentVersion { get; set; }
    }
}
