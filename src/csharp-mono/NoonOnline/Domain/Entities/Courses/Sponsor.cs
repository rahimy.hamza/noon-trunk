﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Sales;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Courses
{
    public class Sponsor : AuditableEntity
    {
        public Sponsor()
        {
            Enrollments = new HashSet<Enrollment>();
        }

        public string SponsorId { get; set; }
        public string SponsorName { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string SaasUserGroupId { get; set; }

        public DateTime ValidityStart { get; set; }
        public DateTime ValidityEnd { get; set; }

        public ICollection<Enrollment> Enrollments { get; private set; }
        public UserGroup SaasUserGroup { get; set; }
    }
}
