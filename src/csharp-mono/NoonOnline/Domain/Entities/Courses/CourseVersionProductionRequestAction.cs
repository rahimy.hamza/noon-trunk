﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Enums.Courses;

namespace NoonOnline.Domain.Entities.Courses
{
    public class CourseVersionProductionRequestAction : AuditableEntity
    {
        public string RequestActionId { get; set; }
        public string CourseVersionId { get; set; }
        public CourseVersionProductionProcessActions Action { get; set; }
        public int TransitionId { get; set; }
        public bool Active { get; set; }
        public bool Complete { get; set; }
        public bool Archived { get; set; }

        public CourseVersion CourseVersion { get; set; }
        public CourseVersionProductionTransition Transition { get; set; }
    }
}
