﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Files;
using NoonOnline.Domain.Enums.Courses;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Courses
{
    public class QuizContent : AuditableEntity
    {
        public QuizContent()
        {
            QuestionAnswers = new HashSet<QuizContent>();
            Versions = new HashSet<QuizContentVersion>();
        }

        public string QuizContentId { get; set; }
        public string CourseContentId { get; set; }
        public QuizContentTypes Type { get; set; }
        public string InQuestionWithId { get; set; }
        public string Title { get; set; }
        public string Value { get; set; }
        public string Explanation { get; set; }
        public int SequencePosition { get; set; }
        public string AttachmentFileId { get; set; }

        // Determines whether this is the correct choice to a question
        // Object can only have a non-null value for this if Type is set to Answer
        public bool? CorrectAnswer { get; set; }

        public bool Archived { get; set; }

        public string CurrentVersionId { get; set; }

        public QuizContent Question { get; set; }
        public ICollection<QuizContent> QuestionAnswers { get; private set; }
        public CourseContent CourseContent { get; set; }
        public BucketFile AttachmentFile { get; set; }
        public ICollection<QuizContentVersion> Versions { get; private set; }
        public QuizContentVersion CurrentVersion { get; set; }
    }
}
