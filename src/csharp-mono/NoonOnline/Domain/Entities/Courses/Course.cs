﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Files;
using NoonOnline.Domain.Entities.Messaging;
using NoonOnline.Domain.Entities.Students;
using NoonOnline.Domain.Enums.Courses;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Courses
{
    public class Course : AuditableEntity
    {
        public Course()
        {
            Content = new HashSet<CourseContent>();
            Versions = new HashSet<CourseVersion>();
            Enrollments = new HashSet<Enrollment>();
            Progressions = new HashSet<CourseProgress>();
            Notes = new HashSet<Message>();
            PackageCourses = new HashSet<PackageCourse>();
            PackageCoursesVersion = new HashSet<PackageCourseVersion>();
        }

        public string CourseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public CourseCultures Culture { get; set; }
        public CourseGrades Grade { get; set; }
        public bool AvailableInTrial { get; set; }
        public bool Archived { get; set; }

        // Details of the course that will appear in student app
        public string AppTitle { get; set; }
        public string AppDescription { get; set; }
        public string AppThumbnailId { get; set; }
        public string AppTrailerId { get; set; }
        public string TeacherName { get; set; }
        public string TeacherImageId { get; set; }
        public string TeacherDescription { get; set; }

        public string CurrentVersionId { get; set; }

        public BucketFile AppThumbnail { get; set; }
        public BucketFile AppTrailer { get; set; }
        public BucketFile TeacherImage { get; set; }
        public ICollection<CourseContent> Content { get; private set; }
        public ICollection<CourseVersion> Versions { get; private set; }
        public ICollection<Enrollment> Enrollments { get; private set; }
        public ICollection<CourseProgress> Progressions { get; private set; }
        public ICollection<Message> Notes { get; private set; }
        public ICollection<PackageCourse> PackageCourses { get; private set; }
        public ICollection<PackageCourseVersion> PackageCoursesVersion { get; private set; }
        public CourseVersion CurrentVersion { get; set; }
    }
}
