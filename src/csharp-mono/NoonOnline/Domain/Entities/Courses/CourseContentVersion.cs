﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Files;
using NoonOnline.Domain.Entities.Students;
using NoonOnline.Domain.Enums.Courses;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Courses
{
    public class CourseContentVersion : AuditableEntity
    {
        public CourseContentVersion()
        {
            ChapterVersionContents = new HashSet<CourseContentVersion>();
            SupportingResources = new HashSet<BucketFile>();
            QuizContent = new HashSet<QuizContentVersion>();
            QuizAttempts = new HashSet<QuizAttempt>();
            Progressions = new HashSet<CourseContentProgress>();
            AssignmentSubmissions = new HashSet<AssignmentSubmission>();
        }

        public string CourseContentVersionId { get; set; }
        public CourseContentTypes ContentType { get; set; }
        public string CourseId { get; set; }
        public string CourseVersionId { get; set; }
        public string InChapterVersionWithId { get; set; }
        public string CourseContentId { get; set; }
        public int SequencePosition { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string TranscriptFileId { get; set; }
        public string PdfFileId { get; set; }
        public string VideoFileId { get; set; }
        public bool Optional { get; set; }
        public bool Archived { get; set; }

        // The total marks that users will need to achieve in order to pass quizzes or assignments
        // Only used if object is a Quiz or Assignment.
        public int? PassingMark { get; set; }
        public decimal Weight { get; set; }

        public string PreviousVersionId { get; set; }
        public string NextVersionId { get; set; }

        public CourseContentVersion ChapterVersion { get; set; }
        public ICollection<CourseContentVersion> ChapterVersionContents { get; private set; }
        public Course Course { get; set; }
        public CourseContent CourseContent { get; set; }
        public CourseVersion CourseVersion { get; set; }
        public BucketFile TranscriptFile { get; set; }
        public BucketFile PdfFile { get; set; }
        public BucketFile VideoFile { get; set; }
        public CourseContentVersion PreviousVersion { get; set; }
        public CourseContentVersion NextVersion { get; set; }
        public ICollection<BucketFile> SupportingResources { get; private set; }
        public ICollection<QuizContentVersion> QuizContent { get; private set; }
        public ICollection<QuizAttempt> QuizAttempts { get; private set; }
        public ICollection<CourseContentProgress> Progressions { get; private set; }
        public ICollection<AssignmentSubmission> AssignmentSubmissions { get; private set; }
    }
}
