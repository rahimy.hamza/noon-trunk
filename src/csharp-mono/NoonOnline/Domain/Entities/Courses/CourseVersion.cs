﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Files;
using NoonOnline.Domain.Entities.Messaging;
using NoonOnline.Domain.Entities.Students;
using NoonOnline.Domain.Enums.Courses;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Courses
{
    /***
     * This represent the state and details of a course and all related entities
     * as as they were in a previously published version of a course.
     * ***/
    public class CourseVersion : AuditableEntity
    {
        public CourseVersion()
        {
            Content = new HashSet<CourseContentVersion>();
            Enrollments = new HashSet<Enrollment>();
            Progressions = new HashSet<CourseProgress>();
            RequestActions = new HashSet<CourseVersionProductionRequestAction>();
            Notes = new HashSet<Message>();
        }

        public string CourseVersionId { get; set; }
        public string CourseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public CourseCultures Culture { get; set; }
        public CourseGrades Grade { get; set; }
        public string ChangesFileId { get; set; }
        public bool AvailableInTrial { get; set; }
        public bool Archived { get; set; }

        public int VersionNumber { get; set; }
        public string VersionName { get; set; }
        public string AdministeredBy { get; set; }
        public DateTime AdministeredOn { get; set; }
        public string PublishedBy { get; set; }
        public DateTime PublishedOn { get; set; }
        public string ReleasedBy { get; set; }
        public DateTime? ReleasedOn { get; set; }
        public string PreviousVersionId { get; set; }
        public string NextVersionId { get; set; }

        public string AppTitle { get; set; }
        public string AppDescription { get; set; }
        public string AppThumbnailId { get; set; }
        public string AppTrailerId { get; set; }
        public string TeacherName { get; set; }
        public string TeacherImageId { get; set; }
        public string TeacherDescription { get; set; }
        public string AppCourseManifestFileId { get; set; }

        public CourseVersionProductionStates CurrentState { get; set; }

        public Course Course { get; set; }
        public BucketFile ChangesFile { get; set; }
        public CourseVersion PreviousVersion { get; set; }
        public CourseVersion NextVersion { get; set; }
        public BucketFile AppThumbnail { get; set; }
        public BucketFile AppTrailer { get; set; }
        public BucketFile TeacherImage { get; set; }
        public BucketFile AppCourseManifestFile { get; set; }
        public ICollection<CourseContentVersion> Content { get; private set; }
        public ICollection<Enrollment> Enrollments { get; private set; }
        public ICollection<CourseProgress> Progressions { get; private set; }
        public ICollection<CourseVersionProductionRequestAction> RequestActions { get; private set; }
        public ICollection<Message> Notes { get; private set; }
    }
}
