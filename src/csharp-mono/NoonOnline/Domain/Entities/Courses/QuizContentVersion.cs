﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Files;
using NoonOnline.Domain.Enums.Courses;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Courses
{
    public class QuizContentVersion : AuditableEntity
    {
        public QuizContentVersion()
        {
            QuestionAnswers = new HashSet<QuizContentVersion>();
        }

        public string QuizContentVersionId { get; set; }
        public string CourseContentVersionId { get; set; }
        public string CourseContentId { get; set; }
        public string QuizContentId { get; set; }
        public QuizContentTypes Type { get; set; }
        public string InQuestionVersionWithId { get; set; }
        public string Title { get; set; }
        public string Value { get; set; }
        public string Explanation { get; set; }
        public int Sequence { get; set; }
        public string AttachmentFileId { get; set; }

        // Determines whether this is the correct choice to a question
        // Object can only have a non-null value for this if Type is set to Answer
        public bool? CorrectAnswer { get; set; }

        public bool Archived { get; set; }

        public string PreviousVersionId { get; set; }
        public string NextVersionId { get; set; }

        public QuizContentVersion Question { get; set; }
        public ICollection<QuizContentVersion> QuestionAnswers { get; private set; }
        public CourseContentVersion CourseContentVersion { get; set; }
        public CourseContent CourseContent { get; set; }
        public QuizContent QuizContent { get; set; }
        public BucketFile AttachmentFile { get; set; }
        public QuizContentVersion PreviousVersion { get; set; }
        public QuizContentVersion NextVersion { get; set; }
    }
}
