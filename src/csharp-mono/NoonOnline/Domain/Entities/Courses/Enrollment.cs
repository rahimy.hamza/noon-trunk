﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Sales;
using System;

namespace NoonOnline.Domain.Entities.Courses
{
    public class Enrollment : AuditableEntity
    {
        public string EnrollmentId { get; set; }
        public string AssigneeUserId { get; set; }
        public string AssigneeUserName { get; set; }
        public string AssigneeUserEmail { get; set; }
        public string CourseId { get; set; }
        public string CourseVersionId { get; set; }
        public string SponsorId { get; set; }
        public string SubscriptionGroupMemberId { get; set; }

        public int? FixedDurationMonths { get; set; }
        public int? FixedDurationDays { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public bool Active { get; set; }
        public string ActivatedBy { get; set; }
        public DateTime? ActivatedOn { get; set; }

        public bool Revoked { get; set; }
        public string RevokedBy { get; set; }
        public DateTime? RevokedOn { get; set; }

        public Course Course { get; set; }
        public CourseVersion CourseVersion { get; set; }
        public Sponsor Sponsor { get; set; }
        public InGroup SubscriptionGroupMember { get; set; }
    }
}
