﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Files;
using NoonOnline.Domain.Enums.Courses;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Courses
{
    public class PackageVersion : AuditableEntity
    {
        public string PackageVersionId { get; set; }
        public string PackageId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public CourseCultures Culture { get; set; }
        public CourseGrades? Grade { get; set; }

        public int VersionNumber { get; set; }
        public string VersionName { get; set; }
        public string VersionAdministeredBy { get; set; }
        public DateTime? VersionAdministeredOn { get; set; }
        public string VersionPublishedBy { get; set; }
        public DateTime? VersionPublishedOn { get; set; }
        public string VersionReleasedBy { get; set; }
        public DateTime? VersionReleasedOn { get; set; }

        // Details of the course that will appear in sales app
        public string AppTitle { get; set; }
        public string AppDescription { get; set; }
        public string AppThumbnailId { get; set; }

        public string PreviousVersionId { get; set; }
        public string NextVersionId { get; set; }

        public Package Package { get; set; }
        public BucketFile AppThumbnail { get; set; }
        public ICollection<PackageCourseVersion> PackageCoursesVersion { get; private set; }
        public PackageVersion PreviousVersion { get; set; }
        public PackageVersion NextVersion { get; set; }
    }
}
