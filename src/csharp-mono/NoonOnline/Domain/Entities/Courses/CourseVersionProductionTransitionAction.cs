﻿using NoonOnline.Domain.Enums.Courses;

namespace NoonOnline.Domain.Entities.Courses
{
    public class CourseVersionProductionTransitionAction
    {
        public int TransitionId { get; set; }
        public CourseVersionProductionProcessActions Action { get; set; }

        public CourseVersionProductionTransition Transition { get; set; }
    }
}
