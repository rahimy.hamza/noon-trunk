﻿using NoonOnline.Domain.Enums.Courses;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Courses
{
    public class CourseVersionProductionTransition
    {
        public CourseVersionProductionTransition()
        {
            TransitionActions = new HashSet<CourseVersionProductionTransitionAction>();
            RequestActions = new HashSet<CourseVersionProductionRequestAction>();
        }

        public int TransitionId { get; set; }
        public CourseVersionProductionStates CurrentState { get; set; }
        public CourseVersionProductionStates NextState { get; set; }

        public ICollection<CourseVersionProductionTransitionAction> TransitionActions { get; private set; }
        public ICollection<CourseVersionProductionRequestAction> RequestActions { get; private set; }
    }
}
