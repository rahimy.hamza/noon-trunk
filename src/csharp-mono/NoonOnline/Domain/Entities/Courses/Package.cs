﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Files;
using NoonOnline.Domain.Enums;
using NoonOnline.Domain.Enums.Courses;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Courses
{
    public class Package : AuditableEntity
    {
        public Package()
        {
            PackageCourses = new HashSet<PackageCourse>();
            Versions = new HashSet<PackageVersion>();
        }

        public string PackageId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public CourseCultures Culture { get; set; }
        public CourseGrades? Grade { get; set; }

        public int VersionNumber { get; set; }
        public string VersionName { get; set; }
        public string VersionAdministeredBy { get; set; }
        public DateTime? VersionAdministeredOn { get; set; }
        public string VersionPublishedBy { get; set; }
        public DateTime? VersionPublishedOn { get; set; }
        public string VersionReleasedBy { get; set; }
        public DateTime? VersionReleasedOn { get; set; }

        // Details of the course that will appear in sales app
        public string AppTitle { get; set; }
        public string AppDescription { get; set; }
        public string AppThumbnailId { get; set; }

        public string CurrentVersionId { get; set; }

        public BucketFile AppThumbnail { get; set; }
        public ICollection<PackageCourse> PackageCourses { get; private set; }
        public ICollection<PackageVersion> Versions { get; private set; }
        public PackageVersion CurrentVersion { get; set; }
    }
}
