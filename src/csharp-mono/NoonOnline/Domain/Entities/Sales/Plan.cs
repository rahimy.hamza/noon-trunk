﻿using NoonOnline.Domain.Common;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Sales
{
    public class Plan : AuditableEntity
    {
        public Plan()
        {
            IncludedOptions = new HashSet<OptionIncluded>();
            OfferPrerequisites = new HashSet<Prerequisite>();
            OfferInclusions = new HashSet<Include>();
            Subscriptions = new HashSet<Subscription>();
            SubscriptionPlanHistoryEvents = new HashSet<PlanHistory>();
        }

        public string PlanId { get; set; }
        public string Name { get; set; }
        public int SoftwareId { get; set; }
        public int UserGroupTypeId { get; set; }
        public decimal CurrentPrice { get; set; }
        public DateTime InserTs { get; set; }
        public bool Active { get; set; }

        public Software Software { get; set; }
        public UserGroupType UserGroupType { get; set; }
        public ICollection<OptionIncluded> IncludedOptions { get; private set; }
        public ICollection<Prerequisite> OfferPrerequisites { get; private set; }
        public ICollection<Include> OfferInclusions { get; private set; }
        public ICollection<Subscription> Subscriptions { get; private set; }
        public ICollection<PlanHistory> SubscriptionPlanHistoryEvents { get; private set; }
    }
}
