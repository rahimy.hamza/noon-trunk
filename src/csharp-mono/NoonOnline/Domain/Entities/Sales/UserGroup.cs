﻿using NoonOnline.Domain.Common;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Sales
{
    public class UserGroup : AuditableEntity
    {
        public UserGroup()
        {
            GroupMembers = new HashSet<InGroup>();
            Subscriptions = new HashSet<Subscription>();
        }

        public string UserGroupId { get; set; }
        public int UserGroupTypeId { get; set; }
        public string CustomerInvoiceData { get; set; }
        public DateTime InsertTs { get; set; }

        public UserGroupType UserGroupType { get; set; }
        public ICollection<InGroup> GroupMembers { get; private set; }
        public ICollection<Subscription> Subscriptions { get; private set; }
    }
}
