﻿using NoonOnline.Domain.Common;
using System;

namespace NoonOnline.Domain.Entities.Sales
{
    public class Invoice : AuditableEntity
    {
        public string InvoiceId { get; set; }
        public string CustomerInvoiceData { get; set; }
        public string SubscriptionId { get; set; }
        public string PlanHistoryId { get; set; }
        public DateTime InvoicePeriodStartDate { get; set; }
        public DateTime InvoicePeriodEndDate { get; set; }
        public string InvoiceDescription { get; set; }
        public decimal InvoiceAmount { get; set; }
        public DateTime InvoiceCreatedTs { get; set; }
        public DateTime InvoiceDueTs { get; set; }
        public DateTime? InvoicePaidTs { get; set; }

        public Subscription Subscription { get; set; }
        public PlanHistory PlanHistory { get; set; }
    }
}
