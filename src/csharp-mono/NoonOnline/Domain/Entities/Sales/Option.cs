﻿using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Sales
{
    public class Option
    {
        public Option()
        {
            IncludedOptions = new HashSet<OptionIncluded>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<OptionIncluded> IncludedOptions { get; private set; }
    }
}
