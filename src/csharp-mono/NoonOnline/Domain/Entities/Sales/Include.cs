﻿using NoonOnline.Domain.Common;

namespace NoonOnline.Domain.Entities.Sales
{
    public class Include : AuditableEntity
    {
        public string IncludeId { get; set; }
        public string OfferId { get; set; }
        public string PlanId { get; set; }

        public Offer Offer { get; set; }
        public Plan Plan { get; set; }
    }
}
