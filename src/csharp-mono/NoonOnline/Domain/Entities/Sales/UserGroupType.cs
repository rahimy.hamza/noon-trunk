﻿using NoonOnline.Domain.Common;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Sales
{
    public class UserGroupType : AuditableEntity
    {
        public UserGroupType()
        {
            Groups = new HashSet<UserGroup>();
            Plans = new HashSet<Plan>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int MembersMin { get; set; }
        public int MembersMax { get; set; }

        public ICollection<UserGroup> Groups { get; private set; }
        public ICollection<Plan> Plans { get; private set; }
    }
}
