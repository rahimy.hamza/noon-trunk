﻿using NoonOnline.Domain.Common;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Sales
{
    public class PlanHistory : AuditableEntity
    {
        public PlanHistory()
        {
            Invoices = new HashSet<Invoice>();
        }

        public string PlanHistoryId { get; set; }
        public string SubscriptionId { get; set; }
        public string PlanId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime InsertTs { get; set; }

        public Subscription Subscription { get; set; }
        public Plan Plan { get; set; }
        public ICollection<Invoice> Invoices { get; private set; }
    }
}
