﻿using NoonOnline.Domain.Common;

namespace NoonOnline.Domain.Entities.Sales
{
    public class Prerequisite : AuditableEntity
    {
        public string PrerequisiteId { get; set; }
        public string OfferId { get; set; }
        public string PlanId { get; set; }

        public Offer Offer { get; set; }
        public Plan Plan { get; set; }
    }
}
