﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Entities.Courses;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Sales
{
    public class InGroup : AuditableEntity
    {
        public InGroup()
        {
            MemberCourseEnrollments = new HashSet<Enrollment>();
        }
        public string InGroupId { get; set; }
        public string UserGroupId { get; set; }
        public string AuthUserIdRef { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime? RemovedOn { get; set; }
        public bool GroupAdmin { get; set; }

        public UserGroup UserGroup { get; set; }
        public ICollection<Enrollment> MemberCourseEnrollments { get; private set; }
    }
}
