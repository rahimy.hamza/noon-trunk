﻿using NoonOnline.Domain.Entities.Courses;
using NoonOnline.Domain.Enums.Sales;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Sales
{
    public class Software
    {
        public Software()
        {
            Plans = new HashSet<Plan>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public string AccessLink { get; set; }
        public SoftwareTypes Type { get; set; }
        public string CourseId { get; set; }
        public string CoursePackageId { get; set; }

        public Course Course { get; set; }
        public Package CoursePackage { get; set; }
        public ICollection<Plan> Plans { get; private set; }
    }
}
