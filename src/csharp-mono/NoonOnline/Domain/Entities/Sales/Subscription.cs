﻿using NoonOnline.Domain.Common;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Sales
{
    public class Subscription : AuditableEntity
    {
        public Subscription()
        {
            PlanHistoryEvents = new HashSet<PlanHistory>();
            Invoices = new HashSet<Invoice>();
        }

        public string SubscriptionId { get; set; }
        public string UserGroupId { get; set; }
        public DateTime? TrialPeriodStartDate { get; set; }
        public DateTime? TrialPeriodEndDate { get; set; }
        public bool SubscribeAfterTrial { get; set; }
        public string CurrentPlanId { get; set; }
        public string OfferId { get; set; }
        public DateTime? OfferStartDate { get; set; }
        public DateTime? OfferEndDate { get; set; }
        public DateTime SubscribedOn { get; set; }
        public DateTime ValidTo { get; set; }
        public DateTime? UnsubscribedOn { get; set; }
        public DateTime InsertTs { get; set; }

        public UserGroup UserGroup { get; set; }
        public Plan CurrentPlan { get; set; }
        public Offer Offer { get; set; }
        public ICollection<PlanHistory> PlanHistoryEvents { get; private set; }
        public ICollection<Invoice> Invoices { get; private set; }
    }
}
