﻿using NoonOnline.Domain.Common;
using System;

namespace NoonOnline.Domain.Entities.Sales
{
    public class OptionIncluded : AuditableEntity
    {
        public string OptionIncludedId { get; set; }
        public string PlanId { get; set; }
        public int OptionId { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime? RemovedOn { get; set; }

        public Plan Plan { get; set; }
        public Option Option { get; set; }
    }
}
