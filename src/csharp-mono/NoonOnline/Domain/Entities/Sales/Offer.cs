﻿using NoonOnline.Domain.Common;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.Entities.Sales
{
    public class Offer : AuditableEntity
    {
        public Offer()
        {
            OfferInclusions = new HashSet<Include>();
            OfferPrerequisites = new HashSet<Prerequisite>();
            Subscriptions = new HashSet<Subscription>();
        }

        public string OfferId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? DiscountPercentage { get; set; }
        public int? DurationMonths { get; set; }
        public DateTime? DurationEndDate { get; set; }

        public ICollection<Include> OfferInclusions { get; private set; }
        public ICollection<Prerequisite> OfferPrerequisites { get; private set; }
        public ICollection<Subscription> Subscriptions { get; private set; }
    }
}
