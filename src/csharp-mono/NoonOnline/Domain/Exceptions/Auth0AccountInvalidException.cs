﻿using System;

namespace NoonOnline.Domain.Exceptions
{
    public class Auth0AccountInvalidException : Exception
    {
        public Auth0AccountInvalidException(string auth0Account, Exception ex)
            : base($"Auth0 Account \"{auth0Account}\" is invalid.", ex)
        {
        }
    }
}
