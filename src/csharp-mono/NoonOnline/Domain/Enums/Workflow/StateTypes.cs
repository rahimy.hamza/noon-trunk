﻿namespace NoonOnline.Domain.Enums.Workflow
{
    public enum StateTypes
    {
        Start,
        Normal,
        Complete,
        Denied,
        Cancelled
    }
}
