﻿namespace NoonOnline.Domain.Enums.Workflow
{
    public enum Targets
    {
        Requester,
        Stakeholders,
        GroupMembers,
        ProcessAdmins
    }
}
