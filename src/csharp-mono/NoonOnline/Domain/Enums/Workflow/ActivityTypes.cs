﻿namespace NoonOnline.Domain.Enums.Workflow
{
    public enum ActivityTypes
    {
        AddNote,
        SendEmail,
        AddStakeholders,
        RemoveStakeholders
    }
}
