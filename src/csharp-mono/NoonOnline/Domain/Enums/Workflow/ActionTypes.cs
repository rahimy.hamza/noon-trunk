﻿namespace NoonOnline.Domain.Enums.Workflow
{
    public enum ActionTypes
    {
        Approve,
        Deny,
        Cancel,
        Restart,
        Resolve,
        Begin,
        Escalate,
        SuperComplete,
        SuperEscalate
    }
}
