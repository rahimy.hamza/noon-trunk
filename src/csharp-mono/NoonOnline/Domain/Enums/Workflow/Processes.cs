﻿namespace NoonOnline.Domain.Enums.Workflow
{
    public enum Processes
    {
        CourseVersionProduction,
        CoursePackageVersionProduction
    }
}
