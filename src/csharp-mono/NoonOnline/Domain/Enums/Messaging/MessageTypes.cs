﻿namespace NoonOnline.Domain.Enums.Messaging
{
    public enum MessageTypes
    {
        DiscussionTopic,
        DiscussionComment,
        CourseAdminNote,
        AssignmentSubmissionNote
    }
}
