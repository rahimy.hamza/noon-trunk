﻿namespace NoonOnline.Domain.Enums.Messaging
{
    public enum MessageReactionTypes
    {
        Like,
        Dislike
    }
}
