﻿namespace NoonOnline.Domain.Enums.Sales
{
    public enum SoftwareOptions
    {
        Discussions,
        Library,
        Analytics,
        SponsorCp
    }
}
