﻿namespace NoonOnline.Domain.Enums.Sales
{
    public enum SoftwareTypes
    {
        Course,
        CoursePackage
    }
}
