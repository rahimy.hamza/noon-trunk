﻿namespace NoonOnline.Domain.Enums.Courses
{
    public enum CourseVersionProductionStates
    {
        Started,
        InDevelopment,
        ReviewRejected,
        ContentPreparation,
        ContentPreparationFailed,
        Released,
        Cancelled
    }
}
