﻿namespace NoonOnline.Domain.Enums.Courses
{
    public enum CourseGrades
    {
        Grade1,
        Grade2,
        Grade3,
        Grade4,
        Grade5,
        Grade6,
        Grade7,
        Grade8,
        Grade9,
        Grade10,
        Grade11,
        Grade12
    }
}
