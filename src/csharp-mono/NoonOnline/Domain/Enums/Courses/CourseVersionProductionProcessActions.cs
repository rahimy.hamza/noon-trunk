﻿namespace NoonOnline.Domain.Enums.Courses
{
    public enum CourseVersionProductionProcessActions
    {
        /*
         * ActionType = Begin
         * Process = CourseVersionProduction
         * Transitions(FromState->ToState) = Started->InDevelopment, ReviewRejected->InDevelopment, ContentPreparationFailed->InDevelopment
         */
        StartDevelopment,
        /*
         * ActionType = Resolve
         * Process = CourseVersionProduction
         * Transitions(FromState->ToState) = InDevelopment->ContentPreparation, InDevelopment->ReviewRejected
         */
        CompleteDevelopment,
        /*
         * ActionType = Approve
         * Process = CourseVersionProduction
         * Transitions(FromState->ToState) = InDevelopment->ContentPreparation
         */
        ApproveReview,
        /*
         * ActionType = Deny
         * Process = CourseVersionProduction
         * Transitions(FromState->ToState) = InDevelopment->ReviewRejected
         */
        RejectReview,
        /*
         * ActionType = Begin
         * Process = CourseVersionProduction
         * Transitions(FromState->ToState) = ContentPreparation->Released, ContentPreparation->ContentPreparationFailed
         */
        StartContentPreparation,
        /*
         * ActionType = Resolve
         * Process = CourseVersionProduction
         * Transitions(FromState->ToState) = ContentPreparation->Released, ContentPreparation->ContentPreparationFailed
         */
        CompleteContentPreparation,
        /*
         * ActionType = Resolve
         * Process = CourseVersionProduction
         * Transitions(FromState->ToState) = ContentPreparation->Released
         */
        PassContentPreparation,
        /*
         * ActionType = Deny
         * Process = CourseVersionProduction
         * Transitions(FromState->ToState) = ContentPreparation->ContentPreparationFailed
         */
        FailContentPreparation,
        /*
         * ActionType = Cancel
         * Process = CourseVersionProduction
         * Transitions(FromState->ToState) = Started->Cancelled, InDevelopment->Cancelled, InReview->Cancelled, ReviewRejected->Cancelled
         */
        Cancel
    }
}
