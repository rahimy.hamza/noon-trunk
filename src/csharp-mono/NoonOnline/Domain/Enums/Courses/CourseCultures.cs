﻿namespace NoonOnline.Domain.Enums.Courses
{
    public enum CourseCultures
    {
        Dari,
        Pashto,
        English
    }
}
