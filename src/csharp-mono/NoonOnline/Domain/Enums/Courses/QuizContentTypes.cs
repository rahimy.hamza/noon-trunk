﻿namespace NoonOnline.Domain.Enums.Courses
{
    public enum QuizContentTypes
    {
        Question,
        Answer
    }
}
