﻿namespace NoonOnline.Domain.Enums.Courses
{
    public enum CourseContentTypes
    {
        Chapter,
        Base,
        PDF,
        Video,
        Quiz,
        Assignment
    }
}
