﻿using NoonOnline.Domain.Entities.Courses;
using NoonOnline.Domain.Enums.Courses;
using System.Collections.Generic;

namespace NoonOnline.Domain.Constants
{
    public class CourseProductionTransitions
    {
        public static readonly CourseVersionProductionTransition StartToInDevTransition = new CourseVersionProductionTransition { TransitionId = 1, CurrentState = CourseVersionProductionStates.Started, NextState = CourseVersionProductionStates.InDevelopment };
        public static readonly CourseVersionProductionTransition InDevToReviewRejectedTransition = new CourseVersionProductionTransition { TransitionId = 2, CurrentState = CourseVersionProductionStates.InDevelopment, NextState = CourseVersionProductionStates.ReviewRejected };
        public static readonly CourseVersionProductionTransition InDevToContentPrepTransition = new CourseVersionProductionTransition { TransitionId = 3, CurrentState = CourseVersionProductionStates.InDevelopment, NextState = CourseVersionProductionStates.ContentPreparation };
        public static readonly CourseVersionProductionTransition ContentPrepToContentPrepFailTransition = new CourseVersionProductionTransition { TransitionId = 4, CurrentState = CourseVersionProductionStates.ContentPreparation, NextState = CourseVersionProductionStates.ContentPreparationFailed };
        public static readonly CourseVersionProductionTransition ContentPrepToReleasedTransition = new CourseVersionProductionTransition { TransitionId = 5, CurrentState = CourseVersionProductionStates.ContentPreparation, NextState = CourseVersionProductionStates.Released };
        public static readonly CourseVersionProductionTransition ReviewRejectedToInDevelopmentTransition = new CourseVersionProductionTransition { TransitionId = 6, CurrentState = CourseVersionProductionStates.ReviewRejected, NextState = CourseVersionProductionStates.InDevelopment };
        public static readonly CourseVersionProductionTransition ContentPrepFailToInDevTransition = new CourseVersionProductionTransition { TransitionId = 7, CurrentState = CourseVersionProductionStates.ContentPreparationFailed, NextState = CourseVersionProductionStates.InDevelopment };
        public static readonly CourseVersionProductionTransition StartedToCancelledTransition = new CourseVersionProductionTransition { TransitionId = 8, CurrentState = CourseVersionProductionStates.Started, NextState = CourseVersionProductionStates.Cancelled };
        public static readonly CourseVersionProductionTransition InDevToCancelledTransition = new CourseVersionProductionTransition { TransitionId = 9, CurrentState = CourseVersionProductionStates.InDevelopment, NextState = CourseVersionProductionStates.Cancelled };
        public static readonly CourseVersionProductionTransition ReviewRejectedToCancelledTransition = new CourseVersionProductionTransition { TransitionId = 10, CurrentState = CourseVersionProductionStates.ReviewRejected, NextState = CourseVersionProductionStates.Cancelled };
        public static readonly CourseVersionProductionTransition ContentPrepFailToCancelledTransition = new CourseVersionProductionTransition { TransitionId = 11, CurrentState = CourseVersionProductionStates.ContentPreparationFailed, NextState = CourseVersionProductionStates.Cancelled };
        public static readonly List<CourseVersionProductionTransition> StaticCourseVersionProductionTransitions = new List<CourseVersionProductionTransition>()
        {
            StartToInDevTransition,
            InDevToReviewRejectedTransition,
            InDevToContentPrepTransition,
            ContentPrepToContentPrepFailTransition,
            ContentPrepToReleasedTransition,
            ReviewRejectedToInDevelopmentTransition,
            ContentPrepFailToInDevTransition,
            StartedToCancelledTransition,
            InDevToCancelledTransition,
            ReviewRejectedToCancelledTransition,
            ContentPrepFailToCancelledTransition
        };
    }
}
