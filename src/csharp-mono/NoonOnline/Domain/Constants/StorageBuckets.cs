﻿namespace NoonOnline.Domain.Constants
{
    public class StorageBuckets
    {
        public static readonly string RawMediaStorage = "raw-media";
        public static readonly string PublishedMediaStorage = "published-media";
        public static readonly string ReleasedCoursesStorage = "released-courses";
        public static readonly string ThumbnailsStorage = "thumbnails";

        // The user id is meant to be concatenated to the end of this string
        public static readonly string UserFilesStorage = "user-";
    }
}
