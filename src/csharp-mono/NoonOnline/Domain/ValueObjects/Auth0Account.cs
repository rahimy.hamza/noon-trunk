﻿using NoonOnline.Domain.Common;
using NoonOnline.Domain.Exceptions;
using System;
using System.Collections.Generic;

namespace NoonOnline.Domain.ValueObjects
{
    public class Auth0Account : ValueObject
    {
        private Auth0Account()
        {
        }

        public static Auth0Account For(string accountString)
        {
            var account = new Auth0Account();

            try
            {
                var index = accountString.IndexOf("|", StringComparison.Ordinal);
                account.Connection = accountString.Substring(0, index);
                account.Id = accountString.Substring(index + 1);
            }
            catch(Exception ex)
            {
                throw new Auth0AccountInvalidException(accountString, ex);
            }

            return account;
        }

        public string Connection { get; private set; }
        public string Id { get; private set; }

        public static explicit operator Auth0Account(string accountString)
        {
            return For(accountString);
        }

        public override string ToString()
        {
            return $"{Connection}|{Id}";
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Connection;
            yield return Id;
        }
    }
}
