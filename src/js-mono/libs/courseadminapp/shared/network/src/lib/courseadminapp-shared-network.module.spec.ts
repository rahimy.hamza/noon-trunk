import { async, TestBed } from '@angular/core/testing';
import { CourseadminappSharedNetworkModule } from './courseadminapp-shared-network.module';

describe('CourseadminappSharedNetworkModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CourseadminappSharedNetworkModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(CourseadminappSharedNetworkModule).toBeDefined();
  });
});
