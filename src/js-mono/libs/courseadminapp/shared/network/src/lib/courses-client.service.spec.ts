import { TestBed } from '@angular/core/testing';

import { CoursesClientService } from './courses-client.service';

describe('CoursesClientService', () => {
  let service: CoursesClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoursesClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
