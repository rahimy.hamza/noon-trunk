module.exports = {
  name: 'courseadminapp-shared-network',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/courseadminapp/shared/network',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
