import { async, TestBed } from '@angular/core/testing';
import { CourseadminappSharedUiModule } from './courseadminapp-shared-ui.module';

describe('CourseadminappSharedUiModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CourseadminappSharedUiModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(CourseadminappSharedUiModule).toBeDefined();
  });
});
