import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SidenavComponent } from './sidenav/sidenav.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { SharedUiModule } from '@js-mono/shared/ui';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { AuthGuard } from '@js-mono/shared/auth-service';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('@js-mono/courseadminapp/courses/feature-list').then(m => m.CourseadminappCoursesFeatureListModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'courses',
    loadChildren: () => import('@js-mono/courseadminapp/courses/feature-list').then(m => m.CourseadminappCoursesFeatureListModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'courseform',
    loadChildren: () => import('@js-mono/courseadminapp/courses/course-form').then(m => m.CourseadminappCoursesCourseFormModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'courses/:courseId',
    loadChildren: () => import('@js-mono/courseadminapp/courses/control-panel').then(m => m.CourseadminappCoursesControlPanelModule),
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    CommonModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    SharedUiModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [SidenavComponent],
  exports: [SidenavComponent]
})
export class CourseadminappSharedUiModule {}
