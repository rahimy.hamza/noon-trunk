module.exports = {
  name: 'courseadminapp-shared-ui',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/courseadminapp/shared/ui',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
