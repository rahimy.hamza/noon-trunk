import { async, TestBed } from '@angular/core/testing';
import { CourseadminappSharedStateModule } from './courseadminapp-shared-state.module';

describe('CourseadminappSharedStateModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CourseadminappSharedStateModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(CourseadminappSharedStateModule).toBeDefined();
  });
});
