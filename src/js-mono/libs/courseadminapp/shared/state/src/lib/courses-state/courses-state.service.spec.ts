import { TestBed } from '@angular/core/testing';

import { CoursesStateService } from './courses-state.service';

describe('CoursesStateService', () => {
  let service: CoursesStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoursesStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
