import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import {
  CourseDetailVm,
  CoursesClientService,
  CourseLookupDto,
  CreateCourseCommand,
  CourseNoteLookupDto
} from '@js-mono/courseadminapp/shared/network';
import { tap, mergeMap, catchError } from 'rxjs/operators';
import { Navigate } from '@ngxs/router-plugin';
import produce from 'immer';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

export interface CourseList {
  courses: CourseLookupDto[];
  resultsLength: number;
  isLoadingResults: boolean;
}

export interface CourseNoteList {
  notes: CourseNoteLookupDto[];
  resultsLength: number;
  isLoadingResults: boolean;
  error: string;
}

export interface NewCourseForm {
  redirectUrl: string;
  model: CreateCourseCommand;
  status: string;
  dirty: false;
  errors: string[];
}

export interface MappedCourseDetails {
  type: string;
  value: any;
}

export interface CoursesStateModel {
  courseList: CourseList;
  courseDetailsLoading: boolean;
  courseDetailsError: string;
  courseDetails: CourseDetailVm;
  courseNotes: CourseNoteList;
  newCourseForm: NewCourseForm;
}

export class FetchCourses {
  static readonly type = '[Courses] FetchCourses';
  constructor(public page: number) {}
}

export class SetNewCourseThumbnail {
  static readonly type = '[Courses] SetNewCourseThumbnail';
  constructor(public fileId: string) {}
}

export class SetNewCourseTrailer {
  static readonly type = '[Courses] SetNewCourseTrailer';
  constructor(public fileId: string) {}
}

export class SetNewCourseTeacherImage {
  static readonly type = '[Courses] SetNewCourseTeacherImage';
  constructor(public fileId: string) {}
}

export class CreateNewCourse {
  static readonly type = '[Courses] CreateNewCourse';
}

export class GetCourseDetails {
  static readonly type = '[Courses] GetCourseDetails';
  constructor(public courseId: string) {}
}

export class StartVersionDevelopment {
  static readonly type = '[Courses] StartVersionDevelopment';
  constructor(public courseId: string, public actionId: string) {}
}

export class CompleteVersionDevelopment {
  static readonly type = '[Courses] CompleteVersionDevelopment';
  constructor(public courseId: string, public actionId: string) {}
}

export class ApproveVersionDevelopment {
  static readonly type = '[Courses] ApproveVersionDevelopment';
  constructor(public courseId: string, public actionId: string) {}
}

export class RejectVersionDevelopment {
  static readonly type = '[Courses] RejectVersionDevelopment';
  constructor(public courseId: string, public actionId: string) {}
}

export class CancelVersionDevelopment {
  static readonly type = '[Courses] CancelVersionDevelopment';
  constructor(public courseId: string, public actionId: string) {}
}

export class StartVersionContentPreparation {
  static readonly type = '[Courses] StartVersionContentPreparation';
  constructor(public courseId: string, public actionId: string) {}
}

export class GetNotes {
  static readonly type = '[Courses] GetNotes';
  constructor(public page: number) {}
}

@State<CoursesStateModel>({
  name: 'courses',
  defaults: {
    courseList: {
      courses: [],
      resultsLength: 0,
      isLoadingResults: true
    },
    courseDetailsLoading: true,
    courseDetailsError: undefined,
    courseDetails: undefined,
    courseNotes: {
      notes: [],
      resultsLength: 0,
      isLoadingResults: true,
      error: undefined
    },
    newCourseForm: {
      redirectUrl: undefined,
      dirty: false,
      model: undefined,
      status: '',
      errors: []
    }
  }
})
@Injectable({
  providedIn: 'root'
})
export class CoursesStateService {
  constructor(private coursesService: CoursesClientService) {}

  @Selector()
  static courseList(state: CoursesStateModel) {
    return state.courseList.courses;
  }

  @Selector()
  static isCourseListLoading(state: CoursesStateModel) {
    return state.courseList.isLoadingResults;
  }

  @Selector()
  static courseListLength(state: CoursesStateModel) {
    return state.courseList.resultsLength;
  }

  @Selector()
  static newCourseThumbnail(state: CoursesStateModel) {
    return state.newCourseForm.model.thumbnailFileId;
  }

  @Selector()
  static newCourseTrailer(state: CoursesStateModel) {
    return state.newCourseForm.model.trailerFileId;
  }

  @Selector()
  static newCourseTeacherImage(state: CoursesStateModel) {
    return state.newCourseForm.model.teacherImageFileId;
  }

  @Selector()
  static selectedCourseDetails(state: CoursesStateModel): CourseDetailVm {
    return state.courseDetails;
  }

  @Selector()
  static mappedSelectedCourseDetails(
    state: CoursesStateModel
  ): MappedCourseDetails[] {
    const mapped = Object.keys(state.courseDetails).map(key => ({
      type: key,
      value: state.courseDetails[key]
    }));
    let finalMapping: MappedCourseDetails[] = [];
    mapped.map(result => {
      if (
        result.type === 'id' ||
        result.type === 'name' ||
        result.type === 'description' ||
        result.type === 'culture' ||
        result.type === 'grade' ||
        result.type === 'availableInTrial' ||
        result.type === 'currentVersion' ||
        result.type === 'currentVersionState' ||
        result.type === 'thumbnailUrl' ||
        result.type === 'teacherImageUrl' ||
        result.type === 'trailerUrl' ||
        result.type === 'teacherName' ||
        result.type === 'teacherDescription'
      ) {
        finalMapping.push(result);
        return result;
      }
    });
    return finalMapping;
  }

  @Selector()
  static coursesDetailLoading(state: CoursesStateModel): boolean {
    return state.courseDetailsLoading;
  }

  @Selector()
  static courseDetailError(state: CoursesStateModel) {
    return state.courseDetailsError;
  }

  @Selector()
  static isCourseNotesLoading(state: CoursesStateModel): boolean {
    return state.courseNotes.isLoadingResults;
  }

  @Selector()
  static courseNotesLength(state: CoursesStateModel): number {
    return state.courseNotes.resultsLength;
  }

  @Selector()
  static courseNotes(state: CoursesStateModel): CourseNoteLookupDto[] {
    return state.courseNotes.notes;
  }

  @Action(FetchCourses)
  fetchCourses(ctx: StateContext<CoursesStateModel>, action: FetchCourses) {
    ctx.setState(
      produce(draft => {
        draft.courseList.isLoadingResults = true;
      })
    );
    return this.coursesService.getAll(action.page).pipe(
      tap(result => {
        ctx.setState(
          produce(draft => {
            draft.courseList.courses = result.coursesList;
            draft.courseList.resultsLength = result.totalCount;
            draft.courseList.isLoadingResults = false;
          })
        );
      })
    );
  }

  @Action(SetNewCourseThumbnail)
  setNewCourseThumbnail(
    ctx: StateContext<CoursesStateModel>,
    action: SetNewCourseThumbnail
  ) {
    ctx.setState(
      produce(draft => {
        draft.newCourseForm.model.thumbnailFileId = action.fileId;
      })
    );
  }

  @Action(SetNewCourseTrailer)
  setNewCourseTrailer(
    ctx: StateContext<CoursesStateModel>,
    action: SetNewCourseTrailer
  ) {
    ctx.setState(
      produce(draft => {
        draft.newCourseForm.model.trailerFileId = action.fileId;
      })
    );
  }

  @Action(SetNewCourseTeacherImage)
  setNewCourseTeacherImage(
    ctx: StateContext<CoursesStateModel>,
    action: SetNewCourseTeacherImage
  ) {
    ctx.setState(
      produce(draft => {
        draft.newCourseForm.model.teacherImageFileId = action.fileId;
      })
    );
  }

  @Action(CreateNewCourse)
  createNewCourse(
    ctx: StateContext<CoursesStateModel>,
    action: CreateNewCourse
  ) {
    const state = ctx.getState();
    const freshForm: NewCourseForm = {
      redirectUrl: undefined,
      dirty: false,
      model: undefined,
      status: '',
      errors: []
    };
    try {
      return this.coursesService.create({ ...state.newCourseForm.model }).pipe(
        tap(result => {
          ctx.setState(
            produce(draft => {
              draft.newCourseForm = freshForm;
            })
          );
        }),
        mergeMap(() => ctx.dispatch(new Navigate(['/courses'])))
      );
    } catch (err) {
      ctx.setState(
        produce(draft => {
          draft.newCourseForm.errors.push(err);
        })
      );
    }
  }

  @Action(GetCourseDetails)
  getCourseDetails(
    ctx: StateContext<CoursesStateModel>,
    action: GetCourseDetails
  ) {
    ctx.setState(
      produce(draft => {
        draft.courseDetailsLoading = true;
      })
    );
    try {
      return this.coursesService.get(action.courseId).pipe(
        tap(result => {
          ctx.setState(
            produce(draft => {
              draft.courseDetailsLoading = false;
              draft.courseDetailsError = undefined;
              draft.courseDetails = result;
            })
          );
        })
      );
    } catch (err) {
      ctx.setState(
        produce(draft => {
          draft.courseDetailsLoading = false;
          draft.courseDetailsError = err;
        })
      );
    }
  }

  @Action(GetNotes)
  getNotes(ctx: StateContext<CoursesStateModel>, action: GetNotes) {
    ctx.setState(
      produce(draft => {
        draft.courseNotes.isLoadingResults = true;
      })
    );
    const state = ctx.getState();
    return this.coursesService
      .getCourseNotes(state.courseDetails.id, action.page)
      .pipe(
        tap(result => {
          ctx.setState(
            produce(draft => {
              draft.courseNotes.isLoadingResults = false;
              draft.courseNotes.resultsLength = result.totalCount;
              draft.courseNotes.notes = result.notes;
              draft.courseNotes.error = undefined;
            })
          );
        }),
        catchError(err => {
          ctx.setState(
            produce(draft => {
              draft.courseNotes.isLoadingResults = false;
              draft.courseNotes.resultsLength = 0;
              draft.courseNotes.notes = undefined;
              draft.courseNotes.error = err;
            })
          );
          return of('');
        })
      );
  }

  @Action(StartVersionDevelopment)
  startVersionDevelopment(
    ctx: StateContext<CoursesStateModel>,
    action: StartVersionDevelopment
  ) {
    ctx.setState(
      produce(draft => {
        draft.courseDetailsLoading = true;
      })
    );
    try {
      return this.coursesService
        .startCourseDevelopment(action.courseId, action.actionId)
        .pipe(
          tap(result => {}),
          mergeMap(() => ctx.dispatch(new GetCourseDetails(action.courseId)))
        );
    } catch (err) {
      ctx.setState(
        produce(draft => {
          draft.courseDetailsLoading = false;
          draft.courseDetailsError = err;
        })
      );
    }
  }

  @Action(CompleteVersionDevelopment)
  completeVersionDevelopment(
    ctx: StateContext<CoursesStateModel>,
    action: CompleteVersionDevelopment
  ) {
    ctx.setState(
      produce(draft => {
        draft.courseDetailsLoading = true;
      })
    );
    try {
      return this.coursesService
        .completeCourseDevelopment(action.courseId, action.actionId)
        .pipe(
          tap(result => {}),
          mergeMap(() => ctx.dispatch(new GetCourseDetails(action.courseId)))
        );
    } catch (err) {
      ctx.setState(
        produce(draft => {
          draft.courseDetailsLoading = false;
          draft.courseDetailsError = err;
        })
      );
    }
  }

  @Action(ApproveVersionDevelopment)
  approveVersionDevelopment(
    ctx: StateContext<CoursesStateModel>,
    action: ApproveVersionDevelopment
  ) {
    ctx.setState(
      produce(draft => {
        draft.courseDetailsLoading = true;
      })
    );
    try {
      return this.coursesService
        .approveCourseDevelopment(action.courseId, action.actionId)
        .pipe(
          tap(result => {}),
          mergeMap(() => ctx.dispatch(new GetCourseDetails(action.courseId)))
        );
    } catch (err) {
      ctx.setState(
        produce(draft => {
          draft.courseDetailsLoading = false;
          draft.courseDetailsError = err;
        })
      );
    }
  }

  @Action(RejectVersionDevelopment)
  rejectVersionDevelopment(
    ctx: StateContext<CoursesStateModel>,
    action: RejectVersionDevelopment
  ) {
    ctx.setState(
      produce(draft => {
        draft.courseDetailsLoading = true;
      })
    );
    try {
      return this.coursesService
        .rejectCourseDevelopment(action.courseId, action.actionId)
        .pipe(
          tap(result => {}),
          mergeMap(() => ctx.dispatch(new GetCourseDetails(action.courseId)))
        );
    } catch (err) {
      ctx.setState(
        produce(draft => {
          draft.courseDetailsLoading = false;
          draft.courseDetailsError = err;
        })
      );
    }
  }

  @Action(CancelVersionDevelopment)
  cancelVersionDevelopment(
    ctx: StateContext<CoursesStateModel>,
    action: CancelVersionDevelopment
  ) {
    ctx.setState(
      produce(draft => {
        draft.courseDetailsLoading = true;
      })
    );
    return this.coursesService
      .cancelCourseDevelopment(action.courseId, action.actionId)
      .pipe(
        tap(result => {}),
        mergeMap(() => ctx.dispatch(new GetCourseDetails(action.courseId))),
        catchError(err => {
          ctx.setState(
            produce(draft => {
              draft.courseDetailsError = err;
              draft.courseDetailsLoading = false;
            })
          );
          return of('');
        })
      );
  }

  @Action(StartVersionContentPreparation)
  startVersionContentPreparation(
    ctx: StateContext<CoursesStateModel>,
    action: StartVersionContentPreparation
  ) {
    ctx.setState(
      produce(draft => {
        draft.courseDetailsLoading = true;
      })
    );
    return this.coursesService
      .startCourseContentPreparation(action.courseId, action.actionId)
      .pipe(
        tap(result => {}),
        mergeMap(() => ctx.dispatch(new GetCourseDetails(action.courseId))),
        catchError(err => {
          ctx.setState(
            produce(draft => {
              draft.courseDetailsError = err;
              draft.courseDetailsLoading = false;
            })
          );
          return of('');
        })
      );
  }
}
