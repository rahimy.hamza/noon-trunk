import { Component } from '@angular/core';
import {
  CoursesStateService,
  MappedCourseDetails
} from '@js-mono/courseadminapp/shared/state';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import {
  CourseCultures,
  CourseGrades
} from '@js-mono/courseadminapp/shared/network';

@Component({
  selector: 'js-mono-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent {
  values = Object.values;
  courseCultures = CourseCultures;
  courseGrades = CourseGrades;
  displayedColumns: string[] = ['type', 'value'];

  constructor() {}

  @Select(CoursesStateService.mappedSelectedCourseDetails)
  mappedSelectedCourse$: Observable<MappedCourseDetails[]>;
  @Select(CoursesStateService.coursesDetailLoading) courseDetailLoading$: Observable<boolean>;
  @Select(CoursesStateService.courseDetailError) courseDetailError$: Observable<string>;

  validUrl(s) {
    var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return regexp.test(s);
  }
}
