import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatListModule } from '@angular/material/list';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { LayoutComponent } from './layout/layout.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { CourseContentComponent } from './course-content/course-content.component';
import { CourseVersionsComponent } from './course-versions/course-versions.component';
import { SharedPipesModule } from '@js-mono/shared/pipes';
import { CourseNotesComponent } from './course-notes/course-notes.component';
import { AuthGuard } from '@js-mono/shared/auth-service';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'details',
        component: CourseDetailsComponent
      },
      {
        path: 'content',
        component: CourseContentComponent
      },
      {
        path: '',
        component: CourseDetailsComponent
      },
      {
        path: 'versions',
        component: CourseVersionsComponent
      },
      {
        path: 'activities',
        component: CourseNotesComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatListModule,
    DragDropModule,
    MatTabsModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    SharedPipesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    LayoutComponent,
    CourseDetailsComponent,
    CourseContentComponent,
    CourseVersionsComponent,
    CourseNotesComponent
  ]
})
export class CourseadminappCoursesControlPanelModule {}
