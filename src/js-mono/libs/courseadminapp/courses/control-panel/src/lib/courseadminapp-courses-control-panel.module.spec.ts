import { async, TestBed } from '@angular/core/testing';
import { CourseadminappCoursesControlPanelModule } from './courseadminapp-courses-control-panel.module';

describe('CourseadminappCoursesControlPanelModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CourseadminappCoursesControlPanelModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(CourseadminappCoursesControlPanelModule).toBeDefined();
  });
});
