import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { CoursesStateService, GetNotes } from '@js-mono/courseadminapp/shared/state';
import { Observable, merge, of as observableOf } from 'rxjs';
import { CourseNoteLookupDto } from '@js-mono/courseadminapp/shared/network';
import { MatPaginator } from '@angular/material/paginator';
import { startWith, switchMap, catchError } from 'rxjs/operators';

@Component({
  selector: 'js-mono-course-notes',
  templateUrl: './course-notes.component.html',
  styleUrls: ['./course-notes.component.css']
})
export class CourseNotesComponent implements AfterViewInit {
  displayedColumns: string[] = [
    'by',
    'title',
    'description',
    'message',
    'createdOn'
  ];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @Select(CoursesStateService.courseNotes) courseNotes$: Observable<
    CourseNoteLookupDto[]
  >;
  @Select(CoursesStateService.isCourseNotesLoading)
  isCourseNotesLoading$: Observable<boolean>;
  @Select(CoursesStateService.courseNotesLength) courseNotesLength$: Observable<
    number
  >;
  constructor(private store: Store) {}

  ngAfterViewInit(): void {
    merge(this.paginator.page)
    .pipe(
      startWith({}),
      switchMap(() => {
        setTimeout(() => this.store.dispatch(new GetNotes(this.paginator.pageIndex)));
        return observableOf([]);
      }),
      catchError(() => {
        return observableOf([]);
      })
    ).subscribe();
  }
}
