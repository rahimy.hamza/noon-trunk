import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseVersionsComponent } from './course-versions.component';

describe('CourseVersionsComponent', () => {
  let component: CourseVersionsComponent;
  let fixture: ComponentFixture<CourseVersionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseVersionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseVersionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
