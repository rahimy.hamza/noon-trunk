import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable, of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import {
  GetCourseDetails,
  CompleteVersionDevelopment,
  ApproveVersionDevelopment,
  RejectVersionDevelopment,
  CancelVersionDevelopment,
  StartVersionContentPreparation
} from '@js-mono/courseadminapp/shared/state';
import { CourseDetailVm } from '@js-mono/courseadminapp/shared/network';
import {
  CoursesStateService,
  StartVersionDevelopment
} from '@js-mono/courseadminapp/shared/state';

@Component({
  selector: 'js-mono-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit, OnDestroy {
  routeDataSub: Subscription;
  selectedCourseId: string;
  navLinks: NavLink[] = [
    { path: 'details', label: 'Details' },
    { path: 'content', label: 'Content' },
    { path: 'versions', label: 'Versions' },
    { path: 'activities', label: 'Activities' }
  ];
  @Select(CoursesStateService.selectedCourseDetails)
  selectedCourse$: Observable<CourseDetailVm>;
  constructor(private route: ActivatedRoute, private store: Store) {}
  ngOnInit(): void {
    this.routeDataSub = this.route.params.subscribe(url => {
      this.store.dispatch(new GetCourseDetails(url.courseId));
      this.selectedCourseId = url.courseId;
    });
  }

  ngOnDestroy() {
    this.routeDataSub.unsubscribe();
  }

  startDevelopmentAction(actionId: string) {
    this.store.dispatch(
      new StartVersionDevelopment(this.selectedCourseId, actionId)
    );
  }

  completeDevelopmentAction(actionId: string) {
    this.store.dispatch(
      new CompleteVersionDevelopment(this.selectedCourseId, actionId)
    );
  }

  approveDevelopmentAction(actionId: string) {
    this.store.dispatch(
      new ApproveVersionDevelopment(this.selectedCourseId, actionId)
    );
  }

  rejectDevelopmentAction(actionId: string) {
    this.store.dispatch(
      new RejectVersionDevelopment(this.selectedCourseId, actionId)
    );
  }

  startContentPreparationAction(actionId: string) {
    this.store.dispatch(
      new StartVersionContentPreparation(this.selectedCourseId, actionId)
    );
  }

  cancelDevelopmentAction(actionId: string) {
    this.store.dispatch(
      new CancelVersionDevelopment(this.selectedCourseId, actionId)
    );
  }
}

export interface NavLink {
  path: string;
  label: string;
}
