module.exports = {
  name: 'courseadminapp-courses-control-panel',
  preset: '../../../../jest.config.js',
  coverageDirectory:
    '../../../../coverage/libs/courseadminapp/courses/control-panel',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
