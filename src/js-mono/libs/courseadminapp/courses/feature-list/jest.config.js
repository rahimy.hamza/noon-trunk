module.exports = {
  name: 'courseadminapp-courses-feature-list',
  preset: '../../../../jest.config.js',
  coverageDirectory:
    '../../../../coverage/libs/courseadminapp/courses/feature-list',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
