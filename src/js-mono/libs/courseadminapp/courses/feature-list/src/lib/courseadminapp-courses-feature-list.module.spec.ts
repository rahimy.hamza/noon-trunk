import { async, TestBed } from '@angular/core/testing';
import { CourseadminappCoursesFeatureListModule } from './courseadminapp-courses-feature-list.module';

describe('CourseadminappCoursesFeatureListModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CourseadminappCoursesFeatureListModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(CourseadminappCoursesFeatureListModule).toBeDefined();
  });
});
