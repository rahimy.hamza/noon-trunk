import { Component, AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import {
  CourseLookupDto,
  CourseCultures,
  CourseGrades
} from '@js-mono/courseadminapp/shared/network';
import { merge, of as observableOf, Observable } from 'rxjs';
import { startWith, switchMap, catchError } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { Select, Store } from '@ngxs/store';
import {
  CoursesStateService,
  FetchCourses
} from '@js-mono/courseadminapp/shared/state';
import { Navigate } from '@ngxs/router-plugin';

@Component({
  selector: 'js-mono-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements AfterViewInit {
  displayedColumns: string[] = [
    'id',
    'name',
    'culture',
    'grade',
    'currentVersionState',
    'currentVersion'
  ];
  CourseCulture = CourseCultures;
  CourseGrade = CourseGrades;

  isLoadingResults = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @Select(CoursesStateService.courseList) courses$: Observable<
    CourseLookupDto[]
  >;
  @Select(CoursesStateService.isCourseListLoading)
  isLoadingResults$: Observable<boolean>;
  @Select(CoursesStateService.courseListLength) resultsLength$: Observable<
    number
  >;

  constructor(private store: Store) {
  }

  ngAfterViewInit() {
    merge(this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          setTimeout(() => this.store.dispatch(
            new FetchCourses(this.paginator.pageIndex)
          ));
          return observableOf([]);
        }),
        catchError(() => {
          return observableOf([]);
        })
      )
      .subscribe();
  }

  routerLink(link: string): string {
    return `/courses/${link}`;
  }

  goNewCourseForm() {
    this.store.dispatch(new Navigate(['/courseform']));
  }
}
