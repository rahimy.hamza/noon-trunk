import { Component, Input, InjectionToken, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {
  CourseCultures,
  CourseGrades,
  CoursesClientService
} from '@js-mono/courseadminapp/shared/network';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { Subscription, of, Observable } from 'rxjs';
import {
  HttpRequest,
  HttpClient,
  HttpEventType,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
import { map, tap, last, catchError } from 'rxjs/operators';
import { Store, Select } from '@ngxs/store';
import {
  SetNewCourseThumbnail,
  CoursesStateService,
  CreateNewCourse,
  SetNewCourseTrailer,
  SetNewCourseTeacherImage
} from '@js-mono/courseadminapp/shared/state';
import { UpdateFormValue } from '@ngxs/form-plugin';

export const BASE_URL = new InjectionToken<string>('BASE_URL');

@Component({
  selector: 'js-mono-main-form',
  templateUrl: './main-form.component.html',
  styleUrls: ['./main-form.component.css'],
  animations: [
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [animate(300, style({ opacity: 0 }))])
    ])
  ]
})
export class MainFormComponent {
  courseCultures = CourseCultures;
  courseGrades = CourseGrades;
  newCourseForm = new FormGroup({
    name: new FormControl(),
    description: new FormControl(),
    culture: new FormControl(),
    grade: new FormControl(),
    availableInTrial: new FormControl(),
    inAppTitle: new FormControl(),
    inAppDescription: new FormControl(),
    teacherName: new FormControl(),
    teacherDescription: new FormControl(),
    thumbnailFileId: new FormControl(),
    trailerFileId: new FormControl(),
    teacherImageFileId: new FormControl()
  });

  files: Array<FileUploadModel> = [];

  @Input() accept = 'image/*';

  @Select(CoursesStateService.newCourseThumbnail) thumbnail$: Observable<
    string
  >;
  @Select(CoursesStateService.newCourseTrailer) trailer$: Observable<string>;
  @Select(CoursesStateService.newCourseTeacherImage) teacherImage$: Observable<
    string
  >;

  constructor(
    private _http: HttpClient,
    private store: Store,
    private coursesService: CoursesClientService
  ) {
  }

  cultureKeys() {
    let toReturnKeys = [];
    Object.values(this.courseCultures).map(value => {
      if (typeof value === 'string') {
        toReturnKeys.push(value);
        return value;
      }
    });
    return toReturnKeys;
  }

  gradeKeys() {
    let toReturnKeys = [];
    Object.values(this.courseGrades).map(value => {
      if (typeof value === 'string') {
        toReturnKeys.push(value);
        return value;
      }
    });
    return toReturnKeys;
  }

  onSubmit() {
    console.log('form was submitted!');
    this.store.dispatch(new CreateNewCourse());
  }

  onThumbnailUploadClick() {
    console.log('thumbnail upload clicked');
    const thumbnailFileUpload = document.getElementById(
      'thumbnailFileUpload'
    ) as HTMLInputElement;
    thumbnailFileUpload.onchange = () => {
      for (let index = 0; index < thumbnailFileUpload.files.length; index++) {
        const file = thumbnailFileUpload.files[index];
        console.log(file);
        this.coursesService
          .createThumbnailUploadUrl({
            contentType: file.type,
            fileName: file.name,
            sizeInBytes: file.size
          })
          .subscribe(result => {
            this.files.push({
              data: file,
              state: 'in',
              inProgress: false,
              progress: 0,
              canRetry: false,
              canCancel: true,
              targetUrl: result.signedUrl,
              apiFileId: result.fileId,
              attachmentType: AttachmentType.Thumbnail,
              contentType: file.type,
              isThumbnail: true
            });
            this.uploadFiles('thumbnailFileUpload');
          });
      }
    };
    thumbnailFileUpload.click();
  }

  onTrailerUploadClick() {
    console.log('trailer upload clicked');
    const trailerFileUpload = document.getElementById(
      'trailerFileUpload'
    ) as HTMLInputElement;
    trailerFileUpload.onchange = () => {
      for (let index = 0; index < trailerFileUpload.files.length; index++) {
        const file = trailerFileUpload.files[index];
        console.log(file);
        this.coursesService
          .createCourseTrailerUploadUrl({
            contentType: file.type,
            fileName: file.name,
            sizeInBytes: file.size
          })
          .subscribe(result => {
            this.files.push({
              data: file,
              state: 'in',
              inProgress: false,
              progress: 0,
              canRetry: false,
              canCancel: true,
              targetUrl: result.signedUrl,
              apiFileId: result.fileId,
              attachmentType: AttachmentType.Trailer,
              contentType: file.type,
              isTrailer: true
            });
            this.uploadFiles('trailerFileUpload');
          });
      }
    };
    trailerFileUpload.click();
  }

  onTeacherImageUploadClick() {
    const teacherImageFileUpload = document.getElementById(
      'teacherImageFileUpload'
    ) as HTMLInputElement;
    teacherImageFileUpload.onchange = () => {
      for (
        let index = 0;
        index < teacherImageFileUpload.files.length;
        index++
      ) {
        const file = teacherImageFileUpload.files[index];
        console.log(file);
        this.coursesService
          .createCourseTrailerUploadUrl({
            contentType: file.type,
            fileName: file.name,
            sizeInBytes: file.size
          })
          .subscribe(result => {
            this.files.push({
              data: file,
              state: 'in',
              inProgress: false,
              progress: 0,
              canRetry: false,
              canCancel: true,
              targetUrl: result.signedUrl,
              apiFileId: result.fileId,
              attachmentType: AttachmentType.TeacherImage,
              contentType: file.type,
              isTrailer: true
            });
            this.uploadFiles('teacherImageFileUpload');
          });
      }
    };
    teacherImageFileUpload.click();
  }

  cancelFile(file: FileUploadModel) {
    file.sub.unsubscribe();
  }

  retryFile(file: FileUploadModel) {
    this.uploadFile(file);
    file.canRetry = false;
  }

  private uploadFile(file: FileUploadModel) {
    console.log('uploadFile started');

    const requestHeaders = new HttpHeaders();
    requestHeaders.append('Content-Type', file.contentType);

    const req = new HttpRequest('PUT', file.targetUrl, file.data, {
      reportProgress: true,
      headers: requestHeaders
    });

    file.inProgress = true;
    file.sub = this._http
      .request(req)
      .pipe(
        map(event => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              file.progress = Math.round((event.loaded * 100) / event.total);
              break;
            case HttpEventType.Response:
              return event;
          }
        }),
        tap(message => {}),
        last(),
        catchError((error: HttpErrorResponse) => {
          file.inProgress = false;
          file.canRetry = true;
          return of(`${file.data.name} upload failed`);
        })
      )
      .subscribe((event: any) => {
        if (typeof event === 'object') {
          if (file.attachmentType === AttachmentType.Thumbnail) {
            console.log('setting thumbnail value');
            this.store.dispatch(new SetNewCourseThumbnail(file.apiFileId));
          }
          if (file.attachmentType === AttachmentType.Trailer) {
            this.store.dispatch(new SetNewCourseTrailer(file.apiFileId));
          }
          if (file.attachmentType === AttachmentType.TeacherImage) {
            this.store.dispatch(new SetNewCourseTeacherImage(file.apiFileId));
          }
          this.removeFileFromArray(file);
        }
      });
  }

  private uploadFiles(htmlFile: string) {
    const fileUpload = document.getElementById(htmlFile) as HTMLInputElement;
    fileUpload.value = '';

    this.files.forEach(file => {
      if (!file.inProgress) {
        this.uploadFile(file);
      }
    });
  }

  private removeFileFromArray(file: FileUploadModel) {
    console.log(file);
    const index = this.files.indexOf(file);
    if (index > -1) {
      this.files.splice(index, 1);
    }
  }
}

export enum AttachmentType {
  Thumbnail,
  Trailer,
  TeacherImage
}

export class FileUploadModel {
  data: File;
  state: string;
  inProgress: boolean;
  progress: number;
  canRetry: boolean;
  canCancel: boolean;
  sub?: Subscription;
  apiFileId: string;
  targetUrl: string;
  attachmentType: AttachmentType;
  contentType: string;
  isThumbnail?: boolean;
  isTrailer?: boolean;
  isTeacherImage?: boolean;
}
