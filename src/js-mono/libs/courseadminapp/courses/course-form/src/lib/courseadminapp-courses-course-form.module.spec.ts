import { async, TestBed } from '@angular/core/testing';
import { CourseadminappCoursesCourseFormModule } from './courseadminapp-courses-course-form.module';

describe('CourseadminappCoursesCourseFormModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CourseadminappCoursesCourseFormModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(CourseadminappCoursesCourseFormModule).toBeDefined();
  });
});
