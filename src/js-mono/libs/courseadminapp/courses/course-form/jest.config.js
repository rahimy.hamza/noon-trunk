module.exports = {
  name: 'courseadminapp-courses-course-form',
  preset: '../../../../jest.config.js',
  coverageDirectory:
    '../../../../coverage/libs/courseadminapp/courses/course-form',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
