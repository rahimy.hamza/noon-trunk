import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'camelToSentenceCase'
})
export class CamelToSentenceCasePipe implements PipeTransform {

  transform(value: string): string {
    var result = value.replace( /([A-Z])/g, " $1" );
    var finalResult = result.charAt(0).toUpperCase() + result.slice(1);
    return finalResult;
  }

}
