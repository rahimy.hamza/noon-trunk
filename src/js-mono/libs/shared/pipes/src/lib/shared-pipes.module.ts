import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CamelToSentenceCasePipe } from './camel-to-sentence-case/camel-to-sentence-case.pipe';
import { TruncateCharPipe } from './truncate-char/truncate-char.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [CamelToSentenceCasePipe, TruncateCharPipe],
  exports: [CamelToSentenceCasePipe, TruncateCharPipe]
})
export class SharedPipesModule {}
