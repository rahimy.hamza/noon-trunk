import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncateChar'
})
export class TruncateCharPipe implements PipeTransform {
  transform(value: string): string {
    let charlimit = 50;
    if (!value || value.length <= charlimit) {
      return value;
    }

    let without_html = value.replace(/<(?:.|\n)*?>/gm, '');
    let shortened = without_html.substring(0, charlimit) + '...';
    return shortened;
  }
}
