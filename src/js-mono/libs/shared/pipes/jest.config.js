module.exports = {
  name: 'shared-pipes',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/shared/pipes',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
