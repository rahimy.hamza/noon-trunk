import { Component, OnInit } from '@angular/core';
import { AuthService } from '@js-mono/shared/auth-service';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';

@Component({
  selector: 'js-mono-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  constructor(public auth: AuthService, private router: Router, private store: Store) {}

  ngOnInit(): void {}

  goHome() {
    // this.router.navigate(['/']);
    this.store.dispatch(new Navigate(['/']));
  }

  goProfile() {
    // this.router.navigate(['profile']);
    this.store.dispatch(new Navigate(['/profile']))
  }
}
