import { Component, OnInit } from '@angular/core';
import { AuthService } from '@js-mono/shared/auth-service';

@Component({
  selector: 'js-mono-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  constructor(public auth: AuthService) {}

  ngOnInit(): void {}
}
