import { async, TestBed } from '@angular/core/testing';
import { SharedAuthServiceModule } from './shared-auth-service.module';

describe('SharedAuthServiceModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedAuthServiceModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(SharedAuthServiceModule).toBeDefined();
  });
});
