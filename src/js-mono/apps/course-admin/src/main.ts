import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

function getBaseUrl() {
  const baseHref = document.getElementsByTagName('base')[0].href;
  if (baseHref[baseHref.length - 1] === '/') {
    return baseHref.substring(0, baseHref.length - 1);
  } else {
    return baseHref;
  }
}

const providers = [{ provide: 'BASE_URL', useFactory: getBaseUrl, deps: [] }];

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic(providers)
  .bootstrapModule(AppModule)
  .catch(err => console.error(err));
