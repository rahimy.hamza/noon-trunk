import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CourseadminappCoursesFeatureListModule } from '@js-mono/courseadminapp/courses/feature-list';
import { CourseadminappSharedUiModule } from '@js-mono/courseadminapp/shared/ui';
import { NgxsModule } from '@ngxs/store';
import { CoursesStateService } from '@js-mono/courseadminapp/shared/state';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { environment } from '../environments/environment';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { BASE_URL } from '@js-mono/courseadminapp/shared/network';

function getBaseUrl() {
  const baseHref = document.getElementsByTagName('base')[0].href;
  if (baseHref[baseHref.length - 1] === '/') {
    return baseHref.substring(0, baseHref.length - 1);
  } else {
    return baseHref;
  }
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CourseadminappSharedUiModule,
    CourseadminappCoursesFeatureListModule,
    NgxsModule.forRoot([CoursesStateService], {
      developmentMode: !environment.production
    }),
    NgxsLoggerPluginModule.forRoot(),
    NgxsRouterPluginModule.forRoot(),
    NgxsFormPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot() // This needs to be last import
  ],
  providers: [{ provide: BASE_URL, useFactory: getBaseUrl }],
  bootstrap: [AppComponent]
})
export class AppModule {}
